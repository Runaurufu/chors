<?php

/**
 * Description of AccessControl
 * @package Chors
 * @version 1.0
 * @since 1.0
 * @author Runaurufu
 */
class Chors_AccessControl
{

  /** Access Control creation Rules file path */
  private static $acrpath = null;

  /** Does AccessControl was initalized */
  private static $init = false;
  private static $permissions = array();
  private $privileges = array();

  /**
   * Init access control class
   */
  public static function init()
  {
    self::clearPermissions();
    include_once self::$acrpath;
    self::$init = true;
  }

  /**
   * Allow to set acr file path
   * @param string $filepath
   * @throws Chors_Exception
   */
  public static function setACRfilepath($filepath)
  {
    if (!Chors_File::exists($filepath))
      throw new Chors_Exception("ACR file not found", Chors_ErrorCodes::FILE_NOT_FOUND, $filepath);
    self::$acrpath = $filepath;
  }

  /**
   * Clears currently saved permissions
   */
  public static function clearPermissions()
  {
    self::$permissions = array();
    self::$permissions['strict'] = array();
    self::$permissions['cluster'] = array();
  }

  /**
   * Adds permission
   * @param string $key
   * @param string $value
   * @param bool $strict
   */
  public static function addPermission($key, $value, $strict = true)
  {
    if ($strict)
    {
      self::$permissions['strict']["$key"] = $value;
    }
    else
    {
      $entry = Array();
      $entry['key'] = $key;
      $entry['value'] = $value;
      self::$permissions['cluster'][] = $entry;
    }
  }

  /**
   * 
   */
  public function __construct()
  {
    $this->privileges['strict'] = array();
    $this->privileges['cluster'] = array();
  }

  /**
   * Add privilege
   * @param string $key
   * @param string $value
   * @param bool $strict
   */
  public function addPrivilege($key, $value, $strict = true)
  {
    if ($strict)
    {
      $this->privileges['strict']["$key"] = $value;
    }
    else
    {
      $entry = Array();
      $entry['key'] = $key;
      $entry['value'] = $value;
      $this->privileges['cluster'][] = $entry;
    }
  }

  /**
   * Checks for minimal rights to execute - will return true upon first condition meet
   * @return bool <b>true</b> if access granted, <b>false</b> if access denied
   */
  public function checkMinimal()
  {
    if (!self::$init)
    {
      self::init();
    }

    foreach ($this->privileges['strict'] as $key => $value)
    {
      if (isset(self::$permissions['strict'][$key]))
      {
        if (self::$permissions['strict'][$key] == $value)
        {
          return true;
        }
      }
    }

    foreach ($this->privileges['cluster'] as $priv)
    {
      foreach (self::$permissions['cluster'] as $perm)
      {
        if ($priv['value'] == $perm['value'] && $priv['key'] == $perm['key'])
        {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Checks for maximal rights to execute - will return true only when all requirements are meet
   * @return bool <b>true</b> if access granted, <b>false</b> if access denied
   */
  public function checkMaximal()
  {
    if (!self::$init)
    {
      self::init();
    }

    foreach ($this->privileges['strict'] as $key => $value)
    {
      if (isset(self::$permissions['strict'][$key]))
      {
        if (self::$permissions['strict'][$key] != $value)
        {
          return false;
        }
      }
      else
      {
        return false;
      }
    }

    foreach ($this->privileges['cluster'] as $priv)
    {
      foreach (self::$permissions['cluster'] as $perm)
      {
        if ($priv['value'] == $perm['value'] && $priv['key'] == $perm['key'])
        {
          continue 2;
        }
      }
      return false;
    }
    return true;
  }

}
