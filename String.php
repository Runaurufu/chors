<?php

/**
 * String class for Chors.
 * @package Chors
 * @version 1.0
 * @since 1.0
 * @author Runaurufu
 */
class Chors_String
{
  /**
   * Splits (Explodes) string into array of strings.
   * @param string $string
   * @param mixed $separators Either string or string[]
   * @param boolean $removeEmptyEntries if true then empty split results will be ommited.
   * @return string[]
   */
  static public function split($string, $separators, $removeEmptyEntries = false)
  {
    if(is_array($separators))
    {
      $separator = $separators[0];
      
      $string = str_replace($separators, $separator, $string);
      
      $separators = $separator;
    }
    
    $array = explode($separators, $string);
    
    if($removeEmptyEntries)
    {
      $ret = array();
      
      foreach ($array as $value)
      {
        if(strlen($value) > 0)
          $ret[] = $value;
      }
      
      $array = $ret;
    }
    
    return $array;
  }
  
  /**
   * Checks if string contain value.
   * @param string $string
   * @param string $value
   * @param bool $caseSensitiveComparison if <b>true</b> then will check with case-sensitive, if <b>false</b> will check with case-insensitive comparison.
   * @return bool
   */
  static public function contains($string, $value, $caseSensitiveComparison = TRUE)
  {
    if ($caseSensitiveComparison)
      return mb_strpos($string, $value) !== false;
    else
      return mb_stripos($string, $value) !== false;
  }
  
  /**
   * Find position of first occurrence of string in a string.
   * @param string $string
   * @param string $value
   * @param boolean $caseSensitiveComparison
   * @return int
   */
  static public function indexOf($string, $value, $caseSensitiveComparison = TRUE)
  {
    if ($caseSensitiveComparison)
      return mb_strpos($string, $value);
    else
      return mb_stripos($string, $value);
  }


  /**
   * String comparison of two strings.
   * @param string $stringA
   * @param string $stringB
   * @param bool $caseSensitiveComparison
   * @return bool
   */
  static public function equals($stringA, $stringB, $caseSensitiveComparison = TRUE)
  {
    return $caseSensitiveComparison
      ? $stringA === $stringB
      : strncmp($stringA, $stringB, strlen($stringA)) === 0;
  }

  /**
   * Checks if string starts with value
   * @param string $string
   * @param string $value
   * @param boolean $caseSensitiveComparison if <b>true</b> then will check with case-sensitive, if <b>false</b> will check with case-insensitive comparison.
   * @return boolean
   */
  static public function startsWith($string, $value, $caseSensitiveComparison = TRUE)
  {
    if($value == '')
      return true;
  
    if($string == '')
      return false;
    
    if($string[0] != $value[0])
      return false;
    
    $len = strlen($value);
    
    if($len > strlen($string))
      return FALSE;
   
    return $caseSensitiveComparison
      ? strncasecmp($string, $value, $len) === 0
      : strncmp($string, $value, $len) === 0;
  }
  
  /**
   * Checks if string ends with value
   * @param string $string
   * @param string $value
   * @param boolean $caseSensitiveComparison if <b>true</b> then will check with case-sensitive, if <b>false</b> will check with case-insensitive comparison.
   * @return boolean
   */
  static public function endsWith($string, $value, $caseSensitiveComparison = TRUE)
  {
    if($value === '')
      return true;
  
    if($string === '')
      return false;

    $len = strlen($value);
    
    if($len > strlen($string))
      return FALSE;
    
    return substr_compare($string, $value, -$len, $len, !$caseSensitiveComparison) === 0;
  }

  /**
   * Convert string into bool value.
   * @param string $string
   * @return bool or <b>null</b> if could not convert
   */
  static public function boolval($string)
  {
    if(is_int($string))
      $string = (int)$string;
    else if(is_float($string))
      $string = (float)$string;
    else
      $string = strtolower($string);
    
    switch ($string)
    {
      case 1:
      case 1.0:
      case 'true':
        $boolvalue = true;
        break;
      case 0:
      case 0.0:
      case 'false':
        $boolvalue = false;
        break;
      default:
        if(is_string($string))
          $boolvalue = filter_var($string, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        else
          $boolvalue = null;
    }
    return $boolvalue;
  }

  /**
   * Make entered string form-safe. This string can now be used within forms attributes.
   * @param string $string
   * @return string
   */
  static public function formSafe($string)
  {
    $string = str_replace('&', '&#38;', $string);
    $string = str_replace('"', '&#34;', $string);
    $string = str_replace('\'', '&#39;', $string);
    $string = str_replace('<', '&#60;', $string);
    $string = str_replace('>', '&#62;', $string);

    return $string;
  }

  /**
   * Strip HTML tags from entered string
   * @param string $string
   * @return string
   */
  static public function stripTags($string)
  {
    return strip_tags($string);
  }

  /**
   * Function acts like parse_str(), but don't perform replacement of any char.
   * @link http://php.net/manual/en/function.parse-str.php
   * @param string $query string in query format: <i>arg1=val1&arg2=val2</i>
   * @return array array in format: <i>['arg1']=val1; ['arg2']=val2;</i>
   * @author Will Voelcker
   */
  public static function parseQueryString($query)
  {
    $op = array();
    $pairs = explode("&", $query);
    foreach ($pairs as $pair)
    {
      list($k, $v) = array_map("urldecode", explode("=", $pair));
      $op[$k] = $v;
    }
    return $op;
  }

  /**
   * Returns string of specified length consisting of only chars specified as a second parameter.
   * @param int $length number of random chars to get.
   * @param string $charset allowed chars.
   * @return string string with random characters.
   */
  public static function getRandom($length, $charset='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789')
  {
    $str = '';
    $count = strlen($charset);
    while ($length--/* > 0*/)
    {
      $str .= $charset[mt_rand(0, $count-1)];
    }
    return $str;
  }
  
  /**
   * Parses template string and fill all template arguments {{ARGUMENT_NAME}} with values found in $templateArgumentsArray.
   * Use \{\{TEXT\}\} to escape parsing TEXT as pattern argument. \{ and \} will be replaced into { and } after arguments usage.
   * @param string $template Template to parse.
   * @param array $templateArgumentsArray Its structure should be like: [templateKey] => keyValue.
   * @return string
   */
  public static function parseTemplate($template, $templateArgumentsArray)
  {
    $arguments = array();
    foreach ($templateArgumentsArray as $key => $value)
    {
      $arguments["{{".$key."}}"] = $value;
    }
    $arguments["\{"] = "{";
    $arguments["\}"] = "}";
    
    return strtr($template, $arguments);
  }
}
