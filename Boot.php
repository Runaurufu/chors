<?php

/**
 * http://www.erulian.in.rs/English/Vuk1.html - Chors, Hors, Horz - God of Moon
 * Boot class for Chors. It is required to make Chors working properly
 * @package Chors
 * @version 1.0
 * @since 1.0
 * @author Runaurufu
 * @todo namespaces should be completely erased echo 'Current PHP version: ' . phpversion();
 */
class Chors_Boot
{  
  static private $instance = NULL;
  static private $baseUrl = NULL;
  static private $useSecureUrl = FALSE;
  static private $rewrite = FALSE;
  private $path = NULL;
  static public $params = NULL;
  /**
   * Sets debug report level:
   * Chors_Logger::LOG_LEVEL_DEBUG - DEBUG and higher messages will be logged,
   * Chors_Logger::LOG_LEVEL_INFO - INFO and higher messages will be logged,
   * Chors_Logger::LOG_LEVEL_WARNING - WARNING and higher messages will be logged,
   * Chors_Logger::LOG_LEVEL_ERROR - ERROR and higher messages will be logged,
   * Chors_Logger::LOG_LEVEL_FATAL - FATAL messages will be logged (this is default).
   * 
   * @var int should be value from Chors_DebugLevel.
   */
  static public $debugReportLevel = Chors_Logger::LOG_LEVEL_FATAL;
  /**
   * Indicate should crashes be logged into single file
   * or should each crash create its own file.
   * @var bool
   */
  static public $logCrashToSingleFile = TRUE;


  /** @todo delete namespaces stuff   */
  protected $allowedNamespaces = NULL;
  protected $namespace = NULL;
  static public $defNamespace = NULL;
  protected $module = NULL;
  protected $controller = NULL;
  protected $action = NULL;
  static public $defController = NULL;
  static public $defAction = NULL;
  private $control = NULL;
  protected $rootDirPath = NULL;
  static public $applicationDir = "application";
  static public $cacheDir = "cache";
  static public $controlDir = "controller";
  static public $entityDir = "entity";
  static public $logDirPath = "application/log";
  static public $viewDir = "view";
  static public $layoutDir = "layout";
  static public $modelDir = "model";
  static public $moduleDir = "module";
  static public $partialDir = "partial";
  static public $presentationDir = "presentation";
  static private $interpreter = "strict";
  static protected $startTime = 0;
  protected $internalErrorUrl = NULL;

  public function __construct($rootPath)
  {
    $this->rootDirPath = $rootPath;

    set_include_path('.' . PATH_SEPARATOR . get_include_path()
            //    .PATH_SEPARATOR . 'Database/'
    );
    
    mb_internal_encoding("UTF-8");
    
    // core utility classes
    require_once 'Core'.DIRECTORY_SEPARATOR.'Interfaces.php';
    require_once 'Core'.DIRECTORY_SEPARATOR.'Enum.php';
    require_once 'Core'.DIRECTORY_SEPARATOR.'Path.php';
    require_once 'Core'.DIRECTORY_SEPARATOR.'DateTime.php';
    require_once 'Core'.DIRECTORY_SEPARATOR.'Blob.php';

    require_once "Time.php";
    Chors_Time::setServerTimeZone(date('Z'));
    date_default_timezone_set('UTC');
    
    require_once "Logger.php";
    require_once "String.php";
    require_once "Array.php";
    require_once "Entity/ARenderable.php";
    require_once "Entity/IdentifiableCollection.php";
    require_once "Entity/LoadedEntities.php";
    require_once "Entity/AEntity.php";
    require_once "AForm.php";
    require_once "AModel.php";
    require_once 'AccessControl.php';
    require_once "Auth.php";
    require_once "Exception.php";
    require_once "Config.php";
    require_once "Cache.php";
    require_once "ClassInfo.php";
    require_once "Controller.php";
    require_once "Database.php";
    require_once "Database/Table.php";
    require_once 'File.php';
    require_once 'File/Ini.php';
    require_once 'Info.php';
    require_once "Layout.php";
    require_once "Registry.php";
    require_once "Localization.php";

    require_once "Auth.php";
    require_once "Session.php";

    self::$instance = $this;
  }

  /**
   * 
   * @return Chors_Boot
   */
  static public function getInstance()
  {
    if (self::$instance === null)
    {
      self::$instance = new self();
      return self::$instance;
    }
    else
      return self::$instance;
  }

  public static function logMessage($debugLevel, $message)
  {
    if($debugLevel >= self::$debugReportLevel)
    {
      self::$instance->cmd_logMessage($debugLevel, $message);
    }
  }
  
  /**
   * Overwrite this function to alter default internal logging
   * (by default chors.log file is used).
   */
  protected function cmd_logMessage($debugLevel, $message)
  {
    Chors_Logger::saveToLogFile("chors.log", $debugLevel."::".$message, true);
  }
  
  public function setRewrite($rewrite)
  {
    self::$rewrite = $rewrite;
  }

  /**
   * Sets used interpreter type. Available interpreters are as follow:<br/>
   * <b>strict</b> - only explicitly assigned values gets key, rest are pushed on params stack<br/>
   * <b>keyEnforced</b> - not explicitly assigned values are grouped into pair, first value becomes key.
   * @param string $type interpreter type name
   */
  public function setInterpreterType($type)
  {
    self::$interpreter = $type;
  }

  private function interpreterStrict()
  {
    self::$params = str_replace("&", "/", $this->path);
    self::$params = str_replace("?", "/", self::$params);
    self::$params = explode("/", self::$params);

    $append = array();
    $params = array();
    $count = count(self::$params);
    for ($i = 2; $i < $count; $i++)
    {
      $comp = explode("=", self::$params[$i]);
      if (isset($comp[1]))
        $params[$comp[0]] = $comp[1];
      else
        $append[] = $comp[0];
    }

    foreach ($append as $value)
      $params[] = $value;

    if (isset(self::$params[0]))
      $params["__controller"] = self::$params[0];
    else
      $params["__controller"] = self::$defController;

    if (isset(self::$params[1]))
      $params["__action"] = self::$params[1];
    else
      $params["__action"] = self::$defAction;

    self::$params = $params;
  }

  private function interpreterKeyEnforced()
  {
    self::$params = str_replace("&", "/", $this->path);
    self::$params = str_replace("?", "/", self::$params);
    self::$params = explode("/", self::$params);

    $append = array();
    $params = array();
    $count = count(self::$params);
    for ($i = 2; $i < $count; $i++)
    {
      $comp = explode("=", self::$params[$i]);
      if (isset($comp[1]))
        $params[$comp[0]] = $comp[1];
      else
        $append[] = $comp[0];
    }

    $count = count($append);
    for ($i = 0; $i + 2 <= $count; $i+=2)
      $params[$append[$i]] = $append[$i + 1];

    if (isset(self::$params[0]))
      $params["__controller"] = self::$params[0];
    else
      $params["__controller"] = self::$defController;

    if (isset(self::$params[1]))
      $params["__action"] = self::$params[1];
    else
      $params["__action"] = self::$defAction;

    self::$params = $params;
  }

  /**
   * Parameter's separators: /?&
   */
  private function interpreter()
  {
    switch (self::$interpreter)
    {
      case "keyEnforced":
        $this->interpreterKeyEnforced();
        break;
      case "strict":
      default:
        $this->interpreterStrict();
        break;
    }
    $this->controller = self::$params['__controller'];
    $this->action = self::$params['__action'];
  }

  public function setBaseUrl($url)
  {
    self::$baseUrl = $url;
  }
  
  /**
   * Sets whenever application should be default use secure BaseUrl or not.
   * @param bool $value
   */
  public function setUseSecureUrl($value)
  {
    self::$useSecureUrl = $value;
  }

  static public function getBaseUrl()
  {
    return self::getCurrentProtocolBaseUrl();
  }

  static public function getUrl()
  {
    return self::getCurrentProtocolUrl();
  }
  
  static public function getCurrentProtocolBaseUrl()
  {
    if(isset($_SERVER['HTTPS']))
      return self::getSecureBaseUrl();
    else
      return self::getNonSecureBaseUrl();
  }
  
  static public function getCurrentProtocolUrl()
  {
    if(isset($_SERVER['HTTPS']))
      return self::getSecureUrl();
    else
      return self::getNonSecureUrl();
  }
  
  static public function getPrefferedProtocolBaseUrl()
  {
    if(self::$useSecureUrl)
      return self::getSecureBaseUrl();
    else
      return self::getNonSecureBaseUrl();
  }
  
  static public function getPrefferedProtocolUrl()
  {
    if(self::$useSecureUrl)
      return self::getSecureUrl();
    else
      return self::getNonSecureUrl();
  }
  
  static public function getNonSecureBaseUrl()
  {
    return "http://" . self::$baseUrl;
  }

  static public function getNonSecureUrl()
  {
    if (self::$rewrite)
      return "http://" . self::$baseUrl . "/";
    else
      return "http://" . self::$baseUrl . "/?";
  }

  static public function getSecureBaseUrl()
  {
    return "https://" . self::$baseUrl;
  }

  static public function getSecureUrl()
  {
    if (self::$rewrite)
      return "https://" . self::$baseUrl . "/";
    else
      return "https://" . self::$baseUrl . "/?";
  }

  public function getApplicationDirectory()
  {
    return self::$applicationDir;
  }

  public function setApplicationDirectory($dir)
  {
    self::$applicationDir = $dir;
  }

  public function setCacheDirectory($dir)
  {
    self::$cacheDir = $dir;
  }

  public function setControllerDirectory($dir)
  {
    self::$controlDir = $dir;
  }

  public function setEntityDirectory($dir)
  {
    self::$entityDir = $dir;
  }

  public function setViewDirectory($dir)
  {
    self::$viewDir = $dir;
  }

  public function setLayoutDirectory($dir)
  {
    self::$layoutDir = $dir;
  }

  public function setLogDirectoryPath($dir)
  {
    self::$logDirPath = $dir;
  }

  public static function getLogDirectoryPath()
  {
    return self::$logDirPath;
  }

  public function setModelDirectory($dir)
  {
    self::$modelDir = $dir;
  }

  public function setModuleDirectory($dir)
  {
    self::$moduleDir = $dir;
  }

  public function setPartialDirectory($dir)
  {
    self::$partialDir = $dir;
  }

  public function setPresentationDirectory($dir)
  {
    self::$presentationDir = $dir;
  }

  /**
   * Sets default values for controller and action.
   * @param string $controller default controller to be called
   * @param string $action default action to be called
   */
  public function setDefault($controller, $action)
  {
    self::$defController = $controller;
    self::$defAction = $action;
  }

  public function setDefaultAction($action)
  {
    self::$defAction = $action;
  }

  /**
   * @deprecated namespaces will be deleted
   * @todo delete
   */
  public function setDefaultNamespace($namespace)
  {
    self::$defNamespace = $namespace;
  }

  /**
   * @deprecated namespaces will be deleted
   * @todo delete
   * Adds specified namespaces to allowed namespaces list.
   * If you do not specify any allowed namespace than this feature will be
   * completely turned off.
   * All namespaces not listed on allowed list will result in being transformed
   * into default namespace (and if default is not specified error will be thrown).
   * @param Array $namespacesArray new namespaces to be allowed
   */
  public function addAllowedNamespaces($namespacesArray)
  {
    if (!is_array($namespacesArray))
      throw new Chors_Exception("Wrong argument's type - Array expected",
      Chors_Exception::ERR_DATATYPE);

    if ($this->allowedNamespaces === null)
      $this->allowedNamespaces = $namespacesArray;
    else
      $this->allowedNamespaces = array_merge($this->allowedNamespaces,
              $namespacesArray);
  }

  /**
   * @deprecated namespaces will be deleted
   * @todo delete
   * @return void
   */
  private function loadNamespaceConfig()
  {
    if ($this->allowedNamespaces === null)
      return;

    Chors_Registry::setFromConfig('namespace',
            self::$applicationDir . DIRECTORY_SEPARATOR . self::$_namespaceDir . DIRECTORY_SEPARATOR . $this->namespace . '.ini',
            true);
  }

  /**
   * @deprecated namespaces will be deleted
   * @todo delete
   * @return <type>
   */
  private function legacyInterpreter()
  {
    self::$params = explode('/', $this->path);
    if ($this->allowedNamespaces === null)
    {
      if (isset(self::$params[0]))
        $this->controller = self::$params[0];
      else
        $this->controller = self::$defController;

      if (isset(self::$params[1]))
        $this->action = self::$params[1];
      else
        $this->action = self::$defAction;

      self::$params = array_slice(self::$params, 2, null, true);
    }
    else
    {
      if (isset(self::$params[0]))
        $this->namespace = self::$params[0];
      elseif (self::$defNamespace !== null)
        $this->namespace = self::$defNamespace;
      else
        throw new Chors_Exception("Default namespace not defined",
        Chors_Exception::ERR_BOOT_NNTDEF);

      if (!in_array($this->namespace, $this->allowedNamespaces))
      {
        if (self::$defNamespace === null)
          throw new Chors_Exception("Default namespace not defined",
          Chors_Exception::ERR_BOOT_NNTDEF);

        $this->namespace = self::$defNamespace;
        $temp = array_keys($this->allowedNamespaces, $this->namespace, true);
        $this->_namespaceIndex = $temp[0] + 1;

        if (isset(self::$params[0]))
          $this->controller = self::$params[0];
        else
          $this->controller = self::$defController;

        if (isset(self::$params[1]))
          $this->action = self::$params[1];
        else
          $this->action = self::$defAction;

        self::$params = array_slice(self::$params, 2, null, true);
      }
      else
      {
        $temp = array_keys($this->allowedNamespaces, $this->namespace, true);
        self::$_namespaceIndex = $temp[0] + 1;

        if (isset(self::$params[1]))
          $this->controller = self::$params[1];
        else
          $this->controller = self::$defController;

        if (isset(self::$params[2]))
          $this->action = self::$params[2];
        else
          $this->action = self::$defAction;

        self::$params = array_slice(self::$params, 3, null, true);
      }
    }
    $params = array();
    foreach (self::$params as $param)
      $params[] = $param;

    self::$params = $params;
  }

  private function run()
  {
    if (self::$rewrite)
    //    $this->_path = str_replace(self::$_baseUrl."/","",$_SERVER['REQUEST_URI']);
      $this->path = str_ireplace("/" . str_replace(" ", "%20",
                      basename($this->getRootDirectory ())) . "/", "", $_SERVER['REQUEST_URI']);
    else
      $this->path = $_SERVER['QUERY_STRING'];

    $this->path = trim($this->path, '\\/');
    $this->interpreter();

    Chors_Controller::checkControllerExistence($this->controller, true);
    $this->controllerRun($this->controller, $this->action);
  }

  /**
   * @return float time in seconds which elapsed since run method initation
   */
  static public function getExecutionTime()
  {
    return microtime(true) - self::$startTime;
  }

  /**
   * @param bool $realUsage
   * @return int memory used in bytes 
   */
  static public function getMemoryUsage($realUsage = false)
  {
    return memory_get_usage($realUsage);
  }

  /**
   * @param bool $realUsage
   * @return int peak memory used in bytes
   */
  static public function getMemoryPeakUsage($realUsage = false)
  {
    return memory_get_peak_usage($realUsage);
  }

  /**
   *
   * @param string $string
   * @todo rewrite as soon as queue really would be added to Boot 
   */
  public function queueURL($string)
  {
    $this->path = $string;
    $this->interpreter();
    $this->controllerRun($this->controller, $this->action);
  }

  private function startup()
  {
    define('E_FATAL',
            E_ERROR | E_USER_ERROR | E_PARSE | E_CORE_ERROR |
            E_COMPILE_ERROR | E_RECOVERABLE_ERROR);

    register_shutdown_function(array($this, 'shutdownHandler'));

    set_error_handler(array($this, 'errorHandler'));
  }

  function shutdownHandler()
  {
    $error = error_get_last();

    if ($error)
    {
      if ($error['type'] & E_FATAL)
      {
        $this->errorHandler($error['type'], $error['message'], $error['file'],
                $error['line']);
      }
    }
  }

  function errorHandler($errno, $errstr, $errfile, $errline)
  {
    switch ($errno)
    {
      case E_ERROR: // 1 //
        $typestr = 'E_ERROR';
        break;
      case E_WARNING: // 2 //
        $typestr = 'E_WARNING';
        break;
      case E_PARSE: // 4 //
        $typestr = 'E_PARSE';
        break;
      case E_NOTICE: // 8 //
        $typestr = 'E_NOTICE';
        break;
      case E_CORE_ERROR: // 16 //
        $typestr = 'E_CORE_ERROR';
        break;
      case E_CORE_WARNING: // 32 //
        $typestr = 'E_CORE_WARNING';
        break;
      case E_COMPILE_ERROR: // 64 //
        $typestr = 'E_COMPILE_ERROR';
        break;
      case E_COMPILE_WARNING: // 128 //
        $typestr = 'E_COMPILE_WARNING';
        break;
      case E_USER_ERROR: // 256 //
        $typestr = 'E_USER_ERROR';
        break;
      case E_USER_WARNING: // 512 //
        $typestr = 'E_USER_WARNING';
        break;
      case E_USER_NOTICE: // 1024 //
        $typestr = 'E_USER_NOTICE';
        break;
      case E_STRICT: // 2048 //
        $typestr = 'E_STRICT';
        break;
      case E_RECOVERABLE_ERROR: // 4096 //
        $typestr = 'E_RECOVERABLE_ERROR';
        break;
      case E_DEPRECATED: // 8192 //
        $typestr = 'E_DEPRECATED';
        break;
      case E_USER_DEPRECATED: // 16384 //
        $typestr = 'E_USER_DEPRECATED';
        break;
    }

    // if ($errno & E_FATAL)
    {
      
      //debug_print_backtrace();
      
      $message = "Time: " . Chors_Time::timestampToString() . PHP_EOL
        ."Uri: " . $_SERVER['REQUEST_URI'] . PHP_EOL
        .Chors_Exception::createErrorString($errfile, $errline,
                      $typestr, $errstr, PHP_EOL);
      $message .= PHP_EOL. "Backtrace: ". print_r(debug_backtrace(), true);
      
      $fileName = "crash.log";
      
      
      // 
      
      try
      {
        if(self::$logCrashToSingleFile)
          Chors_Logger::saveToLogFile($fileName, $message . "--------------------", false);
        else
          Chors_Logger::saveToUniqueLogFile($fileName, $message . "--------------------", false);
      }
      catch (Exception $e)
      {
        try
        {
          $msg = $message . "--------------------" . PHP_EOL . Chors_Exception::toString($e);
          error_log($msg, 3,
                Chors_Boot::getLogDirectoryPath() . DIRECTORY_SEPARATOR . $fileName);
        }
        catch (Exception $ex)
        {
          echo "<pre>" . $message . "</pre>";
          return false;
        }
      }
    }
    
    if ($errno & E_FATAL)
    {
      if($this->internalErrorUrl === NULL)
      {
        echo "<pre>" . $message . "</pre>";
        http_response_code(500);
      }
      else
      {
        header('Location: ' . $this->internalErrorUrl, true, 302);
      }
    }
    
    return true;
  }

  /**
   * Allow setting absolute url to use in case of internal errors
   * @param string $url
   */
  function setInternalErrorUrl($url)
  {
    $this->internalErrorUrl = $url;
  }

  /**
   * Debug run - catches all Chors exceptions and presents them in detailed way.
   * Do not use it on final releases as crucial data may leak in case of any system crash.
   */
  public function debugRun($enableInternalTrace = false)
  {
    error_reporting(E_ALL);
    ini_set('display_errors', '1');

    if ($enableInternalTrace == true)
      Chors_Logger::enableInternalTrace();

    self::$startTime = microtime(true);
    try
    {
      $this->startup();
      $this->cmd_initialization();
      $this->run();
    }
    catch (Exception $e)
    {
      $message = "Time: " . Chors_Time::timestampToString()
        .PHP_EOL ."Uri: " . $_SERVER['REQUEST_URI']
        .PHP_EOL . Chors_Exception::toString($e, PHP_EOL);

      echo "<pre>" . $message . "</pre>";

      Chors_Logger::saveToLogFile("crash.log",
              $message . "--------------------", false);
    }
  }

  /**
   * Release run - catches all Chors exceptions and hides them from final user.
   * Use it in final releases as all errors are silently saved for admin view only.
   */
  public function releaseRun()
  {
    error_reporting(E_ALL & ~E_NOTICE);
    ini_set('display_errors', '0');

    try
    {
      $this->startup();
      $this->cmd_initialization();
      $this->run();
    }
    catch (Exception $e)
    {
      $message = "Time: " . Chors_Time::timestampToString() . PHP_EOL . Chors_Exception::toString($e,
                      PHP_EOL);

      Chors_Logger::saveToLogFile("crash.log",
              $message . "--------------------", false);
    }
  }

  /**
   * Overwrite this function and put inside all actions that should be performed
   * in initialization phase before any business logic happen
   */
  protected function cmd_initialization()
  {
    ;
  }

  public function execute($controllerName = null, $controllerAction = null)
  {
    // TODO 
    Chors_Controller::checkControllerExistence($controllerName, true);
  }

  public function controllerRun($controllerName = null, $controllerAction = null)
  {
    $this->controller = $controllerName;
    $this->action = $controllerAction;

    $control = $this->controller . "Controller";

    $ctrl = $this->control;
    $this->control = new $control();
    $this->control->inheritController($ctrl);

    $this->control->setCurrentAction($this->action);
    /**
     * @todo check if action == null and if than take proper actions -> set to def
     */
    Chors_Logger::internalTrace("Boot run controller: " . $controllerName . "::" . $controllerAction . "()");
    $this->control->{$this->action . "Action"}();
  }

  static public function setParams($array)
  {
    foreach ($array as $key => $value)
      self::$params[$key] = $value;
  }

  static public function setParam($key, $value)
  {
    self::$params[$key] = $value;
  }

  static public function getParams()
  {
    return self::$params;
  }

  static public function getParam($key)
  {
    return self::$params[$key];
  }

  static public function isParamSet($key)
  {
    return isset(self::$params[$key]);
  }
  
  /**
   * @return int Number of parameters
   */
  static public function countParams()
  {
    return count(self::$params) - 2;
  }

  /**
   * Check if specified key exists either in Params, $_POST or $_GET array.
   * @param string $key
   * @return bool
   */
  static public function isDataSet($key)
  {
    return self::isParamSet($key) || isset($_POST[$key]) || isset($_GET[$key]);
  }
  
  /**
   * Get data associated with specified key from Params, $_POST and $_GET arrays
   * (searches in that order) or NULL if no data was found.
   * @param string $key
   * @return mixed
   */
  static public function getData($key)
  {
    if(self::isParamSet($key))
      return self::getParam($key);
    
    if(isset($_POST[$key]))
      return $_POST[$key];
    
     if(isset($_GET[$key]))
      return $_GET[$key];
     
     return NULL;
  }
  
  /**
   * 
   * @return string Request method
   */
  static public function getRequestMethod()
  {
    return $_SERVER['REQUEST_METHOD'];
  }
  
  /**
   * 
   * @return Bool is request method POST?
   */
  static public function isRequestMethodPost()
  {
    return self::getRequestMethod() === "POST";
  }

  /**
   * 
   * @return Bool is request method GET?
   */
  static public function isRequestMethodGet()
  {
    return self::getRequestMethod() === "GET";
  }

  /**
   * @return String orginal URL path desired by user 
   */
  static public function getPath()
  {
    return self::$instance->path;
  }

  /**
   * @return String root directory for current application 
   */
  public function getRootDirectory()
  {
    return $this->rootDirPath;
  }

  /**
   * @link https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html RFC 2616 Status Code Definitions
   * @param int $code http response code to send
   * @param string $text message send in header
   * @return int http response code sent
   */
  static public function httpResponseCode($code, $text = NULL)
  {
    $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
    
    if($text === NULL)
    {
      switch ($code)
      {
        case 100: $text = 'Continue'; break;
        case 101: $text = 'Switching Protocols'; break;
        case 200: $text = 'OK'; break;
        case 201: $text = 'Created'; break;
        case 202: $text = 'Accepted'; break;
        case 203: $text = 'Non-Authoritative Information'; break;
        case 204: $text = 'No Content'; break;
        case 205: $text = 'Reset Content'; break;
        case 206: $text = 'Partial Content'; break;
        case 300: $text = 'Multiple Choices'; break;
        case 301: $text = 'Moved Permanently'; break;
        case 302: $text = 'Moved Temporarily'; break;
        case 303: $text = 'See Other'; break;
        case 304: $text = 'Not Modified'; break;
        case 305: $text = 'Use Proxy'; break;
        case 307: $text = 'Temporary Redirect'; break;
        case 400: $text = 'Bad Request'; break;
        case 401: $text = 'Unauthorized'; break;
        case 402: $text = 'Payment Required'; break;
        case 403: $text = 'Forbidden'; break;
        case 404: $text = 'Not Found'; break;
        case 405: $text = 'Method Not Allowed'; break;
        case 406: $text = 'Not Acceptable'; break;
        case 407: $text = 'Proxy Authentication Required'; break;
        case 408: $text = 'Request Time-out'; break;
        case 409: $text = 'Conflict'; break;
        case 410: $text = 'Gone'; break;
        case 411: $text = 'Length Required'; break;
        case 412: $text = 'Precondition Failed'; break;
        case 413: $text = 'Request Entity Too Large'; break;
        case 414: $text = 'Request-URI Too Large'; break;
        case 415: $text = 'Unsupported Media Type'; break;
        case 500: $text = 'Internal Server Error'; break;
        case 501: $text = 'Not Implemented'; break;
        case 502: $text = 'Bad Gateway'; break;
        case 503: $text = 'Service Unavailable'; break;
        case 504: $text = 'Gateway Time-out'; break;
        case 505: $text = 'HTTP Version not supported'; break;
        default:
            exit('Unknown http status code "' . htmlentities($code) . '"');
        break;
      }
    }
    
    if ($code === NULL)
    {
      $code = 200;
    }
    header($protocol . ' ' . $code . ' ' . $text);
    return $code;
  }
}
