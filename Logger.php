<?php
require_once 'File.php';

/**
 * Logger class for Chors.
 * @package Chors
 * @version 1.0
 * @since 1.0
 * @author Runaurufu
 */
abstract class Chors_Logger
{
  const LOG_LEVEL_DEBUG = 0;
  const LOG_LEVEL_INFO = 1;
  const LOG_LEVEL_WARNING = 2;
  const LOG_LEVEL_ERROR = 3;
  const LOG_LEVEL_FATAL = 4;
  
  private static $traceFilePath = null;

  public static function saveToFile($filename, $content, $addDateTime = false)
  {
    if ($addDateTime == true)
      Chors_File::appendAsString($filename, Chors_Time::timestampToString() . " - " . $content);
    else
      Chors_File::appendAsString($filename, $content);
  }

  public static function saveToLogFile($logFilename, $content, $addDateTime = false)
  {
    self::saveToFile(Chors_Boot::getLogDirectoryPath() . DIRECTORY_SEPARATOR . $logFilename, $content . PHP_EOL, $addDateTime);
  }
  
  /**
   * 
   * @param type $logFileBaseName
   * @param type $content
   * @param type $addDateTime
   * @return string fileName used to save specified content.
   */
  public static function saveToUniqueLogFile($logFileName, $content, $addDateTime = false)
  {
    $ext = Chors_File::getExtension($logFileName);
    if($ext === "")
      $ext = ".log";
    else
      $logFileName = substr($logFileName, 0, strlen($logFileName) - strlen($ext));
    
    $fileName = self::getUniqueFileName(Chors_Boot::getLogDirectoryPath(), $logFileName, $ext);
    self::saveToFile(Chors_Boot::getLogDirectoryPath() . DIRECTORY_SEPARATOR . $fileName, $content, $addDateTime);
    return $fileName;
  }
  
  /**
   * 
   * @param string $directory
   * @param string $baseFileName
   * @param string $extension specify with .dot
   * @return string
   */
  public static function getUniqueFileName($directory, $baseFileName, $extension)
  {
    $fileNameCore = $baseFileName;
    $i = 1;
    do
    {
      $fileName = Chors_Path::removeInvalidFileNameChars($fileNameCore) . $extension;
      $fileNameCore = $baseFileName. " - ".$i;
      $i++;
    }
    while(Chors_File::exists($directory . DIRECTORY_SEPARATOR . $fileName));
    return $fileName;
  }

  public static function enableInternalTrace()
  {
    $traceDir = Chors_Boot::getLogDirectoryPath() . DIRECTORY_SEPARATOR . "trace";
    if (is_dir($traceDir) == false)
      mkdir($traceDir);

    $traceName = Chors_Time::timestampToString() . "-" . $_SERVER['REMOTE_ADDR'] . "-" . $_SERVER['HTTP_USER_AGENT'] . ".trace";
    $traceName = Chors_Path::removeInvalidFileNameChars($traceName);

    $traceName = $traceDir . DIRECTORY_SEPARATOR . $traceName;
    if (Chors_File::exists($traceName))
      unlink($traceName);
    self::$traceFilePath = $traceName;

    Chors_File::appendAsString(self::$traceFilePath, Chors_Time::timestampToString() . PHP_EOL);
  }

  public static function internalTrace($data)
  {
    if (self::$traceFilePath == null)
      return;
    Chors_File::appendAsString(self::$traceFilePath, Chors_Boot::getExecutionTime() . " - " . $data . PHP_EOL);
  }

  /**
   * 
   * @param anything $variableToDump
   * @param bool $useVarDump if set to true then function will use var_dump function and catch its output. Use only in debug or serious need as it causes performance hit.
   * @return string
   */
  public static function getVariableDump($variableToDump, $useVarDump = false)
  {
    if($useVarDump === true)
    {
      ob_start();
      var_dump($variableToDump);
      return ob_get_clean();
    }
    else
      return var_export($variableToDump, true);
  }
}
