<?php

/**
 * Description of Chors_DocumentationParser
 *
 * @author Runaurufu
 */
class Chors_ParsedDoc
{
  /**
   * Description provided for documented element.
   * @var string
   */
  public $description;
  
  /**
   * Params definitions provided for documented element in form of array.
   * Available keys (if found): name, type, description
   * @var string[]
   */
  public $params;

  /**
   * Return data definition provided for documented element in form of array.
   * Available keys (if found): type, description
   * @var string[]
   */
  public $return;
  
  /**
   * Documented type of variable.
   * @var string
   */
  public $var;

  /**
   * 
   * @param string $doc
   * @return Chors_ParsedDoc Description
   * 
   * 
   * @abstract
   * @access public
   * @author John Doe <john.doe@example.com>
   * @category name
   * @copyright (c) year, John Doe
   * @deprecated since version number
   * @example path description
   * @final
   * @global type $variable
   * @ignore
   * @internal description
   * @license http://URL name
   * @link URL description
   * @name $globalVariableName
   -- * @param type $name Description
   -- * @return type Description
   * @see elementName
   * @since version
   * @static
   * @staticvar type Description
   * @throws Exception
   * @todo Description
   * @tutorial package
   * @uses element Description
   -- * @var type 
   * @version string
   */
  public static function createFromDoc($doc)
  {
    $parsedDoc = new Chors_ParsedDoc();
    
    $lines = Chors_String::split($doc, array("\r\n", "\n\r", "\n", "\r"));
    $linesCount = count($lines);
    
    // trim starting /**
    $lines[0] = mb_substr($lines[0], 3);
    // trim ending */
    $lines[$linesCount - 1] = mb_substr($lines[$linesCount - 1], 0, mb_strlen($lines[$linesCount - 1]) - 2);
    
    $parsedDoc->description = NULL;
    
    for ($i = 0; $i < $linesCount; $i++)
    {
       // remove all white characters
      $lines[$i] = trim($lines[$i]);

      // if line starts with * then * should be trimmed as well
      $index = mb_strpos($lines[$i], "*");
      if($index === 0) 
      {
        $lines[$i] = mb_substr($lines[$i], $index + 1);
      }

      //<editor-fold defaultstate="collapsed" desc="@param">
      $index = mb_strpos($lines[$i], "@param ");
      if($index !== FALSE)
      {
        $items = self::parseLineToElements($lines[$i], 4);
        $count = count($items);

        $param = array();
        switch (TRUE)
        {
          case $count > 3;
            $param['description'] = $items[3];
          case $count > 2;
            $param['name'] = substr($items[2], 1);
          case $count > 1:
            $param['type'] = $items[1];
            break;
          default:
            continue;
        }

        if($parsedDoc->params === NULL)
          $parsedDoc->params = array();
        $parsedDoc->params[] = $param;

        continue;
      }
      // </editor-fold>

      //<editor-fold defaultstate="collapsed" desc="@return">
      $index = mb_strpos($lines[$i], "@return ");
      if($index !== FALSE)
      {
        $items = self::parseLineToElements($lines[$i], 3);
        $count = count($items);

        $data = array();;
        switch (TRUE)
        {
          case $count > 2;
            $data['description'] = $items[2];
          case $count > 1:
            $data['type'] = $items[1];
            break;
          default:
            continue;
        }

        $parsedDoc->return = $data;

        continue;
      }
      // </editor-fold>

      // <editor-fold defaultstate="collapsed" desc="@var">
      $index = mb_strpos($lines[$i], "@var ");
      if($index !== FALSE)
      {
        $items = self::parseLineToElements($lines[$i], 3);
        $count = count($items);

        if($count > 1)
          $parsedDoc->var = $items[1];
        if($count > 2)
        {
          if($parsedDoc->description !== NULL)
            $parsedDoc->description .= PHP_EOL;
          $parsedDoc->description .= $items[2];
        }
        continue;
      }
      // </editor-fold>

      if(strlen($lines[$i]) > 0)
      {
        if($parsedDoc->description !== NULL)
          $parsedDoc->description .= PHP_EOL;
        $parsedDoc->description .= $lines[$i];
      }
    }
    
    return $parsedDoc;
  }
  
  /**
   * Parses documentation line into array with up to provided number of entries.
   * @param string $line
   * @param integer $expectedElements
   * @return string[]
   */
  private static function parseLineToElements($line, $expectedElements)
  {
    $line = trim($line);
    $line = preg_replace('/\s+/', ' ', $line);
    $elements = explode(" ", $line, $expectedElements);
    return $elements;
  }
}
