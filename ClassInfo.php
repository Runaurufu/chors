<?php

/**
 * Description of ClassInfo
 * @package Chors
 * @version 1.0
 * @since 1.0
 * @author Runaurufu
 */
abstract class Chors_ClassInfo
{

  /**
   * Returns constants defined in specified class
   * @param mixed $object object instance or class name
   * @return array Array in format <i><b>constant name</b> => <b>constant value</b></i>
   */
  static function getAllConstants($object)
  {
    $globalFields = array();
    $reflectionClass = new ReflectionClass($object);
    return $reflectionClass->getConstants ();
  }
  
  /**
   * 
   * @param object $object
   * @return array Array in format <i><b>property name</b> => <b>property value</b></i>
   */
  static function getAllFields($object)
  {
    $globalFields = array();
    $reflectionClass = new ReflectionClass($object);
    do
    {
      /* @var $reflections ReflectionProperty[] */
      $reflections = $reflectionClass->getProperties();

      foreach ($reflections as $reflectionProperty)
      {
        $reflectionProperty->setAccessible(true);
        $globalFields[$reflectionProperty->name] = $reflectionProperty->getValue($object);
      }
      $reflectionClass = $reflectionClass->getParentClass();
    } while ($reflectionClass);
    return $globalFields;
  }

  /**
   * 
   * @param object $object
   * @return array Array in format <i><b>property name</b> => array( "access", "static", "value" )</i>
   */
  static function getAllFieldsStrict($object)
  {
    $globalFields = array();
    $reflectionClass = new ReflectionClass($object);
    do
    {
      $reflections = $reflectionClass->getProperties();

      foreach ($reflections as $reflectionProperty)
      {
        if ($reflectionProperty->getDeclaringClass()->name == $reflectionClass->name)
        {
          switch (true)
          {
            case $reflectionProperty->isPrivate():
              $globalFields[$reflectionProperty->name]['access'] = 'private';
              break;
            case $reflectionProperty->isProtected():
              $globalFields[$reflectionProperty->name]['access'] = 'protected';
              break;
            case $reflectionProperty->isPublic():
              $globalFields[$reflectionProperty->name]['access'] = 'public';
              break;
          }
          $reflectionProperty->setAccessible(true);
          $globalFields[$reflectionProperty->name]['value'] = $reflectionProperty->getValue($object);
          $globalFields[$reflectionProperty->name]['static'] = $reflectionProperty->isStatic();
        }
      }
      $reflectionClass = $reflectionClass->getParentClass();
    } while ($reflectionClass);
    return $globalFields;
  }

}
