<?php

/**
 * Chors cache class, use it for any caching related operations
 * @author Runaurufu
 * @todo add validate/update/remove method which will delete specified cache file
 */
class Chors_Cache
{
  /** @var string file extension for cache files */
  static private $extension = 'ccc';

  /** @var int cache files expiration time in seconds */
  private $cacheTime = null;

  /** @var string type of access for specified cache file, turns into filename fragment */
  private $accessType = null;

  /** @var string unique identifier for cache files */
  private $identifier = null;

  /** @var string type identifier for cache files (sub directory in cache folder) */
  private $type = null;

  /** @var bool inform if cache source is proper data source or just text source */
  private $isDataSource = false;

  /** @var string cache filename with directory path */
  private $filename = null;

  /** @var int cache age */
  private $age = null;

  /**
   * Sets specified extension for newly created Chors_Cache files.
   * This <u>will not</u> replace or delete any existing cache files with different extensions.
   * By default cache files extension is set to 'ccc' - Chors Cached Content - files.
   * @param string $extension new cached files extension
   */
  public static function setCachedFilesExtension($extension)
  {
    self::$extension = $extension;
  }

  /**
   * Creates cache object
   * @param array $cacheSettings contains cache settings, for more info see {@link Chors_Cache::parseSettings()}
   * @param string $identifier unique identifier for specified cached source
   * @see Chors_Cache::parseSettings()
   */
  public function __construct($cacheSettings, $identifier)
  {
    $this->identifier = $identifier;
    $this->parseSettings($cacheSettings);
  }

  /**
   * Parse settings and puts them into class private variables for use in future operations
   * Options are as follow:<br/>
   * <b>time</b> - cache time life time in seconds<br/>
   * <b>access</b> - type of access to specified data (can be username, id, anything),
   * it allow to distinguish different data types coming from same identifier location
   * (like different rendered view for user and admin)<br/>
   * <b>datasource</b> - true/false - does cached values should be serialized
   * or put in cache as plain text (and thus allow for readfile operation)
   * @param array $cacheSettings contains cache settings
   */
  private function parseSettings($cacheSettings)
  {
    if (isset($cacheSettings['time']))
      $this->cacheTime = $cacheSettings['time'];
    if (isset($cacheSettings['access']))
      $this->accessType = $cacheSettings['access'];
    if (isset($cacheSettings['datasource']))
      $this->isDataSource = $cacheSettings['datasource'];
    if (isset($cacheSettings['type']))
      $this->type = $cacheSettings['type'];
    $this->generateFilename();
  }

  /**
   * Generates filename for current cached identifier.
   */
  private function generateFilename()
  {
    $filename = Chors_Boot::$applicationDir . DIRECTORY_SEPARATOR . Chors_Boot::$cacheDir;
    if ($this->type !== NULL)
      $filename .= DIRECTORY_SEPARATOR . $this->type;

    if (is_dir($filename) === false)
      mkdir($filename, 0777, true);
    $filename .= DIRECTORY_SEPARATOR;

    if ($this->isDataSource)
    {
      $filename .= 'ds.' . $this->identifier;
    }
    else
    {
      $filename .= 'rd.' . $this->identifier;
    }

    if ($this->accessType !== null)
    {
      $filename .='.' . $this->accessType;
    }
    $filename.= '.' . self::$extension;

    $this->filename = $filename;
  }

  /**
   * Checks if current cache file is still up to date.
   * @return bool <b>true</b> - current cache file is up to date<br/>
   * <b>false</b> - current cache file does not exist or is expired.
   */
  public function check()
  {
    $age = $this->getCacheAge();
    if ($age !== NULL)
    {
      if ($this->getCacheAge() < $this->cacheTime)
        return TRUE;
    }
    return FALSE;
  }

  /**
   * @return int Age of existing cache file
   */
  public function getCacheAge()
  {
    if ($this->age === NULL)
    {
      if (Chors_File::exists($this->filename))
        $this->age = time() - filemtime($this->filename);
    }
    return $this->age;
  }

  /**
   * Delete cache file if any exists. 
   */
  public function delete()
  {
    if (Chors_File::exists($this->filename))
      unlink($this->filename);
    $this->age = NULL;
  }

  /**
   * Fetch data from cache file concerning it as datasource - they will be unserialized.
   * @return mixed data retrieved from cache
   */
  public function fetchDataSource()
  {
    $data = Chors_File::readAsString($this->filename);
    $data = unserialize($data);
    return $data;
  }

  /**
   * Saves specified value to cache file
   * @param mixed $cachedValue this value will be serialized and saved in cache file
   */
  public function saveDataSource($cachedValue)
  {
    $cachedValue = serialize($cachedValue);
    Chors_File::writeAsString($this->filename, $cachedValue, true, false, false);
    $this->age = 0;
  }

  /**
   * Fetch data from cache file concerning it as plain text - they <u>will not</u> be unserialized
   * @return string data retrieved from cache
   */
  public function fetchRender()
  {
    $data = Chors_File::readAsString($this->filename);
    return $data;
  }

  /**
   * Retrieves data from cache file and immediately forward them into output buffer.<br/>
   * Works pretty same as <i>echo fetchRender()</i> but utilize other approach.
   */
  public function displayRender()
  {
    ob_clean();
    flush();
    readfile($this->filename);
  }

  /**
   * Saves specified value to cache file
   * @param string $cachedValue this value will be saved in cache file (no serialization)
   */
  public function saveRender($cachedValue)
  {
    Chors_File::writeAsString($this->filename, $cachedValue, true, false, false);
    $this->age = 0;
  }

  /**
   * Starts output buffer caching. 
   */
  public function startOutputCaching()
  {
    ob_start();
  }

  /**
   * Ends caching, returns intercepted output and flush it to output buffer
   * @return string 
   */
  public function endOutputCachingFlush()
  {
    $data = ob_get_contents();
    $this->saveRender($data);
    ob_end_flush();
    return $data;
  }

  /**
   * Ends caching and returns intercepted output (Do not write anything to output buffer)
   * @return string 
   */
  public function endOutputCachingClean()
  {
    $data = ob_get_contents();
    $this->saveRender($data);
    ob_end_clean();
    return $data;
  }

  /**
   * Cache results of public argument-less static method of desired class
   * @param string $className
   * @param string $methodName
   * @param int $cacheTime time in seconds
   * @return mixed Result of calling $className::$methodName
   */
  public static function cacheStaticMethodResults($className, $methodName, $cacheTime)
  {
    $cache = new Chors_Cache(Array("datasource" => true, "time" => (int) $cacheTime),
            "ChorsCache." . $className . "." . $methodName);
    if ($cache->check())
      return $cache->fetchDataSource();

    $ret = call_user_func($className . "::" . $methodName);
    $cache->saveDataSource($ret);

    return $ret;
  }
}
