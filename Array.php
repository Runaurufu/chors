<?php
/**
 * Array class for Chors.
 * @package Chors
 * @version 1.0
 * @since 1.0
 * @author Runaurufu
 */
class Chors_Array
{
  static public function implode($array, $glue, $prefix = '', $postfix = '', $dataType = '%s')
  {
    $str = "";
    if(count($array) == 0)
      return $str;

    foreach ($array as $value)
    {
      $str .= sprintf("%s".$dataType."%s%s", $prefix, $value, $postfix, $glue); 
    }
    $str = substr($str, 0, strlen($str)- strlen($glue));

    return $str;
  }
  
  /**
   * Return array of referenced values
   * @param mixed[] $array
   * @return mixed[]
   */
  static public function getValuesByReferences($array)
  {
    //if (strnatcmp(phpversion(),'5.3') >= 0) //Reference is required for PHP 5.3+
    {
      $refs = array();
      foreach($array as $key => $value)
        $refs[$key] = &$array[$key];
      return $refs;
    }
    return $array;
  }
  
  /**
   * Get values existing both in $array1 and $array2 in order of appeareance of $array1.
   * @param mixed[] $array1
   * @param mixed[] $array2
   * @param delegate $comparisonMethod delegate: bool = (array, array)
   * @return mixed[]
   */
  static public function getIntersection($array1, $array2, $comparisonMethod = NULL)
  {
    $ret = array();
    
    foreach ($array1 as $value1)
    {
      foreach ($array2 as $value2)
      {
        if($comparisonMethod === NULL)
        {
          if($value1 === $value2)
          {
            $ret[] = $value1;
            break;
          }
        }
        else
        {
          if($comparisonMethod($value1, $value2))
          {
            $ret[] = $value1;
            break;
          }
        }
      }
    }
    
    return $ret;
  }
 
  /**
   * Get portion from array which is located between lower and higher key.
   * @param array $array
   * @param mixed $lowerKey
   * @param mixed $higherKey
   * @param boolean $includeLowerKey
   * @param boolean $includeHigherKey
   * @param boolean $keepKeys
   * @return array
   */
  static public function getBetween($array, $lowerKey, $higherKey, $includeLowerKey, $includeHigherKey, $keepKeys)
  {
    $result = array();
    
    $includeValue = FALSE;
    foreach ($array as $key => $value)
    {
      if($lowerKey === $key)
      {
        $includeValue = TRUE;
        
        if($includeLowerKey === FALSE)
          continue;
      }
      
      if($higherKey === $key)
      {
        $includeValue = FALSE;
        
        if($includeHigherKey === TRUE)
          $keepKeys ? $result[$key] = $value : $result[] = $value;
        break;
      }
      
      $keepKeys ? $result[$key] = $value : $result[] = $value;
    }
    return $result;
  }
  
  /**
   * 
   * @param array $array
   * @param delegate $conditionMethod delegate: bool = (array item), function($item){ return true; }
   * @param mixed $defaultValue value returned if method could not found proper data in array.
   * @param boolean $returnValue if set to FALSE then key is returned instead of value.
   * @return mixed
   */
  static public function getFirstOrDefault($array, $conditionMethod, $defaultValue = NULL, $returnValue = TRUE)
  {
    foreach ($array as $key => $value)
    {
      if($conditionMethod($value))
      {
        return $returnValue ? $value : $key;
      }
    }
    return $defaultValue;
  }
  
  /**
   * 
   * @param array $array
   * @param delegate $selectionMethod delegate: array = (array item), function($item){ return $item['a']; };
   * @param bool $keepKeys Should original array keys be kept.
   * @return array
   */
  static public function getSelection($array, $selectionMethod, $keepKeys)
  {
    $ret = array();
    
    foreach ($array as $key => $value)
    {
      $item = $selectionMethod($value);
      if($keepKeys)
        $ret[$key] = $item;
      else
        $ret[] = $item;
    }
    
    return $ret;
  }
  
  /**
   * Adds item as first item to array.
   * @param array $array
   * @param mixed $item
   */
  static public function addToStart(&$array, $item)
  {
    array_unshift($array, $item);
  }
  
  /**
   * Adds item as last item to array.
   * @param array $array
   * @param mixed $item
   */
  static public function addToEnd(&$array, $item)
  {
    $array[] = $item;
  }
}

/**
 * Chors_Collection class for Chors.
 * @package Chors
 * @version 1.0
 * @since 1.0
 * @author Runaurufu
 */
class Chors_Collection
{
  protected $array = array();
  
  public function __construct($array = NULL)
  {
    if(is_array($array))
      $this->array = $array;
  }
  
  public function add($item)
  {
    $this->array[] = $item;
  }
  
  public function remove($item)
  {
    foreach ($this->array as $key => $value)
    {
      if($value === $item)
      {
        unset ($this->array[$key]);
        return;
      }
    }
  }
  
  public function clear()
  {
    unset($this->array);
    $this->array = array();
  }

  public function count()
  {
    return count($this->array);
  }
}