# README #

# Index #

*  [Setting things up](#markdown-header-setting-things-up)
    * [Configuration file](#markdown-header-configuration-file)
    * [Chors_Auth](#markdown-header-chors_auth)
        * [Database tables](#markdown-header-database-tables)
* [Third party authorization](#markdown-header-third-party-authorization)
    * [Facebook](#markdown-header-facebook-support)

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
http://www.erulian.in.rs/English/Vuk1.html - Chors, Hors, Horz - God of Moon
* Version

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## Setting things up ##

* Summary of set up
* Configuration
* There are absolutelly no dependencies! None! Null! Just use PHP 5.2 or newer.
* Database configuration
* How to run tests
* Deployment instructions

### Configuration file
It is good practice to store all your application configuration type data in configuration file.
Configuration file should in most cases reside inside *application/config* path and be in *.ini* format.

Example config.ini content:
~~~~
[Database:Local]
appName = primary
type = mysqli
hostname = 127.0.0.1
username = root
password =
dbname = testBase
prefix =
debugfile = debug.txt
debugmode = true
[Database:Remote]
appName = primary
type = mysqli
hostname = 127.0.0.1
username = root
password = password
dbname = productionBase
prefix =
debugfile = 
debugmode = false
[Power]
isOn = true
~~~~
:Local and :Remote tags on group name marks configurations (release versions) which should be used in case of running website from local (:Local) or external machine (:Remote).
Groups without special markings are available for each release.

Following code allows loading config.ini file to $settings array and acquire specific release configuration data.
~~~~
$settings = Array();
Chors_Config::acquire("application/config/config.ini", $settings);
Chors_Config::acquireSpecificRelease($settings);
~~~~

### Chors_Auth ###
Chors_Auth is center of authorization of entire Chors. It can be used to handle all registration and verification process.

#### Database tables ####

Chors_Auth require some database tables for proper usage.
Their minimal columns content is as defined further, however they can be extended upon it as much as you like as long as all additional fields either allow NULL values or have DEFAULT value defined.

##### Account table #####
~~~~
Chors_Auth::setAccountTable($tableName, $idCol, $levelCol, $nameCol, $timestampCol, $localizationCol = null);
~~~~

Column name  | Column type | Description
------------- | ------------- | -------------
Id  | INT, AI, UQ | User Id
Level  | INT | User access level (value returned by Chors_Auth::getUserLevel())
Name  | VARCHAR | User name (value returned by Chors_Auth::getUserName())
RegistrationTimeStamp  | DATETIME | User register timestamp
LocalizationId  | INT | OPTIONAL, Language Id for localization used for that user

##### Session table #####
~~~~
Chors_Auth::setSessionTable($tableName, $idColumn, $accountIdColumn, $sidColumn, $startTimeColumn, $ipColumn);
~~~~

Column name  | Column type | Description
------------- | ------------- | -------------
Id  | INT, AI, UQ | Session Id
AccountId  | INT | User Id (session owner)
Sid  | VARCHAR, UQ | Session string
StartTimeStamp  | DATETIME | Session start timestamp
Ip  | VARCHAR | IP address of user when session started

##### Autologin table #####
~~~~
Chors_Auth::setAutologinTable($tableName, $idCol, $logIdCol, $alogCol, $timeCol, $cookieName);
~~~~

Column name  | Column type | Description
------------- | ------------- | -------------
Id  | INT, AI, UQ | Autologin Id
AccountId  | INT | User Id (autologin owner)
Key  | VARCHAR, UQ | Autologin string
CreationTimeStamp  | DATETIME | Autologin creation timestamp
Ip  | VARCHAR | IP address of user when session started

$cookieName is not used as a database storable value but as marker for autologin cookie name.

##### Log table #####
~~~~
Chors_Auth::setLogTable($tableName, $logIdCol, $idCol, $timeCol, $ipCol, $userAgentCol);
~~~~

Column name  | Column type | Description
------------- | ------------- | -------------
Id  | INT, AI, UQ | Log entry Id
AccountId  | INT | User Id (user who caused log to appear)
TimeStamp  | DATETIME | When log was entered
Ip  | VARCHAR | IP address of user
UserAgent  | VARCHAR | UserAgent value received from user

##### Localization table #####
~~~~
Chors_Auth::setLocalizationTable($tableName, $idCol, $nameCol);
~~~~

Column name  | Column type | Description
------------- | ------------- | -------------
Id  | INT, AI, UQ | Localization Id
Name  | VARCHAR | Name of localization

##### AuthPassword table #####
~~~~
Chors_Auth::setAuthPasswordTable($tableName, $idCol, $accountCol, $loginCol, $passCol, $timestampCol);
~~~~

Column name  | Column type | Description
------------- | ------------- | -------------
Id  | INT, AI, UQ | Id
AccountId  | VARCHAR | User Id
Login | VARCHAR | Logins
Password | VARCHAR | Passwords
TimeStamp | DATETIME | When login&Password combination was created

##### AuthFacebook table #####
~~~~
Chors_Auth::setAuthFacebookTable($tableName, $idCol, $accountCol, $facebookId);
~~~~

Column name  | Column type | Description
------------- | ------------- | -------------
Id  | INT, AI, UQ | Id
AccountId  | INT | User Id
FacebookId  | INT | Facebook account Id

## Contribution guidelines ##

* Writing tests
* Code review
* Other guidelines

## Who do I talk to? ##

* Please contact with me - Runaurufu via chors@runaurufu.com

## Included libraries & projects ##

* Support for webservice server is provided by https://code.google.com/p/php-wsdl-creator/
* Support for emails is provided by https://github.com/PHPMailer/PHPMailer

## Third party authorization ##

### Facebook support ###

To make Chors handling Facebook authorization you need to let it know your appId and appSecretKey.

#### Back-end handling ####
~~~~
Chors_Auth::setFacebookData($appId, $appSecretKey);
Chors_Auth::activateFacebookSupport();
Chors_Auth::verificate();
~~~~

#### Front-end handling ####
Paste this at the begining of you website body:
~~~~
<script>
window.fbAsyncInit = function() {
FB.init({
    appId: '<?php echo $appId; ?>',
cookie: true,
xfbml: true,
oauth: true
});
FB.Event.subscribe('auth.login', function(response) {
window.location.reload();
});
FB.Event.subscribe('auth.logout', function(response) {
window.location.reload();
});
};
(function() {
var e = document.createElement('script'); e.async = true;
e.src = document.location.protocol +
'//connect.facebook.net/en_US/all.js';
document.getElementsByTagName('head')[0].appendChild(e);
}());
</script>
~~~~

And use that for facebook login:
~~~~
<fb:login-button></fb:login-button>
~~~~