<?php

/**
 * Registry class for Chors.
 * @author Runaurufu
 */
class Chors_Registry
{

  private static $dataStorage = Array();

  /**
   * 
   * @param string $name access key for data in registry
   * @param string $filename file to load data from
   * @param bool $overwrite should existing data be overwritten
   * @return void
   */
  public static function setFromConfig($name, $filename, $overwrite = true)
  {
    if (isset(self::$dataStorage[$name]))
    {
      if (!$overwrite)
        return;
      unset(self::$dataStorage[$name]);
    }
    self::$dataStorage[$name] = Chors_Config::acquire($filename,
                    self::$dataStorage[$name]);
  }

  /**
   * 
   * @param string $name access key for data in registry
   * @param array $array data to set in registry
   * @param bool $overwrite should existing data be overwritten
   * @return void
   * @throws Chors_Exception
   */
  public static function setFromArray($name, $array, $overwrite = true)
  {
    if (isset(self::$dataStorage[$name]))
    {
      if (!$overwrite)
        return;
      unset(self::$dataStorage[$name]);
    }
    if (!is_array($array))
      throw new Chors_Exception("Array expected",
      Chors_Exception::ERR_GENERIC_WRDTTYPE);

    self::$dataStorage[$name] = $array;
  }

  /**
   * 
   * @param string $name access key
   * @return array
   * @throws Chors_Exception
   */
  public static function getArray($name = null)
  {
    if ($name === null)
      return self::$dataStorage;

    $numargs = func_num_args();
    $arg_list = func_get_args();

    $temp = self::$dataStorage;
    for ($i = 0; $i < $numargs; $i++)
    {
      if (isset($temp[$arg_list[$i]]))
        $temp = $temp[$arg_list[$i]];
      else
        throw new Chors_Exception("Specified variable not defined",
        Chors_Exception::ERR_GENERIC_VARNTDEF);
    }
    return $temp;
  }

}
