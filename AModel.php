<?php

/**
 * @author Runaurufu
 */
abstract class Chors_AModel
{

  public function __construct()
  {
    Chors_Logger::internalTrace("Model created: " . get_class($this));
  }

  public final static function getModelFilePath($modelName)
  {
    return Chors_Boot::$applicationDir . DIRECTORY_SEPARATOR . Chors_Boot::$modelDir . DIRECTORY_SEPARATOR . $modelName . ".php";
  }
  
  /**
   * Loads desired model via require once
   * @deprecated use either loadModel or loadAndCreateModel
   * @param string $modelName model which should be required
   * @return Chors_AModel Instance of loaded model or <i>null</i> if $create == false
   */
  public final static function load($modelName, $create = true)
  {
    if($create)
    {
      return self::loadAndCreateModel($modelName);
    }
    else
    {
      return self::loadModel($modelName);
    }
  }

  /**
   * Loads desired model via require once and creates it's instance
   * @param string $modelName model which should be required
   * @return Chors_AModel Instance of loaded model
   */
  public final static function loadAndCreateModel($modelName)
  {
    self::loadModel($modelName);
    return new $modelName();
  }

  /**
   * Loads desired model via require once
   * @param String $modelName model which should be required
   */
  public final static function loadModel($modelName)
  {
    $path = self::getModelFilePath($modelName);
    if (!Chors_File::exists($path))
    {
      throw new Chors_Exception(
        "Model not found",
        Chors_ErrorCodes::MODEL_NOT_FOUND,
        $path);
    }
    require_once $path;
  }

  /**
   * Defines way of caching this data source
   * @abstract
   */
  public abstract function cache($cacheSettings, $modelAction, $argumentList);
}
