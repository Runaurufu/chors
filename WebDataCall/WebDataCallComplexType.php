<?php

/**
 * 
 */
class WebDataCallComplexType
{
  /**
   * Type name.
   * @var string
   */
  public $name;
  
  /**
   * Description of type.
   * @var string
   */
  public $description;
  
  /**
   * List of fields defined within type.
   * @var WebDataCallVariable[] 
   */
  public $properties;
  
  /**
   * Creates WebDataCallComplexType from passed class name.
   * If it fails NULL is returned.
   * @param string $className
   * @return WebDataCallComplexType Type description of specified class.
   */
  public static function createFromClass($className)
  {
    if(class_exists($className) === FALSE)
      return NULL;
    
    $instance = new $className();
    
    return self::createFromClassInstance($instance);
  }
  
  /**
   * Creates WebDataCallComplexType from passed class instance.
   * If it fails NULL is returned.
   * @param mixed $instance
   * @return WebDataCallComplexType Type description of specified class.
   */
  public static function createFromClassInstance($instance)
  {
    if($instance === NULL)
      return NULL;

    $type = new WebDataCallComplexType();
    $type->name = get_class($instance);
    
    $reflection = new ReflectionClass($instance);
    
    // Obtain class parsed doc
    $doc = $reflection->getDocComment();
    $pd = Chors_ParsedDoc::createFromDoc($doc);
    
    $type->description = $pd->description;
        
    $type->properties = array();
    /* @var $propertiesReflections ReflectionProperty[] */
    $propertiesReflections = $reflection->getProperties();
           
    foreach ($propertiesReflections as $propertyReflection)
    {
      // Obtain properties parsed doc
      $doc = $propertyReflection->getDocComment();
      $pd = Chors_ParsedDoc::createFromDoc($doc);
      
      $prop = new WebDataCallVariable();
      $prop->initializeInternalType($pd->var);
      $prop->name = $propertyReflection->name;
      $prop->description = $pd->description;
            
      $type->properties[] = $prop;
    }
    
    return $type;
  }
}
