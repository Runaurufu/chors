<?php

require_once 'WebDataCallComplexType.php';
require_once 'WebDataCallVariable.php';
require_once 'WebDataCallMethodDescription.php';
require_once 'WebDataCallRequest.php';
require_once 'WebDataCallResponse.php';

/**
 * 
 */
class WebDataCallApiDescription
{
  /**
   * Collection of api available methods.
   * @var WebDataCallMethodDescription[] 
   */
  public $methods;
  /**
   * Collection of api available complex types.
   * @var WebDataCallComplexType[] 
   */
  public $dataTypes;
}

abstract class WebDataCall
{
  private $endpointServerName;
  private $endpointRedirectUrl;
  private $endpointUrl;
  
  private $methodsDescriptions = NULL;
  private $typesDescriptions = NULL;
  
  public $inputDataCallback = null;
  public $outputDataCallback = null;
  
  /**
   * This field will be filled in case of log being requested.
   * @var string
   */
  public $currentLog = null;

  function __construct()
  {
    // Default endpoint configuration
    $protocol = (isset($_SERVER['HTTPS'])&&$_SERVER['HTTPS']=='on')?'https':'http';
    $endpoint = $protocol.'://'.$_SERVER['SERVER_NAME'];
    
    if(isset($_SERVER['SERVER_PORT']))
    {
      if($protocol === "https" && $_SERVER['SERVER_PORT'] != 443
        || $protocol === "http" && $_SERVER['SERVER_PORT'] != 80)
        $endpoint .= ':'.$_SERVER['SERVER_PORT'];
    }
    
    //@TODO it should be better default fit...
    $this->setEndPointUrl($endpoint);//.$_SERVER['SCRIPT_NAME'];
  }
  
  /**
   * Allows setting url which acts as a base for all WebDataCall handling ex.: http://127.0.0.1/WebDataCall/
   * @param string $endpointUrl
   */
  public function setEndPointUrl($endpointUrl)
  {
    $endpointUrl = str_replace('\\', '/', $endpointUrl);
    $this->endpointUrl = $endpointUrl;
    
    $items = explode('/', $endpointUrl);
    
    $this->endpointServerName = implode('/', array_slice($items, 0, 3));
    $this->endpointRedirectUrl = '/'.implode('/', array_slice($items, 3));
  }

  /**
   * Retrieve descripion info of all web-callable methods.
   * @return WebDataCallMethodDescription[]
   */
  public function getWebMethodDescriptions()
  {
    if($this->methodsDescriptions === NULL)
    {
      $this->methodsDescriptions = $this->__acquireWebMethodDescriptions();
    }
    return $this->methodsDescriptions; 
  }
  
  /**
   * Describes api methods and types.
   * @return WebDataCallApiDescription
   */
  private function index()
  {
    $ret = new WebDataCallApiDescription();
    $ret->methods = $this->getWebMethodDescriptions();
    $ret->dataTypes = $this->getTypesDescriptions();
    return $ret;
  }
  
  /**
   * 
   * @return WebDataCallMethodDescription[]
   */
  private function __acquireWebMethodDescriptions()
  {
    $reflection = new ReflectionClass($this);
    $methods = $reflection->getMethods(ReflectionMethod::IS_PUBLIC);
    $descriptions = array();
    
    // fill special methods
    $special = new WebDataCallMethodDescription();
    $special->name = "index";
    $special->description = "Describe available methods";
    $special->parameters = array();
    $special->returnType = new WebDataCallVariable();
    $special->returnType->description = "Description of available methods";
    $special->returnType->isArray = TRUE;
    $special->returnType->type = new WebDataCallDataType(WebDataCallDataType::COMPLEX);
    $special->returnType->typeData = "WebDataCallApiDescription";
    $descriptions[$special->name] = $special;
    
    /* @var $method ReflectionMethod */
    foreach ($methods as $method)
    {
      if($method->isConstructor() || $method->isDestructor() || $method->isStatic())
        continue;
     
      if($method->class === __CLASS__)
        continue;
     
      $desc = new WebDataCallMethodDescription();
      $desc->name = $method->name;
     
      if(Chors_String::contains($desc->name, "_"))
        continue;
     
      if($desc->parseFromDocumentation($method->getDocComment()))
      {
        $descriptions[$desc->name] = $desc;
      }
    }
    
    return $descriptions;
  }
  
  /**
   * Retrieve descripion info of all web-callable complex data types.
   * @return WebDataCallComplexType[]
   */
  public function getTypesDescriptions()
  {
    if($this->typesDescriptions === NULL)
    {
      $this->typesDescriptions = $this->__acquireTypesDescriptions();
    }
    return $this->typesDescriptions; 
  }
  
  /**
   * 
   * @return WebDataCallComplexType[]
   */
  private function __acquireTypesDescriptions()
  {
    $typeNames = array();
    
    $methods = $this->getWebMethodDescriptions();
    
    foreach ($methods as $method)
    {
      if($method->returnType->type->equals(WebDataCallDataType::COMPLEX))
      {
        $typeNames[] = $method->returnType->typeData;
      }

      foreach ($method->parameters as $param)
      {
        if($param->type->equals(WebDataCallDataType::COMPLEX))
        {
          $typeNames[] = $param->typeData;
        }
      }
    }
    
    $typesDescriptions = array();
    
    for ($i = 0; $i < count($typeNames); $i++)
    {
      if(isset($typesDescriptions[$typeNames[$i]]) === FALSE)
      {
        $className = $typeNames[$i];
        if(class_exists($className))
          $description = WebDataCallComplexType::createFromClass($className);
        else
        {
          $instance = $this->__getClassInstanceByClassName($className);
          if($instance === NULL)
            continue;
          $description = WebDataCallComplexType::createFromClassInstance($instance);
        }
        $typesDescriptions[$typeNames[$i]] = $description;
        
        foreach ($description->properties as $prop)
        {
          if($prop->type->equals(WebDataCallDataType::COMPLEX))
          {
            $typeNames[] = $prop->typeData;
          }
        }
      }
    }
    
    return $typesDescriptions;
  }
  
  /**
   * @param string $className Class of which instance we want to get.
   * @return mixed Retrieve instance of given className or NULL if it was not found.
   */
  abstract function __getClassInstanceByClassName($className);

  /**
   * Server will handle awaiting request call.
   * @return mixed Returns TRUE/FALSE if requested respond format is not WebDataCallRespondFormat::VARIABLE.
   * If it is however then output of called method is returned (in this mode also any exception can be thrown out).
   */
  public function handleWebCall()
  {
    $input = file_get_contents("php://input");
    
    // if we get any send input then we need to process it.
    if(strlen($input) > 0)
    {
      if($this->inputDataCallback !== null)
      {
        $input = $this->inputDataCallback($input);
      }
    }
    
    // initialize request from input data
    $request = $this->__getInputInitializedRequest($input);
    
    $response = new WebDataCallResponse();
    
    try
    {
      try
      {
        ob_start();
        $response->response = $this->__handleRequest($request);
      }
      finally
      {
        $response->raw = ob_get_contents();
        ob_end_clean();
      }
      
      if($request->respondFormat->equals(WebDataCallRespondFormat::VARIABLE))
      { 
        // BE AWARE: VARIABLE RESPONSES LOSES OUTTED PARAMETERS
        return $response->response->result;
      }
    }
    catch (Exception $ex)
    {
      // VARIABLE responses does not only "NORMALLY" process exceptions but also throws them.
      if($request->respondFormat->equals(WebDataCallRespondFormat::VARIABLE))
      {
        if($ex instanceof WebDataCallException)
        {
          throw $ex;
        }
        else
        {
          throw new WebDataCallException("Exception occurred during request handling!", -2, $ex);
        }
      }
      else
      {
        $response->error = WebDataCallError::CreateFromException($ex);
      }
    }
    
    if($request->respondFormat->equals(WebDataCallRespondFormat::RAW))
    {
      if($this->outputDataCallback !== NULL)
      {
        $this->outputDataCallback($response->raw);
      }
      
      echo $response->raw;
    }
    
    $this->__processOutput($request, $response);
    
    return $response->error !== NULL;
  }
  
  /**
   * 
   * @param SimpleXMLElement $xmlElement
   * @return array Unpacked XML Element
   */
  private function __unpackXmlElementToArray($xmlElement)
  {
    $array = array();

    foreach ($xmlElement as $name => $element) {
        ($node = & $array[$name])
            && (1 === count($node) ? $node = array($node) : 1)
            && $node = & $node[];

        $node = $element->count() ? $this->__unpackXmlElementToArray($element) : trim($element);
    }

    return $array;
  }
  
  /**
   * Initialize request state from passed inputData string
   * which may comes from php://input.
   * All not passed data are taken from global arrays
   * either from Chors params or $_GET, $_POST, etc. 
   * @param string $inputData
   * @return WebDataCallRequest inputData initialized request
   */
  private function __getInputInitializedRequest($inputData)
  { 
    $request = new WebDataCallRequest();
    
    $request->requestId = isset($_SERVER['X-Request-ID']) ? $_SERVER['X-Request-ID'] : NULL;
    $request->correlationId = isset($_SERVER['X-Correlation-ID']) ? $_SERVER['X-Correlation-ID'] : NULL;
        
    $parameters = array();
    
    $outputFormat = NULL;
    
    $inputParsed = FALSE;
    // Check if there is anything to parse...
    if($inputData !== NULL && strlen($inputData) > 0)
    {
      // This block is needed to prevent internal WARNINGS/ERRORS from interrupting application flow.
      $previous = libxml_use_internal_errors(true);
      try
      {
        $xml = simplexml_load_string($inputData);
        
//  if we ever need these gor log handling or something
//        foreach(libxml_get_errors() as $error)
//        {
//            error_log('Error parsing XML file ' . $file . ': ' . $error->message);
//        }
        
      } 
      finally
      {
        libxml_use_internal_errors($previous);
      }
      
      if($xml !== FALSE)
      {
        $request->requestedMethod = (string)$xml->request->method;
        
        foreach ($xml->request->parameters->children() as $value)
        {
          $pName = $value->getName();
          
          if($value->count() === 0)
            $pValue = (string)$value;
          else
            $pValue = $this->__unpackXmlElementToArray($value);
          $parameters[$pName] = $pValue;
        }

        $outputFormat = strtoupper((string)$xml->outputFormat);
        
        $inputParsed = TRUE;
      }
      else 
      {
        //$inputData = utf8_encode($inputData);
        //remove padding
        //$inputData=preg_replace('/.+?({.+}).+/','$1',$inputData); 
        
        $json = json_decode($inputData, true);

        if (json_last_error() === JSON_ERROR_NONE)
        {
          $request->requestedMethod = $json['request']['method'];
          
          foreach ($json['request']['parameters'] as $key => $value)
          {
            $pName = $key;
            $pValue = $value;
            $parameters[$pName] = $pValue;
          }
          
          $outputFormat = strtoupper($json['outputFormat']);

          $inputParsed = TRUE;
        }
      }
    }
    if($inputParsed === TRUE)
    {
      if(WebDataCallRespondFormat::containsName($outputFormat) === FALSE)
      {
        throw new WebDataCallException("Unrecognized output format!", -1);
      }
      
      $request->respondFormat = WebDataCallRespondFormat::convertToObject($outputFormat);
    }
    else
    {
      $requestedUriData = str_ireplace($this->endpointRedirectUrl, '', $_SERVER['REQUEST_URI']);

      $requestedUriData = str_replace("&", "/", $requestedUriData);
      $requestedUriData = str_replace("?", "/", $requestedUriData);
      $requestedUriData = explode("/", $requestedUriData);

      $count = count($requestedUriData);
      
      // Handle endpoint compatibility special filenames
      if($count > 0)
      {
        $file = strtoupper($requestedUriData[0]);
        if($file === "INDEX.PHP" || $file === "INDEX.HTML")
        {
          $requestedUriData = array_slice ($requestedUriData, 1);
          $count = count($requestedUriData);
        }
      }
      
      if($count > 0)
      {
        $response = strtoupper($requestedUriData[0]);

        if(WebDataCallRespondFormat::containsName($response))
        {
          $request->respondFormat = WebDataCallRespondFormat::convertToObject($response);
        }

        if($count > 1)
        {
          $request->requestedMethod = $requestedUriData[1];
        }
        
//        for ($i = 1; $i < $count; $i++)
//        {
//          $index = strpos($requestedUriData[$i], '=');
//          if($index !== FALSE)
//          {
//            $key = substr($requestedUriData[$i], 0, $index);
//            $value = substr($requestedUriData[$i], $index + 1);
//            
//            $request->parameters[$key] = $value;
//          }
//        }
      }
      
      // add $_GET array data
      foreach ($_GET as $key => $value)
      {
        $parameters[$key] = $value;
      }
      
      // add $_POST array data
      foreach ($_POST as $key => $value)
      {
        $parameters[$key] = $value;
      }
      
      // @TODO Handle FILES global array
    }
    
    $descs = $this->getWebMethodDescriptions();
    if(isset($descs[$request->requestedMethod]) === FALSE)
    {
      throw new WebDataCallException("Requested method (" . $request->requestedMethod . ") is not defined!", -1);
    }

    $methodDescription = $descs[$request->requestedMethod];

    foreach ($methodDescription->parameters as $parameterDefinition)
    {
      if(isset($parameters[$parameterDefinition->name]) === FALSE)
      {
        print_r($parameters);
        throw new WebDataCallException("Parameter ".$parameterDefinition->name." not provided!", -1);
      }
      
      $paramValue = $this->__getObjectFromInput($parameterDefinition, $parameters[$parameterDefinition->name]);
      $request->parameters[$parameterDefinition->name] = $paramValue;
    }

    return $request;
  }
  
  
  /**
   * Parses passed string or array into proper type.
   * @param WebDataCallVariable $parameterDefinition
   * @param mixed $paramInput
   * @return mixed
   */
  public function __getObjectFromInput($parameterDefinition, $paramInput)
  {
    if($parameterDefinition->isArray)
    {
      if(is_array($paramInput) === FALSE)
      {
        throw new WebDataCallException("Parameter was expected to be an array but it is not!", 2);;
      }
      
      $array = array();
      
      foreach ($paramInput as $item)
      {
        $array[] = $this->__parseToSingleItemObjectType($parameterDefinition, $item);
      }
      
      return $array;
    }
    return $this->__parseToSingleItemObjectType($parameterDefinition, $paramInput);
  }
  
  /**
   * 
   * @param WebDataCallVariable $parameterDefinition
   * @param mixed $param
   * @return mixed
   */
  private function __parseToSingleItemObjectType($parameterDefinition, $param)
  {
    switch ($parameterDefinition->type->getValue())
    {
      case WebDataCallDataType::BOOL:
        if(is_bool($param))
          return $param;
        $ret = Chors_String::boolval($param);
        if($ret !== NULL)
          return $ret;
        break;
        
      case WebDataCallDataType::ENUM:
        throw new Chors_NotImplementedException("Enum support is not yet implemented!");
        //@TODO!!!!
        break;
      
      case WebDataCallDataType::BYTE:
      case WebDataCallDataType::INT16:
      case WebDataCallDataType::INT32:
      case WebDataCallDataType::INT64:
      case WebDataCallDataType::UINT16:
      case WebDataCallDataType::UINT32:
      case WebDataCallDataType::UINT64:
        if(is_int($param))
          return $param;
        if(is_string($param))
          return intval($param);
        break;
        
      case WebDataCallDataType::FLOAT:
      case WebDataCallDataType::DOUBLE:
        if(is_float($param))
          return $param;
        if(is_string($param))
          return floatval($param);
        break;
        
      case WebDataCallDataType::DECIMAL:
        if(is_float($param))
          return $param;
        if(is_string($param))
          return floatval($param);
        break;
        
      case WebDataCallDataType::STRING:
        if(is_string($param))
          return $param;
        break;
        
      case WebDataCallDataType::UUID:
        if($param instanceof Chors_UUID)
          return $param;
        return Chors_UUID::createFromBinary($param);
        
      case WebDataCallDataType::BINARY:
        if($param instanceof Chors_Blob)
          return $param;
        return Chors_Blob::createFromBase64String($param);
      
      case WebDataCallDataType::DATETIME:
        return Chors_DateTime::create($param);
        break;
      
      case WebDataCallDataType::TIMESPAN:
        throw new Chors_NotImplementedException("Timespan support is not yet implemented!");
        //@TODO!!!!
        break;

      case WebDataCallDataType::COMPLEX:
        
        $typesDescriptions = $this->getTypesDescriptions();
        
        if(isset($typesDescriptions[$parameterDefinition->typeData]) === FALSE)
        {
          throw new WebDataCallException("Passed parameter type (".$parameterDefinition->typeData.") do not have type description!", 13);
        }
        
        if(is_array($param) === FALSE)
        {
          throw new WebDataCallException("Passed parameter input data (".$parameterDefinition->name.") is not an array!", 18);
        }
        
        $typeDescription = $typesDescriptions[$parameterDefinition->typeData];
        
        $instance = new $typeDescription->name();
        
        foreach ($typeDescription->properties as $property)
        {
          if(isset($param[$property->name]) === FALSE)
          {
            throw new WebDataCallException("Passed parameter input data (".$parameterDefinition->name.") does not have key for property ".$property->name."!", 18);
          }
          
          $instance->{$property->name} = $this->__getObjectFromInput($property, $param[$property->name]);
        }
        
        return $instance;
    }

    return $param;
  }
  
  /**
   * Process request output and handles its formating and so.
   * @param WebDataCallRequest $request
   * @param WebDataCallResponse $response
   */
  private function __processOutput($request, $response)
  {
    if($this->outputDataCallback !== null)
    {
      ob_start();
    }
    
    if($request->requestId !== NULL)
    {
      header('X-Request-ID: '.$request->requestId);
    }
    
    if($request->correlationId !== NULL)
    {
      header('X-Correlation-ID: '.$request->correlationId);
    }

    $returnedObject = NULL;
    
    if($response->error !== NULL)
    {
      Chors_Boot::httpResponseCode(500);
      $returnedObject = $response->error;
    }
    else
    {
      Chors_Boot::httpResponseCode(200);
      $returnedObject = $response->response;
    }
    
    if($request->respondFormat->equals(WebDataCallRespondFormat::XML))
    {
      header('Content-Type: application/xml');
      echo Chors_XML::convertToSimpleXml($returnedObject)->asXML();
    }
    else if($request->respondFormat->equals(WebDataCallRespondFormat::JSON))
    {
      header('Content-Type: application/json');
      echo json_encode($returnedObject);
    }
    else if($request->respondFormat->equals(WebDataCallRespondFormat::HTML))
    {
      // @todo make proper HTML
      echo "<PRE>";
      print_r($returnedObject);
      echo "</PRE>";
    }
    
    if($this->outputDataCallback !== null)
    {
      $this->outputDataCallback(ob_get_contents());
      ob_end_clean();
    }
  }
    
  /**
   * Handle specified request
   * @param WebDataCallRequest $request
   * @return WebDataCallResponseObject result of request call.
   */
  private function __handleRequest($request)
  {
    if($request->requestedMethod === NULL || strlen($request->requestedMethod) === 0)
    {
      throw new WebDataCallException("Requested method name was not provided!", 1);
    }

    if(method_exists($this, $request->requestedMethod) === FALSE)
    {
      throw new WebDataCallException("WebDataCall class does not contain requested method named: " . $request->requestedMethod, 2);
    }
    
    // BE AWARE: CURRENTLY PARAMETERS ARE NOT SUPPORTED FOR PASSING OUT OF METHOD
    $ret = call_user_func_array(array($this, $request->requestedMethod), $request->parameters);
    
    $responseObject = new WebDataCallResponseObject();
    $responseObject->result = $ret;

    //$return = $this->{$request->requestedMethod}();
    // just call method and return its output.
    
    // All log/error handling is done outside!
    return $responseObject;
  }
}

/**
 * Chors_NotSupportedException is Chors_Exception class.
 * Please use it for all not supported functionalities.
 */
class WebDataCallException extends Exception
{
  /**
   * 
   * @param string $message
   * @param int $errorCode
   * @param Exception $innerException
   */
  public function __construct($message = "", $errorCode = 0, $innerException = NULL)
  {
    parent::__construct($message, $errorCode, $innerException);
  }
}



/**
 * Indicate all supported WebDataCall types.
 * Be aware that not all types are going to be supported at each server.
 */
class WebDataCallDataType extends Chors_Enum
{
  /**
   * No data. Should be used only as return type.
   */
  const VOID = 0;
  const BOOL = 1;
  const ENUM = 2;
  const BYTE = 3;
  const INT16 = 4;
  const INT32 = 5;
  const INT64 = 6;
  const UINT16 = 7;
  const UINT32 = 8;
  const UINT64 = 9;
  const FLOAT = 10;
  const DOUBLE = 11;
  const DECIMAL = 12;
  const STRING = 13;
  const UUID = 14;
  const BINARY = 15;
  /**
   * Int values are treated as TIMESTAMP, String values are converted.
   */
  const DATETIME = 16;
  /**
   * Defined as number of seconds. Accepts decimal part.
   */
  const TIMESPAN = 17;
  /**
   * User defined data type handling.
   */
  const COMPLEX = 100;
  /**
   * Specifies type which was not recognized.
   */
  const UNDEFINED = 200;
}

class WebDataCallRespondFormat extends Chors_Enum
{
  /**
   * Output will be presented in XML format.
   */
  const XML = 1;
  /**
   * Output will be presented in JSON format.
   */
  const JSON = 2;
  /**
   * Output will be presented in Web UI user-friendly format.
   */
  const HTML = 3;
  /**
   * Any output from request will be returned in form of variable.
   * In case of any error exception will be thrown.
   * Use this option if you want to locally use webCall methods.
   */
  const VARIABLE = 4;
  
  /**
   * Only console output will be provided in response to method call.
   */
  const RAW = 5;
}
