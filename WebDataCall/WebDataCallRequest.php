<?php
/**
 * Description of WebDataCallRequest
 *
 * @author Runaurufu
 */
class WebDataCallRequest
{
  /**
   * Method name which was requested.
   * @var string
   */
  public $requestedMethod = "index";
  
  /**
   * Method parameters which were received as an input.
   * @var array[name] = value 
   */
  public $parameters = array();
  
  /**
   * Format in which respond should be returned.
   * @var WebDataCallRespondFormat 
   */
  public $respondFormat;
  
  /**
   * Each request can send also request token.
   * If such token is send it will also be included in call response.
   * @var string 
   */
  public $requestId = NULL;
  
  /**
   * Each request can send also request token.
   * If such token is send it will also be included in call response.
   * @var string 
   */
  public $correlationId = NULL;
 
  public function __construct()
  {
    $this->respondFormat = new WebDataCallRespondFormat(WebDataCallRespondFormat::XML);
  }
}
