<?php

/**
 * Describes variable used. Applicable both in methods and classes description.
 */
class WebDataCallVariable
{
  /**
   * Parameter name.
   * @var string
   */
  public $name;
  
  /**
   * Description of parameter.
   * @var string
   */
  public $description;
  
  /**
   * Parameter type.
   * @var WebDataCallDataType
   */
  public $type;
  
  /**
   * Extended type data if needed.
   * @var string 
   */
  public $typeData;
  
  /**
   * Informs if parameter is requesting array of specified types.
   * @var boolean 
   */
  public $isArray;
    
  /**
   * Parses string representation of type into WebCataCallDataType recognized type.
   * @param string $type
   */
  public function initializeInternalType($type)
  {
    if(Chors_String::endsWith($type, "[]"))
    {
      $this->isArray = TRUE;
      $type = substr($type, 0, strlen($type) - 2);
    }
    else
    {
      $this->isArray = FALSE;
    }
      
//  const ENUM = 2;
    if(Chors_String::equals($type, "void", FALSE))
    {
      $this->type = new WebDataCallDataType(WebDataCallDataType::VOID);
    }
    elseif(Chors_String::equals($type, "bool", FALSE) || Chors_String::equals($type, "boolean", FALSE))
    {
      $this->type = new WebDataCallDataType(WebDataCallDataType::BOOL);
    }
    elseif(Chors_String::equals($type, "int", FALSE) || Chors_String::equals($type, "integer", FALSE))
    {
      $this->type = new WebDataCallDataType(WebDataCallDataType::INT32);
    }
    elseif(Chors_String::equals($type, "float", FALSE))
    {
      $this->type = new WebDataCallDataType(WebDataCallDataType::FLOAT);
    }
    elseif(Chors_String::equals($type, "decimal", FALSE))
    {
      $this->type = new WebDataCallDataType(WebDataCallDataType::DECIMAL);
    }
    elseif(Chors_String::equals($type, "string", FALSE))
    {
      $this->type = new WebDataCallDataType(WebDataCallDataType::STRING);
    }
    elseif(Chors_String::equals($type, "uuid", FALSE) || Chors_String::equals($type, "Chors_UUID", FALSE))
    {
      $this->type = new WebDataCallDataType(WebDataCallDataType::UUID);
    }
    elseif(Chors_String::equals($type, "binary", FALSE) || Chors_String::equals($type, "Chors_Blob", FALSE))
    {
      $this->type = new WebDataCallDataType(WebDataCallDataType::BINARY);
    }
    elseif(Chors_String::equals($type, "datetime", FALSE)
      || Chors_String::equals($type, "date", FALSE)
      || Chors_String::equals($type, "time", FALSE)
      || Chors_String::equals($type, "timestamp", FALSE)
      || Chors_String::equals($type, "Chors_DateTime", FALSE))
    {
      $this->type = new WebDataCallDataType(WebDataCallDataType::DATETIME);
    }
    elseif(Chors_String::equals($type, "timespan", FALSE))
    {
      $this->type = new WebDataCallDataType(WebDataCallDataType::TIMESPAN);
    }
    else
    {
      $this->typeData = $type;
      if(class_exists($type))
      {
        $this->type = new WebDataCallDataType(WebDataCallDataType::COMPLEX);
      }
      else
      {
        $this->type = new WebDataCallDataType(WebDataCallDataType::UNDEFINED);
      }
    }
  }
}
