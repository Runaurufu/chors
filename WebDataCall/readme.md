﻿# WebDataCall

## Index
* Abstract
* Supported Data Types
    * Supported Predefined Types
    * PHP Implementation Types
* Methods
    * API Restricted Methods
* Endpoint
* Input & Output
    * Input Data Structure
        * XML
        * JSON
        * GET & POST
    * Output Data Structure
        * XML
        * JSON
        * HTML
        * RAW


## Abstract ##
WebDataCall is a mean to retrieve data from remote location utilizing minimum level of formality required in connecting process.
WebDataCall is meant to be lightweight and flexible alternative for WSDL or REST in terms of server-side data manipulation.

* is easy to setup
* allow method calls not bound to specified type
* allow both XML and JSON data output
* accepts as input all of these: XML, JSON and also GET and POST requests
* is stateless (does not store client context on the server between requests)

## Supported Data Types
WebDataCall in generic abstract support both set of predefined types and also allows custom user defined types to be used.
Predefined types however means only that these types should be recognizable by all server implementations. This does not mean that they are being supported in exact form by each implementation however.

### Supported Predefined Types
* VOID - available only for return types
* BOOL
* ENUM - a set of possible options
* BYTE - 8 bytes integer
* INT16 - 16 bytes integer
* INT32
* INT64
* UINT16 - 16 bytes unsigned integer
* UINT32
* UINT64
* FLOAT
* DOUBLE
* DECIMAL
* STRING
* UUID - binary/string representation of 128 bytes integer
* BINARY - any binary data which should be base64 encoded for transportation
* DATETIME
* TIMESPAN

On top of that WebDataCall supports also ***ARRAY*** type of any supported type (both predefined and user defined).

### PHP Implementation Types ###
In case of PHP WebDataCall implementation all **INT** and **UINT** types are considered as generic **INT**s and depending on server OS used interpreted as **INT32** or **INT64**.

Same applies for **FLOAT**, **DOUBLE** and **DECIMAL** - they all are considered **FLOAT**s.

**BINARY** data can be passed not only via default input methods but also via ***$_FILES*** array.

In case of **DATETIME** PHP takes its own liberty to interpret **INT**s as being Date in **TIMESTAMP** format (seconds from epoch). Also all no time zone specified data are considered to be UTC.

**TIMESPAN** same as **DATETIME** bases on seconds.

## Methods ##
Methods exposed by WebDataCall must comply with following rules:
* Method name must start from letter
* Method name can contain only ASCII letters [a-zA-Z] and numbers [0-9]
* Method name cannot be same as any of restricted method names

### API Restricted Methods ###
For proper functioning WebDataCall reserves (rather small) set of method names for facilitation of proper handling requests.

Reserved names are as follows:
* index - this method lists all available methods and custom data types defined for WebDataCall server instance

## Endpoint ##
WebDataCall supports all available operations from single endpoint therefore it is enough to provide user URL for your WebDataCall server like `http://127.0.0.1/MyOwnApi/`.
For compatibility reasons (in case of servers where URL rewriting is not possible) WebDataCall implementation should support also all calls passed into /index.html or /index.php URLs (like `http://127.0.0.1/MyOwnApi/index.php`).

## Input & Output ##
WebDataCall supports multiple ways of interaction with server itself. What is important here is that way user chooses for input data format does not determine output data format. This mean that you can send data to WebDataCall server in XML format and retrieve them as JSON data.

### Input Data Structure ###

#### XML ####
```xml
<root>
    <request>
        <method>MethodName</method>
        <parameters>
            <parameterName1>ParameterValue</parameterName1>
            <parameterName2>ParameterValue</parameterName2>
            <parameterName3>ParameterValue</parameterName3>
        </parameters>
    </request>
    <outputFormat>HTML|XML|JSON|RAW</outputFormat>
</root>
```

#### JSON ####
```json
{
    "request" :
    {
        "method" : "MethodName",
        "parameters" :
        {
            "parameterName1" : "ParameterValue",  
            "parameterName2" : "ParameterValue",
            "parameterName3" : "ParameterValue"
        }
    },
    "outputFormat": HTML|XML|JSON|RAW
}
```

#### GET & POST ####
Be aware that using this way of calling library you should use URL to choose both output format and method which you want to call: `http://WebDataCallServerAddress/OutputFormat/MethodToCall/getParamName=paramValue`
if output format is not provided then WebDataCall will assume that **HTML** was requested.
if method is not provided then WebDataCall will assume that **INDEX** special method was requested.

All parameters are delivered via global FILES, POST and GET arrays (in that order).

### Output Data Structure ###

When no error was encountered server returns with http code 200.
Any other http code should be threated as an error and handled appropriately.

#### XML & JSON ####

Both XML and JSON output types wrap serialized response into presented structure.

Response is divided into two parts - result and parameters array. Result contain method direct output (returned value) while parameters are way of supporting method arguments which may be passed out by reference (*in PHP: foo(&$bar), in C#: foo(ref bar), foo(out bar)*).

##### XML

```xml
<root>
    <result>result value (method return value)</result>
    <parameters>
        <parameterName1>ParameterValue</parameterName1>
        <parameterName2>ParameterValue</parameterName2>
        <parameterName3>ParameterValue</parameterName3>
    </parameters>
</root>
```

##### JSON #####

```json
{
    "result": "result value (method return value)",
    "parameters" :
    {
        "parameterName1" : "ParameterValue",  
        "parameterName2" : "ParameterValue",
        "parameterName3" : "ParameterValue"
    }
}
```

#### HTML ####

***HTML*** output type presents called method returned data in server-branded fashion.
This output is not recommended for returned data processing but it is provided for quick inspection of results.

#### RAW ####

Output supports also special *RAW* output. This output is only pure console output of called method.
This mean that it will not provide you with actual processed and returned data from called method.

#### Error Data Structure

When server returns with http code different than 200 client should assume some kind of error.
In such server will attempt to handle error output data and wrap it into structure matching one presented below. If however such data structure was not provided then client need to continue to operate as if such structure was provided and returns any error data it can (possible raw content as ***details*** field while keeping ***message*** one empty).

##### XML #####
```xml
<root>
    <type>Exception type.</type>
    <message>User friendly message.</message>
    <details>Stacktrace and other technical details of exception.</details>
</root>
```

##### JSON #####
```json
{
    "type": "Exception type.",
    "message": "User friendly message.",
    "details": "Stacktrace and other technical details of exception."
}
```

