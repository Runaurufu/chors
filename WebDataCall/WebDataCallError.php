<?php

/**
 * Class handling error reporting for cases when issues occur during performing calls.
 * 
 * @author Runaurufu
 */
class WebDataCallError
{
  /**
   *
   * @var string type of error encountered
   */
  public $type;
  
  /**
   *
   * @var string more detailed and "user friendly" description
   */
  public $message;
  
  /**
   *
   * @var string technical data. It may be stacktrace or any other applicable data.
   */
  public $details;
  
  /**
   * 
   * @param Exception $exception
   * @return WebDataCallError
   */
  public static function CreateFromException($exception)
  {
    $error = new WebDataCallError();
    
    if($exception === NULL)
    {
      $error->type = "unknown";
      $error->message = "unknown error occurred";
      $error->details =  "no exception provided";
    }
    else
    {
      $error->type = get_class($exception);
      $error->message = $exception->getMessage();
      $error->details = Chors_Exception::toString($exception);
    }
    
    return $error;
  }
}
