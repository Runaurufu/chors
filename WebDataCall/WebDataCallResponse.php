<?php
/**
 * Description of WebDataCallResponse
 *
 * @author Runaurufu
 */
class WebDataCallResponse
{
  /**
   *
   * @var WebDataCallResponseObject 
   */
  public $response;
  
  /**
   *
   * @var WebDataCallError 
   */
  public $error;
  
  /**
   *
   * @var string 
   */
  public $raw;
}

class WebDataCallResponseObject
{
  /**
   *
   * @var mixed 
   */
  public $result;
  
  /**
   * Method parameters which should be returned as an output.
   * @var array[name] = value 
   */
  public $parameters = array();
}
