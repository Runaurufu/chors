<?php

/**
 * 
 */
class WebDataCallMethodDescription
{
  /**
   * Method name.
   * @var string
   */
  public $name;
  
  /**
   * Description of method.
   * @var string
   */
  public $description;
  
  /**
   * Data returned from call.
   * @var WebDataCallVariable 
   */
  public $returnType;

  /**
   * Parameters of method.
   * @var WebDataCallVariable[] 
   */
  public $parameters;
  
  /**
   * Parses method description from provided documentation text.
   * @param string $doc
   * @return boolean
   */
  public function parseFromDocumentation($doc)
  {
    $parsedDoc = Chors_ParsedDoc::createFromDoc($doc);
    
    $this->description = $parsedDoc->description;
    
    $this->returnType = new WebDataCallVariable();
    if(isset($parsedDoc->return['type']))
    {
      $this->returnType->initializeInternalType($parsedDoc->return['type']);
    }
    else
    {
      $this->returnType->type = new WebDataCallDataType(WebDataCallDataType::VOID);
      $this->returnType->isArray = FALSE;
    }
    
    $this->parameters = array();

    if($parsedDoc->params !== NULL)
    {
      foreach ($parsedDoc->params as $docParam)
      {
        $param = new WebDataCallVariable();
        $param->initializeInternalType($docParam['type']);

        if(isset($docParam['name']))
        {
          $param->name = $docParam['name'];
        }
        
        if(isset($docParam['description']))
        {
          $param->description = $docParam['description'];
        }
        
        $this->parameters[] = $param;
      }
    }
    
    return true;
  }
}
