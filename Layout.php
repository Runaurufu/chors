<?php

/**
 * Chors layout and all displaying operations class.
 * It is initated every time you use layout, view or partial.
 * @package Chors
 * @version 1.0
 * @since 1.0
 * @author Runaurufu
 */
class Chors_Layout
{

  /** @var <String> by default it is loaded layout path */
  static private $path = null;

  /** @var <String> path to your view file */
  static private $contentPath = null;

  /** @var <Mixed> Array or single object used to send data from controllers into views */
  private $view;
  private static $instance;

  /**
   * Creates Layout object
   */
  public function __construct()
  {
    self::$instance = $this;
  }

  private static function getInstance()
  {
    if (self::$instance == null)
      new Chors_Layout ();
    return self::$instance;
  }

  /**
   * Perform check if specified layout file exists, if it does not than exception (Chors_ErrorCodes::LAYER_NOT_FOUND) is throw.
   * On success it saves layout location for future use.
   * @param <String> $layoutName layout filename which will be used in current context
   * this can be overwritten by another Chors_Layout::create() call.
   * @static
   */
  static public function create($layoutName)
  {
    if (!Chors_File::exists(Chors_Boot::$applicationDir . '/' . Chors_Boot::$layoutDir . "/" . $layoutName . ".phtml"))
      throw new Chors_Exception("Layout not found", Chors_ErrorCodes::LAYER_NOT_FOUND, Chors_Boot::$applicationDir . '/' . Chors_Boot::$layoutDir . "/" . $layoutName . ".phtml");
    self::$path = Chors_Boot::$applicationDir . '/' . Chors_Boot::$layoutDir . "/" . $layoutName . ".phtml";
  }

  /**
   * Clears previously defined layout. 
   */
  static public function clear()
  {
    self::$path = null;
  }

  /**
   * Sets view variable for all derivied layouts, partials and views usage.
   * @param mixed $viewdata Array or single element which will be accessible in all view contexts
   */
  public function setViewData($viewdata)
  {
    $this->view = $viewdata;
  }

  /**
   * Starts procedure of displaying layouts, views and partials.
   * It is self determinant if any layout file is already loaded or not.
   * @param string $viewPath view file location - this file will be rendered
   * inside current Layout (or if that is not defined - instead of it)
   */
  public function start($viewPath)
  {
    Chors_Logger::internalTrace("Layout started for: " . $viewPath);
    self::$contentPath = $viewPath;
    if (self::$path === null)
    {
      include self::$contentPath;
    }
    else
    {
      include self::$path;
    }
  }

  /**
   * Each usage of this method result in including desired view in method location.
   * If you haven't defined any layout for current view, than this method do nothing.
   */
  private function content()
  {
    if (self::$path !== null)
    {
      include self::$contentPath;
    }
  }
  
  /**
   * 
   */
  private function contentToString()
  {
    if (self::$path !== null)
    {
      ob_start();
      include self::$contentPath;
      $data = ob_get_contents();
      ob_end_clean();
      return $data;
    }
  }

  /**
   * Alias for strip_tags
   * @param string $string String which should be stripped of the HTML tags
   * @return string tags free String
   */
  private function escape($string)
  {
    return Chors_String::stripTags($string);
  }

  /**
   * Includes partial file located in partial directory of current project
   * @param string $pathName partial file name which should be included
   * @param bool $parse should included file be parsed or just displayed as is
   */
  private function partial($pathName, $parse = true)
  {
    if (!Chors_File::exists(Chors_Boot::$applicationDir . '/' . Chors_Boot::$partialDir . "/" . $pathName))
      throw new Chors_Exception("File not found", Chors_ErrorCodes::LAYER_NOT_FOUND, Chors_Boot::$applicationDir . '/' . Chors_Boot::$partialDir . "/" . $pathName);

    if ($parse)
    {
      include Chors_Boot::$applicationDir . '/' . Chors_Boot::$partialDir . "/" . $pathName;
    }
    else
    {
      flush();
      readfile(Chors_Boot::$applicationDir . '/' . Chors_Boot::$partialDir . "/" . $pathName);
    }
  }

  /**
   * Includes partial file located in partial directory of current project
   * @param string $pathName partial file name which should be included
   * @param bool $parse should included file be parsed or just displayed as is
   * @static
   */
  public static function includePartial($pathName, $parse = true)
  {
    self::getInstance()->partial($pathName, $parse);
  }

  /**
   * Allias for Chors::getBaseUrl()
   * @return string baseURL for current webpage
   * @see Chors::getBaseUrl()
   */
  private function getBaseUrl()
  {
    return Chors_Boot::getBaseUrl();
  }

  /**
   * Allias for Chors::getUrl()
   * @return string URL for current webpage (including rewrite characters like '?')
   * @see Chors::getUrl()
   */
  private function getUrl()
  {
    return Chors_Boot::getUrl();
  }

}
