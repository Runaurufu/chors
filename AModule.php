<?php

/**
 * Description of Module
 * @author Runaurufu
 * @todo redo modules as they should be childs of Chors_AModule and not need separate files for install/update/config...
 */
abstract class Chors_AModule
{
  protected $version;
  protected $name;
  protected $config;
  protected $installFile = "install.php";
  protected $updateFile = "update.php";
  protected $configFile = "config.ini";

  protected function __construct()
  {
    $this->loadConfig();
  }

  public function getModuleName()
  {
    return $this->name;
  }

  public function getModuleVersion()
  {
    return $this->version;
  }

  public function getInstallFileName()
  {
    return $this->installFile;
  }

  public function getUpdateFileName()
  {
    return $this->updateFile;
  }

  public function getConfigFileName()
  {
    return $this->configFile;
  }

  /**
   *  Perform module installation. It should be launched only once in the application lifetime.
   */
  public function install()
  {
    if (Chors_File::exists($this->installFile))
    {
      try
      {
        include $this->installFile;
      }
      catch (Chors_Exception $e)
      {
        if ($e->getCode() !== Chors_ErrorCodes::NO_ERROR)
        {
          throw $e;
        }
      }
    }
    else
    {
      throw new Chors_Exception(
        "Module install file not found",
        Chors_ErrorCodes::FILE_NOT_FOUND,
        $this->installFile);
    }
  }

  /**
   * Perform update of current module installation. Actions should be performed ONLY when version change.
   */
  public function update()
  {
    if (Chors_File::exists($this->updateFile))
    {
      try
      {
        include $this->updateFile;
      }
      catch (Chors_Exception $e)
      {
        if ($e->getCode() !== Chors_ErrorCodes::NO_ERROR)
        {
          throw $e;
        }
      }
    }
    else
    {
      throw new Chors_Exception(
        "Module update file not found",
        Chors_ErrorCodes::FILE_NOT_FOUND,
        $this->updateFile);
    }
  }

  protected function loadConfig()
  {
    $this->config = Chors_Config::acquire($this->configFile);
  }

  /**
   * Runs default behaviour of module.
   */
  abstract public function execute();

  /**
   * Loads desired module via require once
   * @param string $moduleName module which should be required
   */
  public final static function loadModule($moduleName)
  {
    if (!Chors_File::exists(Chors_Boot::$applicationDir . '/' . Chors_Boot::$moduleDir . "/" . $moduleName . "/init.php"))
    {
      throw new Chors_Exception(
        "Module not found",
        Chors_ErrorCodes::FILE_NOT_FOUND,
        Chors_Boot::$applicationDir . '/' . Chors_Boot::$moduleDir . "/" . $moduleName . "/init.php");
    }
    require_once Chors_Boot::$applicationDir . '/' . Chors_Boot::$moduleDir . "/" . $moduleName . "/init.php";
  }

  /**
   * Loads desired module via require once and creates its instance
   * @param string $moduleName module which should be required
   * @return Chors_AModule Instance of loaded module
   * @throws Chors_Exception
   */
  public final static function loadAndCreateModel($moduleName)
  {
    if (!Chors_File::exists(Chors_Boot::$applicationDir . '/' . Chors_Boot::$moduleDir . "/" . $moduleName . "/init.php"))
    {
      throw new Chors_Exception(
        "Module not found",
        Chors_ErrorCodes::FILE_NOT_FOUND,
        Chors_Boot::$applicationDir . '/' . Chors_Boot::$moduleDir . "/" . $moduleName . "/init.php");
    }
    require_once Chors_Boot::$applicationDir . '/' . Chors_Boot::$moduleDir . "/" . $moduleName . "/init.php";
    return new $moduleName();
  }
}
