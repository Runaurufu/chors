<?php

/**
 * Description of Controller
 * @author Runaurufu
 */
class Chors_Controller
{

  protected $view;
  protected $fview;
  protected $previous = null;
  protected $name = NULL;
  protected $currentAction;

  public function __construct()
  {
    $this->view = new stdClass();
    $this->fview = new stdClass();
    
    if($this->name === NULL)
    {
      $className = get_class($this);
      $this->name = substr($className, 0, strlen($className) - 10); // Controller -> 10 chars
    }
  }

  /**
   * init function is run at first controller usage.
   */
  protected function init()
  {
    ;
  }

  public function setCurrentAction($action)
  {
    $this->currentAction = $action;
    $this->init();
  }

  /**
   * @param string $viewname view.phtml file used to render webpage
   */
  protected function customView($viewname)
  {
    if (!Chors_File::exists(Chors_Boot::$applicationDir . '/' . Chors_Boot::$viewDir . "/" . $viewname . ".phtml"))
    {
      throw new Chors_Exception(
        "View not found",
        Chors_ErrorCodes::VIEW_NOT_FOUND,
        Chors_Boot::$applicationDir . '/' . Chors_Boot::$viewDir . "/" . $viewname . ".phtml");
    }

    $layout = new Chors_Layout();
    $layout->setViewData($this->view);
    $layout->start(Chors_Boot::$applicationDir . '/' . Chors_Boot::$viewDir . "/" . $viewname . ".phtml");
  }

  protected function view()
  {
    if (!Chors_File::exists(Chors_Boot::$applicationDir . '/' . Chors_Boot::$viewDir . "/" . $this->name . "/" . $this->currentAction . ".phtml"))
    {
      throw new Chors_Exception("View not found",
        Chors_ErrorCodes::VIEW_NOT_FOUND,
        Chors_Boot::$applicationDir . '/' . Chors_Boot::$viewDir . "/" . $this->name . "/" . $this->currentAction . ".phtml");
    }

    $layout = new Chors_Layout();
    $layout->setViewData($this->view);
    $layout->start(Chors_Boot::$applicationDir . '/' . Chors_Boot::$viewDir . "/" . $this->name . "/" . $this->currentAction . ".phtml");
  }

  /**
   * Execute specified controller/action and then halts current
   * function execution.
   * @param string $controllerName controller to be used
   * @param string $actionName action to be executed
   * @todo update after queue adding - applies to all functions concerning others control/action execution
   */
  protected function forward($controllerName = null, $actionName = null)
  {
    self::checkControllerExistence($controllerName, true);
    Chors_Boot::getInstance()->controllerRun($controllerName, $actionName);
    exit;
  }

  /**
   * Execute specified Namespace/Controller/Action/Params url string and then
   * halts current function execution.
   * @param string $string url which will be send to parser and accordingly executed
   */
  protected function forwardURL($string)
  {
    Chors_Boot::getInstance()->queueURL($string);
    exit;
  }

  public function getFView()
  {
    return $this->fview;
  }

  /**
   * Perform all necessary actions regarding controllers forwarding
   * @param Chors_Controller $controller Controller instance which initated 'forward' procedure
   */
  public function inheritController($controller)
  {
    if ($controller !== null)
    {
      $this->view = $controller->getFView();
      $this->previous = $controller->getCurrentSettings();
    }
  }

  public function getCurrentSettings()
  {
    return array('controller' => $this->name, 'action' => $this->currentAction);
  }

  /**
   * Execute specified controller/action and then return to main code
   * @param string $controllerName
   * @param string $methodName
   */
  protected function execute($controllerName = null, $methodName = null)
  {
    if(Chors_String::equals($controllerName, $this->name, TRUE) === FALSE)
      self::checkControllerExistence($controllerName, false);
    $control = $controllerName . "Controller";

    try
    {
      $reflection = new ReflectionMethod($controllerName . "Controller",
              $methodName);
      if ($reflection->isStatic())
      {
        Chors_Logger::internalTrace("Controller execute: " . $controllerName . "::" . $methodName . "()");
        
        // Solve issue with <i>T_PAAMAYIM_NEKUDOTAYIM</i> error
        if (version_compare(PHP_VERSION, '5.3.0') >= 0)
          $control::$methodName();
        else
          call_user_func($control . "::" . $methodName);
        return;
      }
    }
    catch (Exception $e)
    {
      
    }

    Chors_Logger::internalTrace("Controller execute: " . $controllerName . "->" . $methodName . "()");
    $control = new $control();
    $control->inheritController($this);
    $control->setCurrentAction($methodName);
    $control->{$methodName . "Action"}();
  }

  public static function checkControllerExistence(&$controllerName = null, $useDefault = true)
  {
    if ($controllerName === null)
    {
      if (Chors_Boot::$defController === null || !$useDefault)
        throw new Chors_Exception("Controller not defined", Chors_ErrorCodes::CONTROLLER_NOT_DEFINED);
      else
        $controllerName = Chors_Boot::$defController;
    }

    if (!Chors_File::exists(Chors_Boot::$applicationDir . '/' . Chors_Boot::$controlDir . "/" . $controllerName . "Controller.php"))
    {
      if ($useDefault)
      {
        if (!Chors_File::exists(Chors_Boot::$applicationDir . '/' . Chors_Boot::$controlDir . "/" . Chors_Boot::$defController . "Controller.php"))
        {
          throw new Chors_Exception(
            "Controller not found",
            Chors_ErrorCodes::CONTROLLER_NOT_FOUND,
            Chors_Boot::$applicationDir . '/' . Chors_Boot::$controlDir . "/" . Chors_Boot::$defController . "Controller.php");
        }
        else
          $controllerName = Chors_Boot::$defController;
      }
      else
      {
        throw new Chors_Exception(
          "Controller not found",
          Chors_ErrorCodes::CONTROLLER_NOT_FOUND,
          Chors_Boot::$applicationDir . '/' . Chors_Boot::$controlDir . "/" . $controllerName . "Controller.php");
      }
    }
    require_once Chors_Boot::$applicationDir . '/' . Chors_Boot::$controlDir . "/" . $controllerName . "Controller.php";
  }

  public function __call($name, $arguments)
  {
    if ($name === Chors_Boot::$defAction . "Action")
    {
      throw new Chors_Exception(
        "Method not defined",
        Chors_ErrorCodes::CONTROLLER_METHOD_NOT_FOUND,
        Chors_Boot::$defAction . "Action at " . __CLASS__);
    }
    $this->currentAction = Chors_Boot::$defAction;

    Chors_Logger::internalTrace("Controller redirect to: " . $this->name . "->" . $this->currentAction . "()");

    $this->{Chors_Boot::$defAction . "Action"}();
  }

  public static function __callStatic($name, $arguments)
  {
    throw new Chors_Exception(
      "Static method not defined",
      Chors_ErrorCodes::CONTROLLER_STATIC_METHOD_NOT_FOUND,
      $name . " at " . __CLASS__);
  }

}
