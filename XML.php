<?php
/**
 * Description of Chors_XML
 *
 * @author Runaurufu
 */
class Chors_XML
{
  /**
   * Convert passed mixed content into simple Xml instance.
   * @param mixed $object
   * @return \SimpleXMLElement
   */
  public static function convertToSimpleXml($data)
  {
    $xmlstr="<?xml version='1.0' encoding='UTF-8'?><root></root>";
 
    $xml = new SimpleXMLElement($xmlstr);
    
    self::objToXml($xml, $data);
    
    return $xml;
  }
  
  /**
   * 
   * @param \SimpleXMLElement $parentXml
   * @param mixed $obj
   */
  private static function objToXml($parentXml, $obj)
  {
    if(is_array($obj))
    {
      foreach ($obj as $key => $value)
      {
        $child = $parentXml->addChild("item");
        if($value !== NULL)
        {
          self::objToXml($child, $value);
        }
      }
    }
    elseif($obj instanceof Chors_Enum)
    {
      $parentXml[0] = (string)$obj;
    }
    elseif($obj instanceof Chors_Blob)
    {
      $parentXml[0] = $obj->getBase64Value();
    }
    elseif($obj instanceof Chors_DateTime)
    {
      $parentXml[0] = $obj->toSerializableString();
    }
    elseif(is_object($obj))
    {
      $reflection = new ReflectionClass($obj);
      $properites = $reflection->getProperties(ReflectionProperty::IS_PUBLIC);
      
      foreach ($properites as $value) 
      {
        $child = $parentXml->addChild($value->getName());
        if($obj !== NULL)
        {
          self::objToXml($child, $value->getValue($obj));
        }
      }
    }
    elseif(is_bool($obj))
    {
      $parentXml[0] = $obj ? "true" : "false";
    }
    else
    {
      if($obj !== NULL)
      {
        $parentXml[0] = $obj;
      }
    }
  }
}
