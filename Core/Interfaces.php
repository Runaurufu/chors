<?php

/**
 *
 * @author Runaurufu
 */
interface IComparable
{
  /**
   * 
   * @param IComparable $other
   * @return int &lt;0 if <i>$this</i> is less than <i>$other</i>;
   * &gt;0 if <i>$this</i> is greater than <i>$other</i>,
   * and 0 if they are equal.
   */
  function compareTo($other);
}

/**
 *
 * @author Runaurufu
 */
interface IEquatable
{
  /**
   * @param IEquatable $other
   * @return true if both are equal or false otherwise.
   */
  function equals($other);
}
