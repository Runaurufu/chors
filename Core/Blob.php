<?php

/**
 * Blob class for easy handling binary data.
 */
class Chors_Blob implements JsonSerializable, IEquatable
{
  /**
   * Binary representation of data.
   * @var string
   */
  private $value = null;
  
  /**
   * @param string $value binary data
   */
  public function __construct($value = null)
  {
    $this->value = $value;
  }
  
  /**
   * 
   * @param string $base64string base64 representation of data
   * @return Chors_Blob
   */
  public static function createFromBase64String($base64string)
  {
    return new Chors_Blob(base64_decode($base64string));
  }

  /**
   * 
   * @return string gets data in true binary form
   */
  public function getBinaryValue()
  {
    return $this->value;
  }
  
  /**
   * 
   * @return string gets data in base64 encoded form.
   */
  public function getBase64Value()
  {
    return base64_encode($this->value);
  }
  
  public function __toString()
  {
    return $this->getBinaryValue();
  }
  
  public function jsonSerialize()
  {
    return $this->getBase64Value();
  }
  
  public function equals($other)
  {
    $class = get_called_class();
    if($other instanceof $class)
    {
      return $this->getBinaryValue() == $other->getBinaryValue();
    }
    else
    {
      return FALSE;
    }
  }
}
