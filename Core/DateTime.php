<?php

/**
 * DateTime wrapper for holding datetime values.
 */
class Chors_DateTime implements IComparable
{
  protected $timeStamp = 0;
  
  public function setTimeStamp($value)
  {
    $this->timeStamp = $value;
  }
  
  public function getTimeStamp()
  {
    return $this->timeStamp;
  }
  
  public function __construct($timeStamp)
  {
    $this->timeStamp = $timeStamp;
  }
  
  /**
   * Attempts to parse input into actuall timestamp.
   * Numeric values are considered to be timestamp.
   * Dates without time zone are considered UTC dates.
   * @param mixed $mixed
   */
  public static function create($mixed)
  {
    if(is_int($mixed))
    {
      return self::createFromTimeStamp($mixed);
    }
    else if(is_numeric($mixed))
    {
      return self::createFromTimeStamp(intval($mixed));
    }
    else if(preg_match('/[0-9._-]/', $mixed) === TRUE)
    {
      return self::createFromUtcString($mixed);
    }
    else
    {
      return new Chors_DateTime(Chors_Time::stringToTimestamp($mixed));
    }
  }
  
  public static function createFromTimeStamp($timeStamp)
  {
    $ts = new Chors_DateTime($timeStamp);
    return $ts;
  }
  
  public static function now()
  {
    $ts = new Chors_DateTime(Chors_Time::nowTimestamp());
    return $ts;
  }
  
  public static function createFromUtcString($timeUtcString)
  {
    $ts = new Chors_DateTime(Chors_Time::stringToTimestamp($timeUtcString));
    return $ts;
  }
  
  public static function createFromLocalTimeString($timeUtcString)
  {
    $ts = new Chors_DateTime(Chors_Time::localStringToTimestamp($timeUtcString));
    return $ts;
  }
  
  public static function createFromServerTimeString($timeUtcString)
  {
    $ts = new Chors_DateTime(Chors_Time::serverStringToTimestamp($timeUtcString));
    return $ts;
  }
  
  public function __toString()
  {
    return $this->toUtcDateTimeString();
  }
  
  public function toLocalDateString()
  {
    return Chors_Time::timestampToLocalString($this->timeStamp, "Y-m-d");
  }
  
  public function toLocalDateTimeString()
  {
    return Chors_Time::timestampToLocalString($this->timeStamp, "Y-m-d H:i:s");
  }
  
  public function toServerDateString()
  {
    return Chors_Time::timestampToServerString($this->timeStamp, "Y-m-d");
  }
  
  public function toServerDateTimeString()
  {
    return Chors_Time::timestampToServerString($this->timeStamp, "Y-m-d H:i:s");
  }
  
  public function toUtcDateString()
  {
    return Chors_Time::timestampToString($this->timeStamp, "Y-m-d");
  }
  
  public function toUtcDateTimeString()
  {
    return Chors_Time::timestampToString($this->timeStamp, "Y-m-d H:i:s");
  }
  
  public function toSerializableString()
  {
    return Chors_Time::timestampToString($this->timeStamp, "Y-m-d\TH:i:s\Z");
  }

  /**
   * 
   * @param Chors_DateTime $a
   * @param Chors_DateTime $b
   * @return int &lt;0 if <i>$a</i> is less than <i>$b</i>;
   * &gt;0 if <i>$a</i> is greater than <i>$b</i>,
   * and 0 if they are equal.
   */
  public static function compare($a, $b)
  {
    if(($a === null || $a instanceof Chors_DateTime === false) && $b === null || $b instanceof Chors_DateTime === false)
      return 0;
    if($a === null || $a instanceof Chors_DateTime === false)
      return -1;
    if($b === null || $b instanceof Chors_DateTime === false)
      return 1;
    return $a->timeStamp - $b->timeStamp;
  }

  /**
   * 
   * @param Chors_DateTime $other
   * @return int &lt;0 if <i>$this</i> is less than <i>$other</i>;
   * &gt;0 if <i>$this</i> is greater than <i>$other</i>,
   * and 0 if they are equal.
   */
  public function compareTo($other)
  {
    self::compare($this, $other);
  }
}
