<?php

/**
 * Enum class for easy handling enum types.
 * All constant fields are treated as enum values.
 */
abstract class Chors_Enum implements JsonSerializable, IEquatable
{
  protected static $array = array();

  public static function getAllValues()
  {
    return array_values(self::getArray());
  }
  
  public static function getAllNames()
  {
    return array_keys(self::getArray());
  }
  
  public static function getArray()
  {
    $class = get_called_class();

    if(isset(self::$array[$class]) === FALSE)
    {
      self::$array[$class] = Chors_ClassInfo::getAllConstants($class);
    }
    return self::$array[$class];
  }
  
  /**
   * Checks if ENUM contains member with specified value.
   * @param int $value
   * @return boolean
   */
  public static function containsValue($value)
  {
    $array = self::getArray();    
    foreach ($array as $key => $val)
    {
      if($value == $val)
        return TRUE;
    }
    return FALSE;
  }
  
  /**
   * Checks if ENUM contains member with specified name.
   * @param string $name
   * @return boolean
   */
  public static function containsName($name)
  {
    $array = self::getArray();
    foreach ($array as $key => $val)
    {
      if($key == $name)
        return TRUE;
    }
    return FALSE;
  }

  public static function convertToValue($name)
  {
    $array = self::getArray();
    foreach ($array as $key => $val)
    {
      if($name == $key)
        return $val;
    }
    return null;
  }
  
  public static function convertToName($value)
  {
    $array = self::getArray();
    foreach ($array as $key => $val)
    {
      if($value == $val)
        return $key;
    }
    return null;
  }
  
  public static function convertToObject($name)
  {
    $value = self::convertToValue($name);
    if($value !== NULL)
    {
      $class = get_called_class();
      return new $class($value);
    }
    else
      return NULL;
  }
  
  /**
   * @var int
   */
  private $value = 0;
  
  /**
   * @param int $value
   */
  public function __construct($value = 0)
  {
    $this->value = $value;
  }
  
  /**
   * @return int
   */
  public function getValue()
  {
    return $this->value;
  }

  public function __toString()
  {
    $ret = self::convertToName($this->value);
    if($ret === NULL)
      return $this->value;
    else
      return $ret;
  }
  
  public function jsonSerialize()
  {
    return $this->__toString();
  }
  
  public function equals($other)
  {
    $class = get_called_class();
    if($other instanceof $class)
    {
      return $this->getValue() == $other->getValue();
    }
    elseif(is_integer($other))
    {
      return $this->getValue() == $other;
    }
    elseif(is_string($other))
    {
      return $this->__toString() == $other;
    }
    else
    {
      return FALSE;
    }
  }
}
