<?php

/**
 * Description of Chors_Path
 * @package Chors
 * @version 1.0
 * @since 1.0
 * @author Runaurufu
 */
abstract class Chors_Path
{

  /**
   * Returns invalid file names characters.
   * @return string[] 
   */
  public static function getInvalidFileNameChars()
  {
    return array('\\', '/', ':', '"', '*', '?', '<', '>', '|');
  }

  /**
   * Removes invalid characters from filename
   * @param string $fileName
   * @return string 
   */
  public static function removeInvalidFileNameChars($fileName)
  {
    return str_replace(self::getInvalidFileNameChars(), "", $fileName);
  }

  /**
   * Checks if specified path is any protocol path (starts with protocol://).
   * @param string $path
   * @return bool
   */
  public static function isProtocolPath($path)
  {
    return preg_match("/^\w+:\/\//", $path) === 1;
  }
}
