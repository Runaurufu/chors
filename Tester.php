<?php

/**
 * For all your testing needs
 *
 * @author Runaurufu
 */
class Chors_Tester
{
  private $tests;
  
  public function __construct()
  {
    $this->tests = array();
  }
  
  /**
   * 
   * @param callable $callable Define test function which will be called. It must take Chors_Test as param.
   * <code>
   * function($test)
   * {
   * }
   * </code>
   * @param string $testName
   * @param float $maxExecutionTimeAllowed
   */
  public function registerTest($callable, $testName, $maxExecutionTimeAllowed = NULL)
  {
    $this->tests[] = array('callable' => $callable, 'name' => $testName, `maxExecutionTimeAllowed` => $maxExecutionTimeAllowed);
  }
  
  /**
   * Register test methods belonging to specified class.
   * @param string $className Class which test methods should be registered.
   * If NULL supplied then Tester class methods will be registered.
   */
  public function registerClassTestMethods($className = null)
  {
    if(null)
      $className = get_class();

    $reflectionClass = new ReflectionClass($className);
    
    /* @var $reflections ReflectionMethod[] */
    $reflections = $reflectionClass->getMethods(ReflectionMethod::IS_STATIC);
    foreach ($reflections as $reflectionMethod)
    {
      if(Chors_String::startsWith($reflectionMethod->name, "test", false) || Chors_String::endsWith($reflectionMethod->name, "test", false))
      {
        $val = $reflectionMethod->class."::".$reflectionMethod->name;
        $this->registerTest($val, $val, NULL);
      }
    }
  }
  
  /**
   * Remove tests from tester.
   */
  public function removeAllTests()
  {
    $this->tests = array();
  }
  
  /**
   * 
   * @return array indexed array containing value-arrays with keys:
   * name - test case name
   * executionTime - execution time for test case
   * result - test case result (if === PASSED then it passed without issues).
   * Be aware that result values are considered flags so they may return values like:
   * RESULT_EXCEPTION|RESULT_TIMEDOUT - test failed due exception and still it took it more time than expected.
   */
  public function runTests()
  {
    $testResults = array();
    for ($index = 0; $index < count($this->tests); $index++)
    {
      $testResult = array();
      $test = new Chors_Test($this->tests[$index]['callable'], $this->tests[$index]['name']);

      $test->runTest();

      $result = $test->result;
      $elapsed = $test->elapsedTime;
      
      if($this->tests[$index]['maxExecutionTimeAllowed'] !== NULL
        && $elapsed > $this->tests[$index]['maxExecutionTimeAllowed'])
      {
        $result |= Chors_Test::RESULT_TIMEDOUT;
      }
      
      $testResult['name'] = $this->tests[$index]['name'];
      $testResult['executionTime'] = $elapsed;
      $testResult['result'] = $result;
      $testResults[] = $testResult;
    }
    
    return $testResults;
  }
  
  /**
   * This method will repeat each test case multiple times in random order and will provide with total time elapsed for each test case.
   * Do not use for test-passing checks as it takes time!
   * At least single execution of each test is guaranteed!
   * @param int $maxTime max execution time (in seconds) allowed for testing.
   * @param int $extendIniTime if true then "max_execution_time" in ini will be extended by $maxTime.
   * @return array indexed array containing value-arrays with keys:
   * name
   * executionTotalTime
   * executionCount
   * executionMeanTime
   */
  public function checkTestsPerformance($maxTime, $extendIniTime = TRUE)
  {
    if($extendIniTime)
    {
      $executeTime = (int)ini_get('max_execution_time');
      if($executeTime !== 0) // 0 === infinite
      {
        ini_set('max_execution_time', $executeTime + (int)$maxTime);
      }
    }
        
    $testResults = array();
    
    $totalTime = 0;
    $count = count($this->tests);
    for ($index = 0; $index < $count; $index++)
    {
      /** @var Chors_Test */
      $test = new Chors_Test($this->tests[$index]['callable'], $this->tests[$index]['name']);

      $test->runTest();

      $elapsed = $test->elapsedTime;
      $totalTime += $elapsed;
      
      $testResult['name'] = $this->tests[$index]['name'];
      $testResult['executionTotalTime'] = $elapsed;
      $testResult['executionCount'] = 1;
      $testResults[$index] = $testResult;
    }
    
    $count -= 1;
    while($totalTime < $maxTime)
    {
      $testIndex = mt_rand(0, $count);

      $test = new Chors_Test($this->tests[$testIndex]['callable'], $this->tests[$testIndex]['name']);

      $test->runTest();
      
      $elapsed = $test->elapsedTime;
      $totalTime += $elapsed;
      
      $testResults[$testIndex]['executionTotalTime'] += $elapsed;
      $testResults[$testIndex]['executionCount'] += 1;
    }
    
    $count += 1;
    
    for ($index = 0; $index < $count; $index++)
    {
      $testResults[$index]['executionMeanTime'] = $testResults[$index]['executionTotalTime'] / $testResults[$index]['executionCount'];
    }
    
    return $testResults;
  }
}

final class Chors_Test 
{
  const RESULT_NONE = 0;
  const RESULT_EXECUTED = 1;
  const RESULT_PASSED = 2;
  const RESULT_FAILED = 4;
  const RESULT_EXCEPTION = 8;
  const RESULT_TIMEDOUT = 16;
  
  public $name;
  private $callable;
  public $result = self::RESULT_NONE;
  public $elapsedTime = 0;
  public $exception = null;
  public $failureInfo = null;
  
  public function __construct($callable, $name = null)
  {
    $this->callable = $callable;
    $this->name = $name === null ? "" : $name;
  }
  
  public function runTest()
  {
    $startTime = microtime(true);
    try
    {
      call_user_func($this->callable, $this);
      $this->elapsedTime = microtime(true) - $startTime;
      
      $this->result |= Chors_Test::RESULT_EXECUTED;
      
      if($this->result & Chors_Test::RESULT_FAILED == 0)
        $this->result |= Chors_Test::RESULT_PASSED;
    }
    catch (Exception $ex)
    {
      $this->elapsedTime = microtime(true) - $startTime;
      
      $this->exception = $ex;
      $this->result |= Chors_Test::RESULT_EXCEPTION;
    }
  }
  
  public function assertTrue($var)
  {
    if($var !== true)
    {
      $this->setFailureInfo();
    }
  }
  
  public function assertFalse($var)
  {
    if($var !== false)
    {
      $this->setFailureInfo();
    }
  }
  
  public function assert($var, $value)
  {
    if($var !== $value)
    {
      $this->setFailureInfo();
    }
  }
  
  private function setFailureInfo()
  {
    $this->result = self::RESULT_FAILED;
    
    $caller = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2)[2];
    
    $name = trim($caller["class"].$caller["type"].$caller["function"]);
    
    $this->failureInfo = array("name" => $name, "file" => $caller["file"], "line" => $caller["line"]);
  }
}