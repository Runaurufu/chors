<?php
require_once 'String.php';
require_once 'Logger.php';
require_once 'Boot.php';

/**
 * Description of AutoLoader
 * @package Chors
 * @author Runaurufu
 */

class Chors_Autoloader
{
  public function __construct()
  {
    spl_autoload_register(array($this, 'loader'));
  }
  
  private function loader($class)
  {
    if(Chors_String::startsWith($class, "Chors"))
    {       
      $array = explode("_", $class);
      $path = dirname(__FILE__).DIRECTORY_SEPARATOR;

      $len = count($array) - 1;

      for ($index = 1; $index < $len; $index++)
      {
        $path .= $array[$index].DIRECTORY_SEPARATOR;
      }

      $path.= $array[$len].".php";

      if(file_exists($path))
      {
        // due Chors_ classes should not really be handled via autoloader as they should be loaded by now...
        Chors_Boot::logMessage(Chors_Logger::LOG_LEVEL_WARNING, "AUTOLOADER::Called for class: ".$class);
        require_once $path;
      }
      else
      {
        Chors_Boot::logMessage(Chors_Logger::LOG_LEVEL_WARNING, "AUTOLOADER::Could not find file {$path} for class {$class}");
      }
    }
    else
    {
      $path = null;
      if(Chors_String::endsWith($class, "controller", false))
      {
        $path = Chors_Boot::$applicationDir.DIRECTORY_SEPARATOR.Chors_Boot::$controlDir.DIRECTORY_SEPARATOR.$class.".php";
      }
      else if(Chors_String::endsWith($class, "ent", false))
      {
        $path = Chors_Boot::$applicationDir.DIRECTORY_SEPARATOR.Chors_Boot::$entityDir.DIRECTORY_SEPARATOR.$class.".php";
      }
      else if(Chors_String::endsWith($class, "ext", false))
      {
        $path = Chors_Boot::$applicationDir.DIRECTORY_SEPARATOR."extension".DIRECTORY_SEPARATOR.$class.".php";
      }
      else //if(Chors_String::endsWith($class, "mod", false)) // and we assume rest files are models...
      {
        if(Chors_String::endsWith($class, "ModData", false)) // special case handling for ModData classes
        {
          $fileName = substr($class, 0, strlen($class) - 7);
          
          // As first step try search for obtained name
          $path = Chors_Boot::$applicationDir.DIRECTORY_SEPARATOR.Chors_Boot::$modelDir.DIRECTORY_SEPARATOR.$fileName.".php";
          
          // If this fails try with (NAME)Mod as it may be contained within Model file
          if(file_exists($path) === false)
          {
            $path = Chors_Boot::$applicationDir.DIRECTORY_SEPARATOR.Chors_Boot::$modelDir.DIRECTORY_SEPARATOR.$fileName."Mod.php";
          }
        }
        
        if($path === null || file_exists($path) === false)
        {
          $path = Chors_Boot::$applicationDir.DIRECTORY_SEPARATOR.Chors_Boot::$modelDir.DIRECTORY_SEPARATOR.$class.".php";
        }
      }
      
      if(file_exists($path))
      {
        Chors_Boot::logMessage(Chors_Logger::LOG_LEVEL_INFO, "AUTOLOADER::Loading {$path} for {$class}");
        require_once $path;
      }
      else
      {
        Chors_Boot::logMessage(Chors_Logger::LOG_LEVEL_WARNING, "AUTOLOADER::Could not find file for class: ".$class);
      }
    }
  }
}

$autoloader = new Chors_Autoloader();
