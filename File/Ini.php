<?php

/**
 * Description of ChorsFileIni
 * @package Chors
 * @version 1.0
 * @since 1.0
 * @author Runaurufu
 */
class Chors_File_Ini
{

  /**
   * Saves data from array into specified file in "INI" format.
   * @param string $filename
   * @param array $array
   * @throws ChorsException if file could not be locked
   */
  static function save($filename, $array)
  {
    $handle = fopen($filename, 'wb');

    if (flock($handle, LOCK_EX | LOCK_NB))
    {
      foreach ($array as $key => $value)
      {
        if (is_array($value))
        {
          fwrite($handle, "[" . $key . "]" . PHP_EOL);
          foreach ($value as $k => $v)
          {
            fwrite($handle, $k . " = " . $v . PHP_EOL);
          }
        }
        else
        {
          fwrite($handle, $key . " = " . $value . PHP_EOL);
        }
      }
      flock($handle, LOCK_UN);
      fclose($handle);
    }
    else
    {
      fclose($handle);
      throw new Chors_Exception("File lock failed", Chors_ErrorCodes::FILE_LOCK_FAILED);
    }
  }

  /**
   * Saves data from array into specified file in "INI" format.
   * If file exists than it attempt to append data to it's content
   * @param string $filename
   * @param array $array
   * @throws ChorsException if file could not be locked
   */
  static function append($filename, $array)
  {
    if (Chors_File::exists($filename))
    {
      self::load($filename, $array);
    }

    self::save($filename, $array);
  }

  /**
   * Acquire configuration settings from specified file. File should be in format:
   * [section]<br/>
   * <b>key=value</b><br/>
   * no escape characters are provided so text will be fetched "as is"
   * @param string $filename file which should parsed
   * @param array $array already existing config array, to which data would be added
   * @return array returns created (or modified) array
   * @throws ChorsException if file does not exists
   */
  static function load($filename, &$array = NULL)
  {
    if ($array === NULL)
      $array = array();
    if (Chors_File::exists($filename))
    {
      $buffer = Chors_File::readAsArray($filename, null, true);

      $tag = '';
      $tag_is_set = false;
      foreach ($buffer as $value)
      {
        if (($pos = strpos($value, '[')) === 0 && ($pose = strpos($value, ']')) > 1)
        { // if tag exists
          $tag = trim(substr($value, 1, $pose - 1));
          $tag_is_set = true;
        }
        elseif ($pos === 0 && $pose === 1)
        {
          $tag = '';
          $tag_is_set = false;
        }
        elseif (($pos = strpos($value, '=')) > 0)
        {
          if ($tag_is_set)
            $array[$tag][trim(substr($value, 0, $pos))] = trim(substr($value, $pos + 1));
          else
            $array[trim(substr($value, 0, $pos))] = trim(substr($value, $pos + 1));
        }
      }
    }
    else
    {
      throw new Chors_Exception("File not found", Chors_ErrorCodes::FILE_NOT_FOUND, $filename);
    }
    return $array;
  }

}
