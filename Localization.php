<?php

/**
 * Localization class for Chors.
 * @author Runaurufu
 */
class Chors_Localization
{
  private static $localizationArray = array();
  private static $localizationDirectory = "application/localization";
  private static $unlocalizatedFilepath = "unlocalizated.ini";
  private static $currentLanguageName = "";
  private static $currentLanguageId = 0;
  public static $notTranslatedPlaceholder = "[Not Translated!] %s";

  /**
   * Private constructor to prevent using instances of this class
   */
  private function __construct()
  {
    ;
  }

  public static function setLanguage($language, $id)
  {
    if (is_dir(self::$localizationDirectory . "/" . $language))
    {
      if (self::$currentLanguageName != $language)
        self::$localizationArray = array();

      self::$currentLanguageName = $language;
      self::$currentLanguageId = $id;

      Chors_Auth::changeCurrentUserLocalization(self::$currentLanguageId);
    }
    else
    {
      if (mkdir(self::$localizationDirectory . "/" . $language, 0755))
        self::setLanguage($language, $id);
      else
        throw new Chors_Exception("Localization directory not found",
        Chors_Exception::ERR_GENERIC_DIRNTFND,
        self::$localizationDirectory . "/" . $language);
    }
  }

  public static function setLanguageFromUserPreferences($defaultLanguageId, $defaultLanguageName)
  {
    if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) == false)
    {
      Chors_Localization::setLanguage($defaultLanguageId, $defaultLanguageName);
      return;
    }

    $langs = array();
    foreach (explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']) as $k => $pref)
    {
      // split $pref again by ';q='
      // and decorate the language entries by inverted position
      if (false !== ($i = strpos($pref, ';q=')))
        $langs[substr($pref, 0, $i)] = array((float) substr($pref, $i + 3), -$k);
      else
        $langs[$pref] = array(1, -$k);
    }
    arsort($langs);
    // no need to undecorate, because we're only interested in the keys
    $langs = array_keys($langs);

    foreach ($langs as $languageName)
    {
      if (is_dir(self::$localizationDirectory . "/" . $languageName))
      {
        $lanId = Chors_Auth::getLocalizationIdFromLocalizationName($languageName);
        if ($lanId == null)
          continue;
        self::setLanguage($languageName, $lanId);
        return;
      }
    }
    Chors_Localization::setLanguage($defaultLanguageId, $defaultLanguageName);
  }

  public static function setLocalizationDirectory($path)
  {
    self::$localizationDirectory = $path;
  }

  public static function getCurrentLanguageName()
  {
    return self::$currentLanguageName;
  }

  public static function getCurrentLanguageId()
  {
    return self::$currentLanguageId;
  }

  public static function addLocalization($language)
  {//TODO
    return $newId;
  }

  public static function editLocalization($id, $language)
  {//TODO
  }

  public static function removeLocalization($id)
  {//TODO
  }

  public static function get($path, $section, $key)
  {
    if (isset(self::$localizationArray[$path][$section][$key]))
    {
      return self::$localizationArray[$path][$section][$key];
    }
    else
    {
      if (Chors_File::exists(self::$localizationDirectory . "/" . self::$currentLanguageName . "/" . $path . ".ini"))
      {
        self::$localizationArray[$path] = Chors_File_Ini::load(self::$localizationDirectory . "/" . self::$currentLanguageName . "/" . $path . ".ini");
        if (isset(self::$localizationArray[$path][$section][$key]))
          return self::$localizationArray[$path][$section][$key];
      }

      self::$localizationArray[$path][$section][$key] = sprintf(self::$notTranslatedPlaceholder, $key);
      Chors_File_Ini::append(self::$localizationDirectory . "/" . self::$currentLanguageName . "/" . $path . ".ini",
              self::$localizationArray[$path]);

      // @todo add also to file with untranslated files!

      return self::$localizationArray[$path][$section][$key];
    }
  }
}

function C_($path, $section, $key)
{
  return Chors_Localization::get($path, $section, $key);
}
