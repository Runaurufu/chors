<?php

/**
 * Description of Time
 * Quick explanation for terms used:
 * local time - time with time zone used by CurrentTimeZone parameter (user time)
 * server time - time with time zone used by server (date('Z'))
 * utc time - raw timeStamp time in time zone = 0
 * In most cases time should be taken using UTC time and displayed using local time.
 * @package Chors
 * @version 1.0
 * @since 1.0
 * @author Runaurufu
 */
abstract class Chors_Time
{
  /** 60 seconds */
  const MINUTE = 60;

  /** 60 minutes */
  const HOUR = 3600;

  /** 24 hours */
  const DAY = 86400;

  /** 7 days */
  const WEEK = 604800;

  /** 1/12 of year ≈ 30,4375 days */
  const MONTH = 2629800;

  /** 365,25 days */
  const YEAR = 31557600;
  
  private static $serverTimeZone = null;
  private static $currentTimeZone = null;
  
  /**
   * Private constructor to prevent using instances of this class
   */
  private function __construct()
  {
    ;
  }
  
  public static function getServerTimeZone()
  {
    if(self::$serverTimeZone === NULL)
    {
      self::$serverTimeZone = date('Z');
    }
    return self::$serverTimeZone;
  }
  
  /**
   * Lets define current time zone used by "server" functions
   * @param int $time number of seconds ahead of UTC
   */
  public static function setServerTimeZone($time)
  {
    self::$serverTimeZone = $time;
  }
  
  public static function getCurrentTimeZone()
  {
    if(self::$currentTimeZone === NULL)
    {
      self::$currentTimeZone = self::getServerTimeZone();
    }
    return self::$currentTimeZone;
  }
  
  /**
   * Lets define current time zone used by "local" functions
   * @param int $time number of seconds ahead of UTC
   */
  public static function setCurrentTimeZone($time)
  {
    self::$currentTimeZone = $time;
  }

    /**
   * Gets a timestamp that is set to the current date and time on this computer, expressed as the Coordinated Universal Time (UTC).
   * @return int
   */
  public static function nowTimestamp()
  {
    return time();
  }

  /**
   * Gets the current date (today date and time equal to 00:00:00).
   * @return int
   */
  public static function todayTimestamp()
  {
    return self::dateTimestamp(self::nowTimestamp());
  }
  
  /**
   * Gets the current date (today date and time equal to 00:00:00 in current time zone).
   * @return int
   */
  public static function localTodayTimestamp()
  {
    return self::localDateTimestamp(self::nowTimestamp());
  }
  
  /**
   * Gets the current date (today date and time equal to 00:00:00 in server time zone).
   * @return int
   */
  public static function serverTodayTimestamp()
  {
    return self::serverDateTimestamp(self::nowTimestamp());
  }

  /**
   * Get timestamp of the same day of hour 00:00:00 utc
   * @param int $timestamp
   * @return int
   */
  public static function dateTimestamp($timestamp)
  {
    // Timestamp starts at 00:00:00 UTC on January 1, 1970
    return $timestamp - $timestamp % self::DAY;
  }
  
  /**
   * Get timestamp of the same day of hour 00:00:00 in local time zone
   * @param int $timestamp
   * @return int
   */
  public static function localDateTimestamp($timestamp)
  {
    // Timestamp starts at 00:00:00 UTC on January 1, 1970
    return ($timestamp - $timestamp % self::DAY) - self::getCurrentTimeZone();
  }
  
  /**
   * Get timestamp of the same day of hour 00:00:00 in server time zone
   * @param int $timestamp
   * @return int
   */
  public static function serverDateTimestamp($timestamp)
  {
    // Timestamp starts at 00:00:00 UTC on January 1, 1970
    return ($timestamp - $timestamp % self::DAY) - self::getServerTimeZone();
  }

  /**
   * Returns the number of days in the specified month and year.
   * @param int $year The year.
   * @param int $month The month (a number ranging from 1 to 12).
   * @return int The number of days in month for the specified year.
   */
  public static function daysInMonth($year, $month)
  {
    return gmdate("t", mktime(0, 0, 0, $month, 1, $year));
  }

  /**
   * Returns an indication whether the specified year is a leap year.
   * @param int $year A 4-digit year.
   * @return bool <b>true</b> if year is a leap year; otherwise, <b>false</b>.
   */
  public static function isLeapYear($year)
  {
    return gmdate("L", mktime(0, 0, 0, 1, 1, $year)) === 1;
  }

  /**
   * Returns formated time string for utc time
   * @param int $timestamp If not set then nowTimestamp is used.
   * @param string $format if not set then <i>'Y-m-d H:i:s'</i> is used.
   * @return string
   */
  static public function timestampToString($timestamp = null, $format = 'Y-m-d H:i:s')
  {
    if ($timestamp === null)
    {
      $timestamp = self::nowTimestamp();
    }
    return gmdate($format, (int) $timestamp);
  }
  
  /**
   * Returns formated time string for local time
   * @param int $timestamp If not set then nowTimestamp is used.
   * @param string $format if not set then <i>'Y-m-d H:i:s'</i> is used.
   * @return string
   */
  static public function timestampToLocalString($timestamp = null, $format = 'Y-m-d H:i:s')
  {
    if ($timestamp === null)
    {
      $timestamp = self::nowTimestamp();
    }
    $timestamp += self::getCurrentTimeZone();
    
    return gmdate($format, (int) $timestamp);
  }

  /**
   * Returns formated time string for server local time
   * @param int $timestamp If not set then nowTimestamp is used.
   * @param string $format if not set then <i>'Y-m-d H:i:s'</i> is used.
   * @return string
   */
  static public function timestampToServerString($timestamp = null, $format = 'Y-m-d H:i:s')
  {
    if ($timestamp === null)
    {
      $timestamp = self::nowTimestamp();
    }
    $timestamp += self::getServerTimeZone();
    
    return gmdate($format, (int) $timestamp);
  }
  
  /**
   * Generate time in timestamp format from string containing utc date
   * @param string $string timestring to be converted to timestamp
   * @return int formated to timestamp string
   */
  static public function stringToTimestamp($string)
  {
    return strtotime($string);
  }
  
  /**
   * Generate time in timestamp format from string containing local (current time zone) date
   * @param string $string timestring to be converted to timestamp
   * @return int timestamp
   */
  static public function localStringToTimestamp($string)
  {
    return self::stringToTimestamp($string) - self::getCurrentTimeZone();
  }
  
  /**
   * Generate time in timestamp format from string containing server local date
   * @param string $string timestring to be converted to timestamp
   * @return int timestamp
   */
  static public function serverStringToTimestamp($string)
  {
    return self::stringToTimestamp($string) - self::getServerTimeZone();
  }
}
