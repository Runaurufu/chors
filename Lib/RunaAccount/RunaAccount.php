<?php
/**
 * Copyright 2017 Runaurufu
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

class RunaAccount
{
  private $applicationKey = null;
  private $applicationSecret = null;
  
  private $cookieName = "runaauth";
  private $cookiePath = "/";
  private $cookieLifeTime = 31557600;
  
  private $sessionKeyName = "runaauth";
  
  private $useSecureUrl = false;

  /**
   *
   * @var RunaAccountIdentity 
   */
  private $currentIdentity = null;
  
  /**
   * 
   * @param Array $config Array supporting following keys:
   *  - applicationKey
   *  - applicationSecret
   *  - cookieName
   *  - sessionKeyName
   *  - useSecureUrl
   */
  public function RunaAccount($config)
  {
    if(isset($config['applicationKey']))
      $this->applicationKey = $config['applicationKey'];
    
    if(isset($config['applicationSecret']))
      $this->applicationSecret = $config['applicationSecret'];
    
    if(isset($config['cookieName']))
      $this->cookieName = $config['cookieName'];
    
    if(isset($config['sessionKeyName']))
      $this->sessionKeyName = $config['sessionKeyName'];
    
    if(isset($config['useSecureUrl']))
      $this->useSecureUrl = $config['useSecureUrl'];
  }

  private function getApiUrl()
  {
    $url = "";    
    if($this->useSecureUrl)
      $url.= "https";
    else
      $url.= "http";
    $url.= "://account.runaurufu.com/apiXml/";
    return $url;
  }
  
  /**
   * 
   * @return string
   */
  public function getApplicationKey()
  {
    return $this->applicationKey;
  }
  
  /**
   * 
   * @return string
   */
  public function getApplicationSecret()
  {
    return $this->applicationSecret;
  }
  
  /**
   * Returns currently hold auth token or NULL if it does not exist.
   * @return string
   */
  public function getCurrentAuthToken()
  {
    if (!isset($_COOKIE[$this->cookieName]))
      return NULL;
    
    return $_COOKIE[$this->cookieName];
  }
  
  /**
   * Set current token.
   */
  public function setCurrentAuthToken($token)
  {
    $expire = time() + $this->cookieLifeTime;
    
    setcookie($this->cookieName, $token, $expire, $this->cookiePath);
  }
  
  private function loadUrlToString($url)
  {
    if(function_exists("curl_init"))
    {
      $curl_handle = curl_init();
      curl_setopt($curl_handle, CURLOPT_URL, $url);
      curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 0);
      curl_setopt($curl_handle, CURLOPT_TIMEOUT, 0);
      curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, FALSE); 
      curl_setopt($curl_handle, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
      curl_setopt($curl_handle, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
      $response = curl_exec($curl_handle); 
      $curl_info = curl_getinfo($curl_handle);
      curl_close($curl_handle);

      if($curl_info !== FALSE && $curl_info["http_code"] === 200)
        return $response; 
    }
    
    $ctx = stream_context_create(array(
      'http' => array(
        'timeout' => 10,
        'header'=>'Connection: close'
        )
      )
    );

    $response = @file_get_contents($url, false, $ctx);

    if($response !== FALSE)
      return $response;
    
    return NULL;
  }
  
  /**
   * Clears local data.
   */
  public function clearLocalData()
  {
    if (isset($_COOKIE[$this->cookieName]))
      setcookie($this->cookieName, '', 1, $this->cookiePath);
    $this->currentIdentity = NULL;
  }
  
  /**
   * Clear remote data.
   */
  public function clearRemoteData()
  {
    $token = $this->getCurrentAuthToken();
    if($token === NULL)
      return;
    
    // INVALIDATE CURRENT TOKEN DATA
    $url = $this->getApiUrl()
          . "invalidateToken"
          . "?appKey=" . $this->applicationKey
          . "&appSecret=" . $this->applicationSecret
          . "&token=" . $token;
    $string = $this->loadUrlToString($url);
    if($string === NULL)
      return;
    
  }
  
  /**
   * Clear both remote and local data.
   */
  public function clearAllData()
  {
    $this->clearRemoteData();
    $this->clearLocalData();
  }
  
  /**
   * Returns current identity or NULL if current identity is not set.
   * @return RunaAccountIdentity
   */
  public function getCurrentIdentity()
  {
    if($this->currentIdentity === NULL)
    {    
      $token = $this->getCurrentAuthToken();
      if($token === NULL)
        return NULL;

      // RETRIEVE IDENTITY FROM ACCOUNT
      $url = $this->getApiUrl()
            . "getIdentityByToken"
            . "?appKey=" . $this->applicationKey
            . "&appSecret=" . $this->applicationSecret
            . "&token=" . $token;
      $string = $this->loadUrlToString($url);
      if($string === NULL)
        return NULL;

      $xml = simplexml_load_string($string);
      if($xml === FALSE)
        return NULL;

      $identity = new RunaAccountIdentity();
      // PARSE!
      $identity->GUID = (string)$xml->GUID;
      $identity->displayName = (string)$xml->DisplayName;

      $this->currentIdentity = $identity;
    }
    
    return $this->currentIdentity;
  }
} 

class RunaAccountIdentity
{
  /**
   * Identity GUID.
   * @var string 
   */
  public $GUID;
  /**
   * Identity display name.
   * @var string
   */
  public $displayName;
}

