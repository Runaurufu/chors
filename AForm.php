<?php

/**
 * @package Chors
 * @version 1.0
 * @since 1.0
 * @author Runaurufu
 */
abstract class Chors_AForm
{

  /**
   * Creates select box
   * @param array $keyArray Contains keys which should be inserted to select
   * @param string $keyValue Key in keyArray which represents option's value attribute
   * @param string $keyDescription Key in keyArray which represents option's description
   * @param array $optionsArray Select box attributes (passed directly)
   * @param mixed $keySelected Option which value is exact as this parameter will receive "SELECTED", if array than key=><u>values</u> represents values to be selected
   */
  public final static function createSelect($keyArray, $keyValue, $keyDescription, $optionsArray = null, $keySelected = null)
  {
    if (!is_array($keyArray) || $optionsArray !== null && !is_array($optionsArray))
    {
      throw new Chors_Exception('Both $keyArray and $optionsArray parameters should be arrays', Chors_Exception::ERR_GENERIC_WRDTTYPE);
    }

    if ($keySelected !== null)
    {
      if (!is_array($keySelected))
      {
        $aux = $keySelected;
        unset($keySelected);
        $keySelected[0] = $aux;
      }
    }

    echo "<select ";
    if($optionsArray !== NULL)
    {
      foreach ($optionsArray as $key => $value)
      {
        echo Chors_String::formSafe($key) . '="' . Chors_String::formSafe($value) . '" ';
      }
    }
    echo ">";

    foreach ($keyArray as $key => $opt)
    {
      if (is_array($opt))
      {
        if (!isset($opt[$keyValue]) || !isset($opt[$keyDescription]))
        {
          continue;
        }
        $value = $opt[$keyValue];
        echo "<option value='" . Chors_String::formSafe($value) . "' ";
        if ($keySelected !== null)
          if (in_array($value, $keySelected))
            echo "selected='selected'";
        echo ">" . $opt[$keyDescription] . "</option>";
      }
      else
      {
        if(isset($opt->$keyValue))
          $value = $opt->$keyValue;
        else
          $value = $opt->$keyValue();
        echo "<option value='" . Chors_String::formSafe($value) . "' ";
        if ($keySelected !== null)
          if (in_array($value, $keySelected))
            echo "selected='selected'";
        echo ">";
        if(isset($opt->$keyDescription))
          echo $opt->$keyDescription;
        else
          echo $opt->$keyDescription();
        
        echo "</option>";
      }
    }

    echo "</select>";
  }

}
