<?php

/**
 * Chors file class, use it for any write/read operations on files.
 * @package Chors
 * @since 1.0
 * @version 1.0
 * @author Runaurufu
 * @todo add non static methods representing these functions.
 */
class Chors_File
{
  /**
   * Retrieve extension of specified file or "" if extension not found.
   * @param string $filepath
   * @return string Extension (with .) of file.
   */
  public static function getExtension($filepath)
  {
    $index = strripos($filepath, ".");
    
    if($index <= 0)
      return "";
    
    return substr($filepath, $index);
  }
  /**
   * Check if file exist. Can be set to throw exception when file does not exist.
   * @param string $filepath file location which should be determined
   * @param bool $throwNotExist if true and file does not exist than method throw exception ERR_FILE_NTEXST
   * @return bool true - file exist, false - file do not exist
   * @throws Chors_Exception
   */
  public static function exists($filepath, $throwNotExist = false)
  {
    if (is_file($filepath))
    {
      return true;
    }
    else
    {
      if (file_exists($filepath))
      {
        return true;
      }
      elseif ($throwNotExist)
      {
        throw new Chors_Exception("File does not exist", Chors_ErrorCodes::FILE_NOT_FOUND);
      }
      else
      {
        return false;
      }
    }
  }

  /**
   * Returns size of specified file.
   * For formated size (KiB, MiB, TiB, etc...) use {@link Chors_File::sizeFormated()}
   * @param string $filepath file location which size should be determined
   * @return <Int> size of specified file in bytes
   */
  public static function size($filepath)
  {
    if (Chors_Path::isProtocolPath($filepath) === false && self::exists($filepath, true))
    {
      return filesize($filepath);
    }
    else
    {
      return null;
    }
  }

  /**
   * Returns formated size of specified file.
   * For pure byte size use {@link Chors_File::size()}
   * @param string $filepath file location which size should be determined
   * @param float $precision defines when method should move to next unit (KiB -> MiB)
   * @param int $scale defines number of digits stored to the right of decimal point
   */
  public static function sizeFormated($filepath, $precision = 0.5, $scale = 2)
  {
    $sizeFix = Array(0 => "B", 1 => "KiB", 2 => "MiB", 3 => "GiB", 4 => "TiB", 5 => "PiB", 6 => "EiB", 7 => "ZiB", 8 => "YiB");

    $orgSize = $prevSize = $size = self::size($filepath);
    if($size === NULL)
        return NULL;
    
    $step = 0;
    while ($size > $precision)
    {
      $prevSize = $size;
      $size /= 1024;
      $step++;
    }
    if ($size < $precision && $step > 0)
    {
      $size = $prevSize;
      $step--;
    }
    $size = round($size, $scale);

    if ($step < count($sizeFix))
      return $size.=$sizeFix[$step];
    else
      return $orgSize.=$sizeFix[0];
  }

  /**
   * Read data from file and returns them in String form.
   * @param string $filepath file's location from which data should be read.
   * @param int $timeout timeout set for reading specified file. It automatically add time to max_execution_time.
   * @return string contains file content in single String variable
   */
  public static function readAsString($filepath, $timeout = 0)
  {
    if (Chors_Path::isProtocolPath($filepath) || self::exists($filepath, true))
    {
      $ctx = stream_context_create(array(
        'http' => array(
          'timeout' => $timeout,
          'header'=>'Connection: close'
          )
        )
      );
      
      if($timeout > 0)
      {
        $executeTime = ini_get('max_execution_time');
        if($executeTime !== 0) // 0 === infinite
          ini_set('max_execution_time', (int)$executeTime + (int)$timeout);
      }
      $result = file_get_contents($filepath, false, $ctx);
      
      if($result === FALSE)
        return null;
      return $result;
    }
  }
  
  /**
   * Read data from file and returns them in Array form
   * @param string> $filename file location from which data should be read
   * @param mixed $flags flags used in data reading : FILE_IGNORE_NEW_LINES, FILE_SKIP_EMPTY_LINES
   * @param bool $forcefgets if forcefully use fgets function - by default it is used only for files larger than 1 MiB
   * @return array contains file content, each file's line in separate array's row
   */
  public static function readAsArray($filename, $flags = null, $forcefgets = false)
  {
    if (Chors_Path::isProtocolPath($filename) === FALSE && self::exists($filename, true) == false)
      return null;

    $size = self::size($filename);
    if ($size === NULL || $size > 1048576 || $forcefgets) // for greater than 1 MB files
    {
      $handle = fopen($filename, 'rb');
      $aux = sqrt($flags - FILE_SKIP_EMPTY_LINES);
      while (!feof($handle))
      {
        $buff = fgets($handle, 4096);

        if ($aux > 0 && (int) $aux == $aux && strlen($buff) == 0)
          continue;

        $file[] = $buff;
      }
      fclose($handle);
    }
    else
    {
      $file = file($filename, $flags);
    }

    $aux = sqrt($flags - FILE_IGNORE_NEW_LINES);
    if ($aux > 0 && (int) $aux == $aux)
    {
      foreach ($file as $key => $value)
      {
        $file[$key] = rtrim($value, "\r\n") . PHP_EOL;
      }
    }
    return $file;
  }

  /**
   * Writes data to specified file with 'wb' flags - truncate file or create new,
   * then perform binary safe write.
   * @param string $filename file location to which data should be save
   * @param string $content content to be save in file
   * @param bool $lock should method shall lock file for writing
   * @param bool $blockLock should lock be blocking or nonblocking (dont stops code execution)
   * @param bool $throwBlock if lock nonblocking and not lock and this is true than throw exception Chors_ErrorCodes::FILE_LOCK_FAILED
   * @return int number of bytes wrote to file
   */
  public static function writeAsString($filename, $content, $lock = false, $blockLock = true, $throwBlock = true)
  {
    $handle = fopen($filename, 'wb');
    if ($lock)
    {
      if ($blockLock)
      {
        $flags = LOCK_EX;
      }
      else
      {
        $flags = LOCK_EX | LOCK_NB;
      }

      if (flock($handle, $flags))
      {
        $bytes = fwrite($handle, $content);
        flock($handle, LOCK_UN);
        fclose($handle);
        if ($bytes === FALSE)
        {
          throw new Chors_Exception(
            "File write failed",
            Chors_ErrorCodes::FILE_WRITE_FAILED);
        }
      }
      elseif ($throwBlock)
      {
        fclose($handle);
        throw new Chors_Exception("File lock failed", Chors_ErrorCodes::FILE_LOCK_FAILED);
      }
    }
    else
    {
      $bytes = fwrite($handle, $content);
      fclose($handle);
      if ($bytes === FALSE)
      {
        throw new Chors_Exception(
          "File write failed",
          Chors_ErrorCodes::FILE_WRITE_FAILED);
      }
    }
    return $bytes;
  }

  /**
   * Append data to specified file with 'a' flags - append or create new,
   * then perform binary safe write.
   * @param string $filename file location to which data should be save
   * @param string $content content to be save in file
   * @param bool $lock should method shall lock file for writing
   * @param bool $blockLock should lock be blocking or nonblocking (dont stops code execution)
   * @param bool $throwBlock if lock nonblocking and not lock and this is true than throw exception Chors_ErrorCodes::FILE_LOCK_FAILED
   * @return int number of bytes wrote to file
   */
  public static function appendAsString($filename, $content, $lock = false, $blockLock = true, $throwBlock = true)
  {
    $handle = fopen($filename, 'a');
    if ($handle === FALSE)
    {
      throw new Chors_Exception(
        "Cannot obtain file handle " . $filename,
        Chors_ErrorCodes::FILE_CANNOT_CREATE_HANDLE,
        $filename);
    }
    try
    {

      if ($lock)
      {
        if ($blockLock)
        {
          $flags = LOCK_EX;
        }
        else
        {
          $flags = LOCK_EX | LOCK_NB;
        }

        if (flock($handle, $flags))
        {
          $bytes = fwrite($handle, $content);
          flock($handle, LOCK_UN);

          if ($bytes === FALSE)
          {
            throw new Chors_Exception(
              "File write failed",
              Chors_ErrorCodes::FILE_WRITE_FAILED);
          }
        }
        elseif ($throwBlock)
        {
          throw new Chors_Exception("File lock failed", Chors_ErrorCodes::FILE_LOCK_FAILED);
        }
      }
      else
      {
        $bytes = fwrite($handle, $content);

        if ($bytes === FALSE)
        {
          throw new Chors_Exception("File write failed", Chors_ErrorCodes::FILE_WRITE_FAILED);
        }
      }
      return $bytes;
    }
    catch(Exception $e)
    {
      fclose($handle);
      throw  $e;
    }
    fclose($handle);
//    finally
//    {
//      fclose($handle);
//    }
  }

  /**
   * @return bool <i>true</i> on file successfully uploaded or <i>false</i> when error occured
   * @param string $fileIdent name of file used in $_FILES array
   */
  public static function isUploaded($fileIdent, &$errorNotice)
  {
    $errorNotice = null;
    if (isset($_FILES[$fileIdent]) == false)
    {   // request failed
      $errorNotice = "Request failed";
      return false;
    }
    if (empty($_FILES[$fileIdent]['tmp_name']))
    {   // file not found - nothing uploaded
      switch ($_FILES[$fileIdent]['error'])
      {
        case UPLOAD_ERR_INI_SIZE:
          $errorNotice .= "The uploaded file exceeds the upload_max_filesize directive in php.ini";
          break;
        case UPLOAD_ERR_FORM_SIZE:
          $errorNotice .= "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
          break;
        case UPLOAD_ERR_PARTIAL:
          $errorNotice .= "The uploaded file was only partially uploaded";
          break;
        case UPLOAD_ERR_NO_FILE:
          $errorNotice .= "No file was uploaded";
          break;
        case UPLOAD_ERR_NO_TMP_DIR:
          $errorNotice .= "Missing a temporary folder";
          break;
        case UPLOAD_ERR_CANT_WRITE:
          $errorNotice .= "Failed to write file to disk";
          break;
        case UPLOAD_ERR_EXTENSION:
          $errorNotice .= "File upload stopped by extension";
          break;
        default:
          $errorNotice .= "Unknown upload error";
          break;
      }
      return false;
    }
    else
    {
      if (is_uploaded_file($_FILES[$fileIdent]['tmp_name']) == FALSE)
      { // nothing uploaded - error?
        $errorNotice = "File not found";
        return false;
      }
      else
      {
        return true;
      }
    }
  }

  /**
   * @
   * @param string $fileIdent name of file used in $_FILES array
   * @param string $uploadDirectory path to directory in which file should be stored
   * @param string $newFileName file save name or <i>null</i> if orginal name should be used
   */
  public static function handleUpload($fileIdent, $uploadDirectory, $newFileName = null)
  {
    if (self::isUploaded($fileIdent, $errorNotice) == FALSE)
    {
      throw new Chors_Exception("File upload error",
      $_FILES[$fileIdent]['error'], $errorNotice);
    }
    else
    {
      if ($newFileName == null)
        $newFileName = $_FILES[$fileIdent]['name'];

      $newFilePath = $uploadDirectory . DIRECTORY_SEPARATOR . $newFileName;

      if (is_dir($uploadDirectory) == false)
        mkdir($uploadDirectory, 0777, true);

      move_uploaded_file($_FILES[$fileIdent]['tmp_name'], $newFilePath);
      chmod($newFilePath, 0644);
    }
  }
}
