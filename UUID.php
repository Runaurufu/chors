<?php
/**
 * Description of Chors_UUID
 *
 * @author Runaurufu
 */
class Chors_UUID
{
  /** UUID in form XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX  */
  private $uuid;
  
  public function __construct($uuid)
  {
    $len = strlen($uuid);
    
    if(($len === 38 || $len === 34) && $uuid[0] === "{" && $uuid[$len-1] === "}")
    {
      $uuid = trim ($uuid, "{}");
      $len = strlen($uuid);
    }
    if($len === 32)
    {
      $uuid = substr ($uuid, 0, 8)."-".substr ($uuid, 8, 4)."-".substr ($uuid, 12, 4)."-".substr ($uuid, 16, 4)."-".substr ($uuid, 20, 12);
      $len = 36;
    }
    
    if($len === 36)
      $this->uuid = strtoupper($uuid);
    else
      throw new Chors_Exception("Improper UUID length", Chors_ErrorCodes::GENERIC_ARGUMENT_SIZE, "32 or 36 (hypens separated) chars expected", NULL);
  }
  
  public function getString($withHypens)
  {
    if($withHypens === FALSE)
      return str_replace('-', '', $this->uuid);
    else
      return $this->uuid;
  }
  
  public function __toString()
  {
    return $this->getString(FALSE);
  }
  
  public function getBinary()
  {
    return pack("H*", $this->getString(false));
  }
  
  public static function createFromBinary($binary)
  {
    $string = unpack("H*", $binary);
    $uuid = preg_replace("/([0-9a-f]{8})([0-9a-f]{4})([0-9a-f]{4})([0-9a-f]{4})([0-9a-f]{12})/", "$1-$2-$3-$4-$5", $string);
    return new Chors_UUID($uuid[1]);
  }
  
  public static function createRandom()
  {
    $d1 = Chors_String::getRandom(8, '0123456789ABCDEF');
    $d2 = Chors_String::getRandom(4, '0123456789ABCDEF');
    $d3 = Chors_String::getRandom(3, '0123456789ABCDEF');
    $d4 = Chors_String::getRandom(1, '89AB');
    $d5 = Chors_String::getRandom(3, '0123456789ABCDEF');
    $d6 = Chors_String::getRandom(12, '0123456789ABCDEF');
    
    return new Chors_UUID($d1."-".$d2."-4".$d3."-".$d4.$d5."-".$d6);
  }
}
