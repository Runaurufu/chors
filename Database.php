<?php

abstract class Chors_Database_DataType extends Chors_Enum
{
  /** Bit and bool data types */
  const BIT = 0;
  /** integer data type */
  const INT = 1;
  /** string data type */
  const STR = 2;
  /** binary data type */
  const BIN = 3;
  /** float and double data types */
  const FLO = 4;
  /** Chors_UUID data type */
  const UUID = 5;
}

/**
 * PreparedQuery class for unification of prepared queries across database providers.
 */
abstract class Chors_Database_PreparedQuery
{
  /**
   * Bind variables to prepared statement.
   * @param mixed $arguments each passed argument require pair of DATATYPE and binded variable reference
   */
  public abstract function bind($arguments = NULL);
  
  /**
   * Execute prepared query.
   */
  public abstract function execute();
  
  /**
   * Return results of executed query in form of array.
   */
  public abstract function getResults();
  
  /**
   * Return last id inserted to database.
   */
  public abstract function getLastInsertId();
  
  /**
   * Returns number of affected rows.
   * @return int Or string if value > PHP_INT_MAX
   */
  public abstract function getAffectedRows();
  
  /**
   * Close prepared query.
   */
  public abstract function close();
}

/**
 * Main database class it is used for creating all database classes.
 * @package Chors
 * @version 1.0
 * @since 1.0
 * @author Runaurufu
 */
abstract class Chors_Database
{
  static private $instances = array();
  protected $hostname = null;
  protected $port = null;
  protected $username = null;
  protected $password = null;
  //    protected $dbname = null;
  //    protected $flags = null;
  protected $socket = null;
  protected $prefix = '';
  protected $debugfile = null;
  protected $debugmode = null;

  /**
   * This constructor should be used only for IDE context help support purposes
   * If you provide settings param then it will also generate instance,
   * which can be later retrieved by using getInstance().
   * @see getInstance()
   * @param array $settings settings array to be applied to newly created database
   * @return Chors_Database dummy Chors_Database object - <b>do not use it</b> directly in your code to methods invocation
   */
  public function __construct($settings = null)
  {
    // dummy constructor just for creating reference for help in IDEs
    if ($settings === null)
    {
      return;
    }
    // creates static instance
    if (!isset(self::$instances[$settings['appName']]))
    {
      switch ($settings['type'])
      {
        case 'mysql':
          require_once('Database/MySQL.php');
          self::$instances[$settings['appName']] = new Chors_Database_MySQL($settings);
          break;
        case 'mysqli':
          require_once('Database/MySQLi.php');
          self::$instances[$settings['appName']] = new Chors_Database_MySQLi($settings);
          break;
        case 'postgresql':
          require_once('Database/PostgreSQL.php');
          self::$instances[$settings['appName']] = new Chors_Database_PostgreSQL($settings);
          break;
        default:
          throw new Chors_Exception("Database type not defined", Chors_Exception::ERR_DB_TYPE);
      }
    }
  }
  
  /**
   * Retrieve debug file associated with database connection.
   * @return string
   */
  public function getDebugFile()
  {
    return $this->debugfile;
  }
  
  /**
   * Inform if debug mode is active for this database connection.
   * @return bool
   */
  public function isDebugMode()
  {
    return $this->debugmode;
  }

  /**
   * Use it only to create new Database object. For using already existing connection use { @link Chors_DataBase::getInstance()}
   * @static
   * @param array $settings settings array containing all necessary information
   * @return Chors_Database proper type of DataBase object or <b>null</b> if designed type not found
   */
  static public function create($settings)
  {
    $newdb = null;
    switch ($settings['type'])
    {
      case 'mysql':
        require_once('Database/MySQL.php');
        $newdb = new Chors_Database_MySQL($settings);
        break;
      case 'mysqli':
        require_once('Database/MySQLi.php');
        $newdb = new Chors_Database_MySQLi($settings);
        break;
      case 'postgresql':
          require_once('Database/PostgreSQL.php');
          $newdb = new Chors_Database_PostgreSQL($settings);
          break;
      default:
        throw new Chors_Exception("Database type not defined", Chors_Exception::ERR_DB_TYPE);
    }
    self::$instances[$settings['appName']] = $newdb;

    return $newdb;
  }

  /**
   * @static
   * @param array $settings settings array containing all necessary information
   * @return Chors_Database proper type of Chors_Database object or <b>null</b> if designed type not found
   */
  static public function getInstance($settings = null)
  {
    if ($settings === null && count(self::$instances) === 0)
      throw new Chors_Exception("No default database defined", Chors_Exception::ERR_DB_NODEF);

    if (count(self::$instances) === 0)
    {
      switch ($settings['type'])
      {
        case 'mysql':
          require_once('Database/MySQL.php');
          self::$instances[$settings['appName']] = new Chors_Database_MySQL($settings);
          break;
        case 'mysqli':
          require_once('Database/MySQLi.php');
          self::$instances[$settings['appName']] = new Chors_Database_MySQLi($settings);
          break;
        case 'postgresql':
          require_once('Database/PostgreSQL.php');
          self::$instances[$settings['appName']] = new Chors_Database_PostgreSQL($settings);
          break;
        default:
          throw new Chors_Exception("Database type not defined", Chors_Exception::ERR_DB_TYPE);
      }
    }

    foreach (self::$instances as $connection)
      return $connection;
  }

  static public function getConnection($name)
  {
    if (isset(self::$instances[$name]))
      return self::$instances[$name];
    else
      return null;
  }

  /**
   * Returns defined prefix for that database's tables
   * @return string tables prefix
   */
  public function getPrefix()
  {
    return $this->prefix;
  }
  
  public abstract function query($query);
  
  /**
   * @return Chors_Database_PreparedQuery
   */
  public abstract function prepareQuery($query);

  public abstract function affectedRows();

  public abstract function numRows($resultSet);

  public abstract function lastInsertedId();
  
  public abstract function fetchArray($resultSet, $resultType='DEFAULT');

  public abstract function isConnected();

  public abstract function connect();

  public abstract function stringSafe($string);

  public abstract function arraySafe($array);

  public abstract function createBackup($backupDirectory, $tables = '*');

  public abstract function loadBackup($filename);
}
