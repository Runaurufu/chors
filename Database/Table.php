<?php

/**
 * If you are going to define your own __construct then be sure to implement
 * "parent::__construct()" inside. Without it default constructor in parent class
 * won't be initated.
 * @author Runaurufu
 * @todo descriptions
 */
class Chors_Database_Table extends Chors_AModel
{
  /** @var Chors_Database */
  protected $db = null;
  protected $name = null;

  public function __construct($databaseInstance = null)
  {
    if ($databaseInstance === null)
      $this->db = Chors_Database::getInstance();
    else
      $this->db = $databaseInstance;
  }

  /**
   * Perform all operations concerning caching database table data,
   * thus it can be used also by different datasources in modified way.
   * @param array $cacheSettings cache settings, described at Chors_Cache::parseSettings()
   * @link Chors_Cache::parseSettings()
   * @param string $modelAction action of current model to be invoked
   * @param mixed $argumentList list of arguments read using func_get_args
   */
  public final function cache($cacheSettings, $modelAction, $argumentList)
  {
    $identifier = get_class($this) . '.' . $modelAction;
    $cache = new Chors_Cache($cacheSettings, $identifier);
    if ($cache->check())
      return $cache->fetchDataSource();

    $numargs = func_num_args();
    $arg_list = func_get_args();
    $argArray = Array();
    for ($i = 2; $i < $numargs; $i++)
      $argArray[] = $arg_list[$i];

    $ret = call_user_func_array(array($this, $modelAction), $argArray);

    $cache->saveDataSource($ret);

    return $ret;
  }

  protected function query($sql)
  {
    return $this->db->query($sql);
  }
  
  protected function prepareQuery($query)
  {
    return $this->db->prepareQuery($query);
  }

  protected function affectedRows()
  {
    return $this->db->affectedRows();
  }

  protected function numRows($resultSet)
  {
    return $this->db->numRows($resultSet);
  }

  protected function lastInsertedId()
  {
    return $this->db->lastInsertedId();
  }

  protected function fetchArray($resultSet, $resultType = 'DEFAULT')
  {
    return $this->db->fetchArray($resultSet, $resultType);
  }

  protected function getPrefix()
  {
    return $this->db->getPrefix();
  }

  protected function safe($string)
  {
    return $this->db->safe($string);
  }

  protected function stringSafe($string)
  {
    return $this->db->safe($string);
  }

  protected function arraySafe($array)
  {
    $arr = array();
    foreach ($array as $key => $value)
      $arr[$key] = $this->db->safe($value);

    return $arr;
  }

  protected function getName()
  {
    return $this->db->getPrefix() . $this->name;
  }

  protected function transactionStart()
  {
    return $this->db->transactionStart();
  }

  protected function transactionCommit()
  {
    return $this->db->transactionCommit();
  }

  protected function transactionRollback()
  {
    return $this->db->transactionRollback();
  }
}
