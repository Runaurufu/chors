<?php

class Chors_Database_PreparedQuery_MySQLi extends Chors_Database_PreparedQuery
{
  /** @var mysqli_stmt */
  protected $statement = null;
  
  /** @var Chors_Database_MySQLi */
  protected $db = null;
  
  /**
   * 
   * @param Chors_Database_MySQLi $db
   * @param string $query
   */
  function __construct($db, $query)
  {
    $this->db = $db;
    $this->statement = $db->getUnderlyingLink()->prepare($query);
    $this->error($query);
  }
  
  /**
   * Bind variables to prepared statement
   * @param mixed $arguments each passed argument require pair of DATATYPE and binded variable reference
   */
  public function bind($arguments = NULL)
  {
    $args = func_get_args();
    if(count($args > 0))
    {
      if(count($args) % 2 != 0)
        throw new Chors_Exception("", Chors_Exception::ERR_DB_GENERIC);
      
      $types = "";
      $params = array();
      for ($index = 0; $index < count($args); $index+= 2)
      {
        switch ($args[$index])
        {
          case Chors_Database_DataType::BIT:
            $types .= "i";
            $params[] = &$args[$index + 1];
            break;
          case Chors_Database_DataType::INT:
            $types .= "i";
            $params[] = &$args[$index + 1];
            break;
          case Chors_Database_DataType::STR:
            $types .= "s";
            $params[] = &$args[$index + 1];
            break;
          case Chors_Database_DataType::BIN:
            $types .= "b";
            $params[] = &$args[$index + 1];
            break;
          case Chors_Database_DataType::FLO:
            $types .= "d";
            $params[] = &$args[$index + 1];
            break;
          case Chors_Database_DataType::UUID:
            $types .= "s";
            // get proper binary value of UUID
            if($args[$index + 1] instanceof Chors_UUID)
              $args[$index + 1] = $args[$index + 1]->getBinary();
            $params[] = &$args[$index + 1];
            break;
          default:
            throw new Exception ("Not supported datatype", Chors_Exception::ERR_DB_GENERIC);
        }
      }
      array_unshift($params, $types);
      call_user_func_array(array($this->statement, "bind_param"), Chors_Array::getValuesByReferences($params));
      $this->error();
    }
  }
    
  /**
  * @param Boolean $halt Will generate exception upon expierenced error when <i>true</i>
  * @todo rewrite it with using Chors_File class
  */
  private function error($data = null)
  {   
    $errno = $this->statement->errno;
    $error = $this->statement->error;

    if($errno !== 0)
    {      
      $errormsg = $errno." - ".$error;
      
      if($data !== null)
        $errormsg .= " Data: ".$data;

      if($this->db->isDebugMode() && $this->db->getDebugFile() !== null)
      {
        Chors_Logger::saveToLogFile($this->db->getDebugFile(), $errormsg, true);
      }

      throw new Chors_Exception("Database error encountered", Chors_Exception::ERR_DB_GENERIC, $errormsg);
    }
  }
  
  /**
   * Execute prepared statement with previously specified parameters
   */
  public function execute()
  {
    $this->statement->execute();
    $this->error();
  }
  
  /**
   * Get last execution results
   * @return mixed[]
   */
  public function getResults()
  {
    $meta = $this->statement->result_metadata();
    $this->error();
         
    $array = array();
    while ($field = $meta->fetch_field())
    {
      $array[] = &$row[$field->name];
    }  
    
    call_user_func_array(array($this->statement, 'bind_result'), Chors_Array::getValuesByReferences($array));
    $this->error();      
    
    $results = array();
    while ($this->statement->fetch())
    { 
      $this->error();
      $x = array(); 
      foreach( $row as $key => $val )
      { 
        $x[$key] = $val; 
      } 
      $results[] = $x; 
    }
    
    return $results;
  }
  
  /**
   * Return last id inserted to database.
   */
  public function getLastInsertId()
  {
    return $this->statement->insert_id;
  }
  
  /**
   * Returns number of affected rows.
   * @return int Or string if value > PHP_INT_MAX
   */
  public function getAffectedRows()
  {
    return $this->statement->affected_rows;
  }
  
  /**
   * Close prepared statement
   */
  public function close()
  {
    $this->statement->close();
    $this->error();
  }
}

/**
 * Description of MySQL
 *
 * @package Chors
 * @author Runaurufu
 */
class Chors_Database_MySQLi extends Chors_Database
{
  /** @var mysqli */
  private $db_link = null;
  private $host = null;
  private $dbname = null;
  private $flags = null;
  
  /**
   * Retrieve underlying database link.
   * @return mysqli
   */
  public function getUnderlyingLink()
  {
    return $this->db_link;
  }

  function __construct($settings)
  {
    if(!isset($settings['hostname']) && !isset($settings['socket']))
      throw new Chors_Exception("Hostname or socket not defined", Chors_Exception::ERR_DB_HOST);
    if(isset($settings['hostname']))
    {
      $this->hostname = $this->host = $settings['hostname'];
      if(isset($settings['port']))
      {
        $this->port=$settings['port'];
        $this->host=$this->hostname.":".$this->port;
      }
    }
    else
      $this->socket = $this->host = $settings['socket'];
    if(!isset($settings['username']))
      throw new Chors_Exception("Username not defined", Chors_Exception::ERR_DB_USER);
    $this->username = $settings['username'];
    if(!isset($settings['password']))
      throw new Chors_Exception("Password not defined", Chors_Exception::ERR_DB_PASS);
    $this->password = $settings['password'];
    if(!isset($settings['dbname']))
      throw new Chors_Exception("Database name not defined", self::ERROR_DBNAME);
    $this->dbname = $settings['dbname'];
    if(!isset($settings['flags']))
      $this->flags = 0;
    else
      $this->flags = $settings['flags'];
    if(isset($settings['prefix']))
      $this->prefix = $this->basicSafe($settings['prefix']);

    if(isset($settings['debugfile']))
      $this->debugfile = $settings['debugfile'];
    if(isset($settings['debugmode']))
      $this->debugmode = Chors_String::boolval($settings['debugmode']);
  }

  public function isConnected()
  {
    if($this->db_link === null)
      return false;
    else
      return true;
  }
  /**
  *
  */
  public function connect()
  {
    if($this->db_link !== null)
      throw new Chors_Exception("Already connected to database", Chors_Exception::ERR_DB_ALRCONN);

    $this->db_link = @new mysqli($this->hostname, $this->username, $this->password, $this->dbname, $this->port);
    
    $errno = mysqli_connect_errno();
    $error = mysqli_connect_error();
    
    if($errno !== 0)
    {      
      $errormsg = $errno." - ".$error;
      
      if($this->debugfile !== null)
      {
        Chors_Logger::saveToLogFile($this->debugfile, $errormsg, true);
      }

      throw new Chors_Exception("Cannot connect to database", Chors_Exception::ERR_DB_GENERIC, $errormsg);
    }

    if($this->db_link->set_charset("utf8") === false)
    {
      $this->query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");
    }
   }

  public function query($query)
  {
    $resultSet = $this->db_link->query($query, MYSQLI_STORE_RESULT);
    $this->error(TRUE, $query);
    return $resultSet;
  }
  
  public function prepareQuery($query)
  {
    return new Chors_Database_PreparedQuery_MySQLi($this, $query);
  }
  
  public function affectedRows()
  {
    $amount = (int)$this->db_link->affected_rows;
    $this -> error();
    return $amount;
  }

  public function numRows($resultSet)
  {
    $amount = (int) $resultSet->num_rows;
    $this -> error();
    return $amount;
  }

  public function lastInsertedId()
  {
    $res = $this->query('SELECT LAST_INSERT_ID()');
    $id=$this->fetchArray($res);
    return $id[0];
  }

  /**
  * @todo LANG associative?? chceck in dictionary
  * @param ResultSet $resultSet ResultSet returned by {@see Chors_Database_MySQL::query()};
  * @param Enum $resultType Available options:<br/>
  * <i>ASSOC</i> - create associative keys<br/>
  * <i>NUM</i> - creates only numeric keys<br/>
  * <i>BOTH</i> - creates both associative and numeric keys.<br/>
  * <i>DEFAULT</i> - use default base specific settings. It is default behaviour if any other value is inserted.<br/>
  * @return <type>
  */
  public function fetchArray($resultSet, $resultType='DEFAULT')
  {
    switch($resultType)
    {
      case 'ASSOC' : $array = $resultSet->fetch_array(MYSQL_ASSOC); break;
      case 'NUM' : $array = $resultSet->fetch_array(MYSQL_NUM); break;
      case 'BOTH' : $array = $resultSet->fetch_array(MYSQL_BOTH); break;
      default:
      case 'DEFAULT' : $array = $resultSet->fetch_array(); break;
    }
    $this -> error();
    return $array;
  }
  
  /**
  *
  * @param Boolean $halt Will generate exception upon expierenced error when <i>true</i>
  * @todo rewrite it with using Chors_File class
  */
  private function error($halt=TRUE, $sql = "")
  {
    if($this->db_link === false || $this->db_link === null)
    {
      $errormsg = "Database link not found";
      if($this->debugfile !== null)
      {
        Chors_Logger::saveToLogFile($this->debugfile, $errormsg, true);
      }

      if($halt === TRUE)
      {
        throw new Chors_Exception("Database error encountered", Chors_Exception::ERR_DB_GENERIC, $errormsg);
      }
    }
    else
    {      
      $errno = $this->db_link->errno; // mysqli_errno($this->db_link)
      $error = $this->db_link->error; // mysqli_error($this->db_link)
    
      if($errno!==0)
      {      
        $errormsg = $errno." - ".$error;
        if(strlen($sql) > 0)
        {
          $errormsg .= " SQL: " . $sql;
        }

        if($this->debugfile !== null)
        {
          Chors_Logger::saveToLogFile($this->debugfile, $errormsg, true);
        }

        if($halt === TRUE)
        {
          throw new Chors_Exception("Database error encountered", Chors_Exception::ERR_DB_GENERIC, $errormsg);
        }
      }
    }
  }

  public function basicSafe($string)
  {
    if(get_magic_quotes_gpc())
      $string=stripslashes($string);
    $string = str_replace(array('\'', '`'), array('\\\'', '\`'), $string);
    return $string;
  }
  
  public function arraySafe($array)
  {
    $arr = array();
    foreach ($array as $key => $value)
      $arr[$key] = $this->stringSafe($value);
    return $arr;
  }
  
  public function stringSafe($string)
  {
    if($this->db_link === null)
      throw new Chors_Exception("Not connected to database", Chors_Exception::ERR_DB_NOTCONN);

    if(get_magic_quotes_gpc())
      $string=stripslashes($string);
    $string = $this->db_link->real_escape_string($string);
    return $string;
  }

  public function safe($string)
  {
    return $this->stringSafe($string);
  }

  public function transactionStart()
  {
    if (version_compare(PHP_VERSION, '5.5.0') >= 0)
      $this->db_link->begin_transaction();
    else
    {
      $this->query("START TRANSACTION");
      $this->query("SET autocommit=0");
    }
  }

  public function transactionCommit()
  {
    if (version_compare(PHP_VERSION, '5.5.0') >= 0)
      $this->db_link->commit();
    else
    {
      $this->query("SET autocommit=1");
      $this->query("COMMIT");
    }
  }

  public function transactionRollback()
  {
    if (version_compare(PHP_VERSION, '5.5.0') >= 0)
      $this->db_link->rollback();
    else
    {
      $this->query("SET autocommit=1");
      $this->query("ROLLBACK");
    }
  }

  public function select($tableName, $columns = array(), $where = array())
  {
    if(count($columns) == 0)
    {
      $cols = "*";
    }
    else
    {
      foreach ($columns as &$value)
      {
        $value = "`".$this->safe($value)."`";
      }
      $cols = implode(",", $columns);
    }

    $query;
    if(count($where) == 0)
    {
      $sql = sprintf("SELECT %s FROM `%s`",
      $cols,
      $this->safe($tableName));
      $query = $this->db->prepareQuery($sql);
    }
    else
    {
      $bind = array();
      $whereString = "";
      foreach ($where as $key => $value)
      {
        $whereString .= $this->safe($key);
        if($value === NULL)
        {
          $whereString .= " IS NULL AND";
          continue;
        }

        $whereString .= "? AND";

        if(is_int($value))
        {
          $bind[] = Chors_Database_DataType::INT;
          $bind[] = $value;
        }
        elseif(is_float($value))
        {
          $bind[] = Chors_Database_DataType::FLO;
          $bind[] = $value;
        }
        elseif(is_bool($value))
        {
          $bind[] = Chors_Database_DataType::BIT;
          $bind[] = $value;
        }
        elseif($value instanceof Chors_UUID)
        {
          $bind[] = Chors_Database_DataType::BIN;
          $bind[] = $guid->getBinary();
        }
        else
        {
          $bind[] = Chors_Database_DataType::STR;
          $bind[] = $value;
        }
      }

      $whereString = substr($whereString, 0, strlen($whereString) - 3);
      
      $sql = sprintf("SELECT %s FROM `%s` WHERE %s", $cols, $this->safe($tableName), $whereString);
      
      $query = $this->db->prepareQuery($sql);
      
      call_user_func_array(array($query, "bind"), $bind);
            //Chors_Array::getValuesByReferences($params));
    }
    $query->execute();
    return $query->getResults();
  }

  public function insert($tableName, $data)
  {
    if(count($data) == 0 || !is_array($data))
    {
      $sql = "INSERT INTO `'.$tableName.'`";
      $this->db->query($sql);
      return $this->lastInsertedId();
    }
    
    $keys = array_keys($data);
    $values = array_values($data);
    
    $bind = array();
    $valuesString = "";
    foreach ($values as $value)
    {
      if($value === NULL)
      {
        $valuesString .= "NULL,";
        continue;
      }

      $valuesString .= "?,";

      if(is_int($value))
      {
        $bind[] = Chors_Database_DataType::INT;
        $bind[] = $value;
      }
      elseif(is_float($value))
      {
        $bind[] = Chors_Database_DataType::FLO;
        $bind[] = $value;
      }
      elseif(is_bool($value))
      {
        $bind[] = Chors_Database_DataType::BIT;
        $bind[] = $value;
      }
      elseif($value instanceof Chors_UUID)
      {
        $bind[] = Chors_Database_DataType::BIN;
        $bind[] = $guid->getBinary();
      }
      else
      {
        $bind[] = Chors_Database_DataType::STR;
        $bind[] = $value;
      }
    }
    
    $valuesString = substr($valuesString, 0, strlen($valuesString) - 1);
    
    $sql = sprintf("INSERT INTO `%s` (`%s`) VALUES (%s);",
      $tableName,
      implode('`,`', $keys),
      $valuesString);

    $query = $this->db->prepareQuery($sql);
    
    call_user_func_array(array($query, "bind"), $bind);
            //Chors_Array::getValuesByReferences($params));
    
    $query->execute();

    return $query->getLastInsertId();
  }

  public function update($tableName, $data, $where)
  {
    if(count($data) == 0 || !is_array($data))
    {
      return 0;
    }
    
    $bind = array();
    $dataString = "";
    foreach ($data as $key => $value)
    {
      $dataString .= $this->safe($key);
      if($value === NULL)
      {
        $dataString .= " = NULL,";
        continue;
      }

      $dataString .= "?,";

      if(is_int($value))
      {
        $bind[] = Chors_Database_DataType::INT;
        $bind[] = $value;
      }
      elseif(is_float($value))
      {
        $bind[] = Chors_Database_DataType::FLO;
        $bind[] = $value;
      }
      elseif(is_bool($value))
      {
        $bind[] = Chors_Database_DataType::BIT;
        $bind[] = $value;
      }
      elseif($value instanceof Chors_UUID)
      {
        $bind[] = Chors_Database_DataType::BIN;
        $bind[] = $guid->getBinary();
      }
      else
      {
        $bind[] = Chors_Database_DataType::STR;
        $bind[] = $value;
      }
    }
    
    $dataString = substr($dataString, 0, strlen($dataString) - 1);
    
    $sql = sprintf("UPDATE `%s` SET %s",
      $this->safe($tableName),
      $dataString);
    
    if(count($where) > 0)
    {
      $whereString = "";
    
      foreach ($data as $key => $value)
      {
        $whereString .= $this->safe($key);
        if($value === NULL)
        {
          $whereString .= " = NULL,";
          continue;
        }

        $whereString .= "?,";

        if(is_int($value))
        {
          $bind[] = Chors_Database_DataType::INT;
          $bind[] = $value;
        }
        elseif(is_float($value))
        {
          $bind[] = Chors_Database_DataType::FLO;
          $bind[] = $value;
        }
        elseif(is_bool($value))
        {
          $bind[] = Chors_Database_DataType::BIT;
          $bind[] = $value;
        }
        elseif($value instanceof Chors_UUID)
        {
          $bind[] = Chors_Database_DataType::BIN;
          $bind[] = $guid->getBinary();
        }
        else
        {
          $bind[] = Chors_Database_DataType::STR;
          $bind[] = $value;
        }
      }
      
      $whereString = substr($whereString, 0, strlen($whereString) - 1);
      
      $sql .= sprintf(" WHERE %s",
        $whereString);
    }  
    
    $query = $this->db->prepareQuery($sql);
    $query->execute();
    return $query->getAffectedRows();
  }

  public function delete($tableName, $where)
  {
    $query = NULL;
    if(count($where) == 0)
    {
      $sql = sprintf("DELETE FROM `%s`",
        $this->safe($tableName));
      $query = $this->db->prepareQuery($sql);
    }
    else
    {
      $bind = array();
      $whereString = "";
      foreach ($where as $key => $value)
      {
        $whereString .= $this->safe($key);
        if($value === NULL)
        {
          $whereString .= " IS NULL AND";
          continue;
        }

        $whereString .= "? AND";

        if(is_int($value))
        {
          $bind[] = Chors_Database_DataType::INT;
          $bind[] = $value;
        }
        elseif(is_float($value))
        {
          $bind[] = Chors_Database_DataType::FLO;
          $bind[] = $value;
        }
        elseif(is_bool($value))
        {
          $bind[] = Chors_Database_DataType::BIT;
          $bind[] = $value;
        }
        elseif($value instanceof Chors_UUID)
        {
          $bind[] = Chors_Database_DataType::BIN;
          $bind[] = $guid->getBinary();
        }
        else
        {
          $bind[] = Chors_Database_DataType::STR;
          $bind[] = $value;
        }
      }

      $whereString = substr($whereString, 0, strlen($whereString) - 3);
      
      $sql = sprintf("DELETE FROM `%s` WHERE %s", $this->safe($tableName), $whereString);
      
      $query = $this->db->prepareQuery($sql);
      
      call_user_func_array(array($query, "bind"), $bind);
            //Chors_Array::getValuesByReferences($params));
    }
    $query->execute();
    return $query->getAffectedRows();
  }

  public function createBackup($backupDirectory, $tables = '*')
  {
    if($this->isConnected() == false)
    $this->connect ();

    //get all of the tables
    if($tables == '*')
    {
      $tables = array();
      $result = $this->query("SHOW TABLES");
      while($row = mysql_fetch_row($result))
      {
        $tables[] = $row[0];
      }
    }
    else
    {
      $tables = is_array($tables) ? $tables : explode(',',$tables);
    }
    $content = "";
    //cycle through
    foreach($tables as $table)
    {
      $result = $this->query('SELECT * FROM '.$table);
      $num_fields = mysql_num_fields($result);

      $content.= 'DROP TABLE '.$table.';';
      $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
      $content.= "\n\n".$row2[1].";\n\n";

      for ($i = 0; $i < $num_fields; $i++) 
      {
        while($row = mysql_fetch_row($result))
        {
          $content.= 'INSERT INTO '.$table.' VALUES(';
          for($j=0; $j<$num_fields; $j++) 
          {
            $row[$j] = addslashes($row[$j]);
            $row[$j] = ereg_replace("\n","\\n",$row[$j]);
            if (isset($row[$j])) { $content.= '"'.$row[$j].'"' ; } else { $content.= '""'; }
            if ($j<($num_fields-1)) { $content.= ','; }
          }
          $content.= ");\n";
        }
      }
      $content.="\n\n\n";
    }

    //save file
    $filename = 'db-backup-'.Chors_Time::utcDatetime().'-'.(md5(implode(',',$tables))).'.sql';
    Chors_File::writeAsString($backupDirectory.DIRECTORY_SEPARATOR.$filename, $content);
  }

  public function loadBackup($filename)
  {
    throw new Chors_Exception("Method not implemented yet", Chors_Exception::ERR_GENERIC_OPTNTIMP);
  }
}
