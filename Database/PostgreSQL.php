<?php

class Chors_Database_PreparedQuery_PostgreSQL extends Chors_Database_PreparedQuery
{
  /** @var string */
  protected $statement = null;
  
  /** @var Chors_Database_PostgreSQL */
  protected $db = null;
  
  protected $boundParameters = null;


  protected $lastResult = null;


  /**
   * 
   * @param Chors_Database_PostgreSQL $db
   * @param string $query
   */
  function __construct($db, $query)
  {    
    $this->db = $db;
    $this->statement = pg_prepare ($this->db->getUnderlyingLink(), "pgQuery", $query);
    if($this->statement === FALSE)
      $this->error($query);
  }
  
  /**
   * Bind variables to prepared statement
   * @param type $arguments
   */
  public function bind($arguments = NULL)
  {
    $args = func_get_args();
    if(count($args > 0))
    {
      if(count($args) % 2 != 0)
        throw new Exception ("", Chors_Exception::ERR_DB_GENERIC);
      
      $params = array();
      
      for ($index = 0; $index < count($args); $index+= 2)
      {
        $type = $args[$index];
        $value = $args[$index + 1];
        
        if($type === Chors_Database_DataType::BIT)
        {
          if($value === TRUE)
            $value = "true";
          else if($value === FALSE)
            $value = "false";
        }
        
        $params[] = &$value;
      }
      $this->boundParameters = $params;
    }
  }
  
   /**
  * @param Boolean $halt Will generate exception upon expierenced error when <i>true</i>
  * @todo rewrite it with using Chors_File class
  */
  private function error($data = null)
  {   
    $error = pg_last_error($this->db->getUnderlyingLink());
    if($error !== FALSE)
    {
      if($data !== null)
        $error .= " Data: ".$data;

      if($this->db->isDebugMode() && $this->db->getDebugFile() !== null)
      {
        Chors_Logger::saveToLogFile($this->db->getDebugFile(), $error, true);
      }

      throw new Chors_Exception("Database error encountered", Chors_Exception::ERR_DB_GENERIC, $error);
    }
  }
  
  /**
   * Execute prepared statement with specified parameters
   * @param type $types
   * @param type $params
   */
  public function execute()
  {
    $this->lastResult = pg_execute($this->db->getUnderlyingLink(), "pgQuery", $this->boundParameters);
    if($this->lastResult === FALSE)
      $this->error();
  }
  
  /**
   * Get last execution results
   * @return mixed[]
   */
  public function getResults()
  {
    $array = array();
    while($raw = $this->fetchArray($this->lastResult, 'ASSOC'))
    {
      $array[] = $raw;
    }
    return $array;
  }
  
  public function getLastInsertId()
  {
    return $this->db->lastInsertedId();
  }
  
  /**
   * Returns number of affected rows.
   * @return int Or string if value > PHP_INT_MAX
   */
  public function getAffectedRows()
  {
    return $this->db->affectedRows();
  }
  
  /**
   * Close prepared statement
   */
  public function close()
  {
    $this->statement = null;
    if($this->lastResult !== NULL)
      mysql_free_result($this->lastResult);
  }
}

/**
 * Description of Chors_Database_PostgreSQL
 *
 * @package Chors
 * @version 1.0
 * @since 1.0
 * @author Runaurufu
 */
class Chors_Database_PostgreSQL extends Chors_Database
{
  private $db_link = null;
  private $host = null;
  private $dbname = null;
  private $flags = null;

  /**
   * Retrieve underlying database link.
   * @return resource
   */
  public function getUnderlyingLink()
  {
    return $this->db_link;
  }
  
  function __construct($settings)
  {
    if(!isset($settings['hostname']) && !isset($settings['socket']))
      throw new Chors_Exception("Hostname or socket not defined", Chors_Exception::ERR_DB_HOST);
    if(isset($settings['hostname']))
    {
      $this->hostname = $this->host = $settings['hostname'];
      if(isset($settings['port']))
      {
        $this->port=$settings['port'];
        $this->host=$this->hostname.":".$this->port;
      }
    }
    else
      $this->socket = $this->host = $settings['socket'];
    if(!isset($settings['username']))
      throw new Chors_Exception("Username not defined", Chors_Exception::ERR_DB_USER);
    $this->username = $settings['username'];
    if(!isset($settings['password']))
      throw new Chors_Exception("Password not defined", Chors_Exception::ERR_DB_PASS);
    $this->password = $settings['password'];
    if(!isset($settings['dbname']))
      throw new Chors_Exception("Database name not defined", self::ERROR_DBNAME);
    $this->dbname = $settings['dbname'];
    if(!isset($settings['flags']))
      $this->flags = 0;
    else
      $this->flags = $settings['flags'];
    if(isset($settings['prefix']))
      $this->prefix = $this->basicSafe($settings['prefix']);

    if(isset($settings['debugfile']))
      $this->debugfile = $settings['debugfile'];
    if(isset($settings['debugmode']))
      $this->debugmode = Chors_String::boolval($settings['debugmode']);
  }

  public function isConnected()
  {
    if($this->db_link === null)
      return false;
    else
      return true;
  }
  /**
  *
  */
  public function connect()
  {
    if($this->db_link !== null)
      throw new Chors_Exception("Already connected to database", Chors_Exception::ERR_DB_ALRCONN);

    $connectionString = sprintf("host=%s user=%s password=%s dbname=%s",
      $this->host,
      $this->username,
      $this->password,
      $this->dbname);
    $this->db_link = @pg_connect($connectionString, PGSQL_CONNECT_FORCE_NEW);
    $this->error();

    //$this->query('SET CHARACTER SET utf8');
  }


  public function query($query)
  {
    $resultSet = pg_query($query, $this->db_link);
    $this->error(TRUE, $query);
    return $resultSet;
  }
  
  public function prepareQuery($query)
  {
    return new Chors_Database_PreparedQuery_PostgreSQL($this, $query);
  }

  public function affectedRows($resultSet)
  {
    $amount = (int) pg_affected_rows($result);
    $this->error();
    return $amount;
  }

  public function numRows($resultSet)
  {
    $amount = (int) pg_num_rows($resultSet);
    if($amount == -1)
      $this->error();
    return $amount;
  }

  public function lastInsertedId()
  {
    $res = $this->query('SELECT lastval();');
    $id=$this->fetchArray($res);
    return $id[0];
  }

  /**
  * @todo LANG associative?? chceck in dictionary
  * @param ResultSet $resultSet ResultSet returned by {@see Chors_Database_MySQL::query()};
  * @param Enum $resultType Available options:<br/>
  * <i>ASSOC</i> - create associative keys<br/>
  * <i>NUM</i> - creates only numeric keys<br/>
  * <i>BOTH</i> - creates both associative and numeric keys.<br/>
  * <i>DEFAULT</i> - use default base specific settings. It is default behaviour if any other value is inserted.<br/>
  * @return <type>
  */
  public function fetchArray($resultSet, $resultType='DEFAULT')
  {
    switch($resultType)
    {
      case 'ASSOC' : $array = pg_fetch_array($resultSet, null, PGSQL_ASSOC); break;
      case 'NUM' : $array = pg_fetch_array($resultSet, null, PGSQL_NUM); break;
      case 'BOTH' : $array = pg_fetch_array($resultSet, null, PGSQL_BOTH); break;
      default:
      case 'DEFAULT' : $array = pg_fetch_array($resultSet); break;
    }
    $this -> error();
    return $array;
  }

  /**
  *
  * @param Boolean $halt Will generate exception upon expierenced error when <i>true</i>
  * @todo rewrite it with using Chors_File class
  */
  private function error($halt=TRUE, $sql = "")
  {
    if($this->db_link === false || $this->db_link === null)
    {
      $error = pg_last_error();
    }
    else
    {
      $error = pg_last_error($this->db_link);
    }

    if($error!== FALSE)
    {
      $errormsg = $error;
      if(strlen($sql) > 0)
      {
        $errormsg .= " SQL: " . $sql;
      }

      if($this->debugfile !== null)
      {
        Chors_Logger::saveToLogFile($this->debugfile, $errormsg, true);
      }

      if($halt===TRUE)
      {
        throw new Chors_Exception("Database error encountered", Chors_Exception::ERR_DB_GENERIC, $errormsg);
      }
    }
  }

  public function basicSafe($string)
  {
    if(get_magic_quotes_gpc())
      $string=stripslashes($string);
    $string = str_replace(array('\'', '`'), array('\\\'', '\`'), $string);
    return $string;
  }
  
  public function arraySafe($array)
  {
    $arr = array();
    foreach ($array as $key => $value)
      $arr[$key] = $this->stringSafe($value);
    return $arr;
  }
  
  public function stringSafe($string)
  {
    if(get_magic_quotes_gpc())
      $string=stripslashes($string);
    $string = pg_escape_string($this->db_link, $string);
    return $string;
  }

  public function safe($string)
  {
    return $this->stringSafe($string);
  }

  public function transactionStart()
  {
    $this->query("BEGIN");
  }

  public function transactionCommit()
  {
    $this->query("COMMIT");
  }

  public function transactionRollback()
  {
    $this->query("ROLLBACK");
  }

  public function select($tableName, $columns = array(), $where = array())
  {
    if(count($columns) == 0)
    {
      $cols = "*";
    }
    else
    {
      foreach ($columns as &$value)
      {
        $value = "`".$this->safe($value)."`";
      }
      $cols = implode(",", $columns);
    }

    if(count($where) == 0)
    {
      $sql = sprintf("SELECT %s FROM `%s`",
      $cols,
      $this->safe($tableName));
    }
    else
    {
      foreach ($where as $key => &$value)
      {
        switch(true)
        {
          case $value == null: $value = sprintf("`%s` IS NULL",$key);
            break;
          case is_numeric($value): $value = sprintf("`%s` = %s",$key,$this->safe($value));
            break;
          default:
            $value = sprintf("`%s` = '%s'",$key,$this->safe($value));
        }
      }
      $wh = implode(" AND ", $where);
      $sql = sprintf("SELECT %s FROM `%s` WHERE %s",$cols,$this->safe($tableName), $wh);
    }
    $res = $this->query($sql);

    $results = array();

    while($raw = $this->fetchArray($res, 'ASSOC'))
    {
      $results[] = $raw;
    }
    return $results;
  }

  public function insert($tableName, $data)
  {
    if(count($data) == 0 || !is_array($data))
    {
      return 0;
    }

    $values = array_map('mysql_real_escape_string', array_values($inserts));
    $keys = array_keys($inserts);

    $sql = 'INSERT INTO `'.$tableName.'` (`'.implode('`,`', $keys).'`) VALUES (\''.implode('\',\'', $values).'\')';

    $res = $this->query($sql);

    return $this->lastInsertedId();
  }

  public function update($tableName, $data, $where)
  {
    if(count($data) == 0 || !is_array($data))
    {
      return 0;
    }

    foreach ($data as $key => &$value)
    {
      switch(true)
      {
        case $value == null: $value = sprintf("`%s` = NULL",$key);
          break;
        case is_numeric($value): $value = sprintf("`%s` = %s",$key,$this->safe($value));
          break;
        default:
          $value = sprintf("`%s` = '%s'",$key,$this->safe($value));
      }
    }
    $vals = implode(", ", $data);

    if(count($where) == 0)
    {
      $sql = sprintf("UPDATE `%s` SET %s",
      $this->safe($tableName),
      $vals);
    }
    else
    {
      foreach ($where as $key => &$value)
      {
        switch(true)
        {
          case $value == null: $value = sprintf("`%s` IS NULL",$key);
            break;
          case is_numeric($value): $value = sprintf("`%s` = %s",$key,$this->safe($value));
            break;
          default:
            $value = sprintf("`%s` = '%s'",$key,$this->safe($value));
        }
      }
      $wh = implode(" AND ", $where);
      $sql = sprintf("UPDATE `%s` SET %s WHERE %s",
      $this->safe($tableName),
      $vals, $wh);
    }

    $res = $this->query($sql);

    return $this->affectedRows();
  }

  public function delete($tableName, $where)
  {
    if(count($where) == 0)
    {
      $sql = sprintf("DELETE FROM `%s`",
      $this->safe($tableName));
    }
    else
    {
      foreach ($where as $key => &$value)
      {
        switch(true)
        {
          case $value == null: $value = sprintf("`%s` IS NULL",$key);
            break;
          case is_numeric($value): $value = sprintf("`%s` = %s",$key,$this->safe($value));
            break;
          default:
            $value = sprintf("`%s` = '%s'",$key,$this->safe($value));
        }
      }
      $wh = implode(" AND ", $where);
      $sql = sprintf("DELETE FROM `%s` WHERE %s", $this->safe($tableName), $wh);
    }
    $res = $this->query($sql);

    $results = array();

    return $this->affectedRows();
  }

  public function createBackup($backupDirectory, $tables = '*')
  {
    if($this->isConnected() == false)
    $this->connect ();

    //get all of the tables
    if($tables == '*')
    {
      $tables = array();
      $result = $this->query("select table_name from information_schema.tables WHERE table_schema='public'");
      while($row = mysql_fetch_row($result))
      {
        $tables[] = $row[0];
      }
    }
    else
    {
      $tables = is_array($tables) ? $tables : explode(',',$tables);
    }
    $content = "";
    //cycle through
    foreach($tables as $table)
    {
      $result = $this->query('SELECT * FROM '.$table);
      $num_fields = mysql_num_fields($result);

      $content.= 'DROP TABLE '.$table.';';
      $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
      $content.= "\n\n".$row2[1].";\n\n";

      for ($i = 0; $i < $num_fields; $i++) 
      {
        while($row = mysql_fetch_row($result))
        {
          $content.= 'INSERT INTO '.$table.' VALUES(';
          for($j=0; $j<$num_fields; $j++) 
          {
            $row[$j] = addslashes($row[$j]);
            $row[$j] = ereg_replace("\n","\\n",$row[$j]);
            if (isset($row[$j])) { $content.= '"'.$row[$j].'"' ; } else { $content.= '""'; }
            if ($j<($num_fields-1)) { $content.= ','; }
          }
          $content.= ");\n";
        }
      }
      $content.="\n\n\n";
    }

    //save file
    $filename = 'db-backup-'.Chors_Time::utcDatetime().'-'.(md5(implode(',',$tables))).'.sql';
    Chors_File::writeAsString($backupDirectory.DIRECTORY_SEPARATOR.$filename, $content);
  }

  public function loadBackup($filename)
  {
    throw new Chors_Exception("Method not implemented yet", Chors_Exception::ERR_GENERIC_OPTNTIMP);
  }
}
