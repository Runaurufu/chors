<?php

/**
 * @package Chors
 * @author Runaurufu
 * @version 1.0
 * @since 1.0
 */
class Chors_Auth
{
  /** @var Chors_Auth */
  static protected $instance;
  /** @var Chors_Database */
  static protected $db = null;

  /**
   * 'table' = tableName,
   * 'id' = idCol, 
   * 'level' => $levelCol,
   * 'name' => $nameCol,
   * 'timestamp' => $timestampCol,
   * 'localeId' => $localizationCol);
   * @var Array
   */
  static protected $dbAccount = null;

  /**
   * 'table' = table name
   * 'id' = password id
   * 'accountId' = related account id
   * 'login' = login column
   * 'password' = password column
   * 'timestamp' = last change
   * @var Array
   */
  static protected $dbAuthPassword = null;
  /**
   * 'table' = table name
   * 'id' = id column
   * 'accountId' = account id column
   * 'facebookId' = facebook id
   * @var Array
   */
  static protected $dbAuthFacebook = null;
  /**
   * 'table' = table name
   * 'id' = id column
   * 'accountId' = account id column
   * 'identityGUID' = identity guid column (binary 16)
   * @var Array 
   */
  static protected $dbAuthRunaAccount = null;
  /**
   * 'table' = table name
   * 'id' = id column
   * 'accountId' = account id column
   * 'sid' = session SID column
   * 'startTime' = session start date time
   * 'ip' = session ip column
   * @var Array
   */
  static protected $dbSession = null;
  static protected $dbAutologin = null;  // -> AuthAutologin
  static protected $dbLog = null;
  static protected $dbLocalization = null;
  static protected $annon = null;
  static protected $userId = null;
  static protected $userName = null;
  static protected $userLevel = null;
  static protected $userLogged = null;
  static protected $userStart = null;
  static protected $userLastRenew = null;
  static protected $sessionTime = 3600;
  static protected $isUniqueUserNameRequired = FALSE;
  static protected $isMultipleSessionsSupportActive = false;
  static protected $sessionRenewable = false;
  static protected $sessionRenewTime = 3600;
  static protected $autologinLife = 86400;
  static protected $autologSingleMachine = false;
  static protected $autologinHashFunction = "md5";
  static protected $localizationActive = false;
  static protected $passwordSalt = null;
  static protected $newUserLevel = 1;
  
  static protected $isFacebookAuthActive = false;
  /** @var Facebook */
  static protected $facebook = null;
  static protected $facebookAppId = null;
  static protected $facebookAppSecret = null;

  static protected $isRunaAccountAuthActive = false;
  /** @var RunaAccount */
  static protected $runaAccount = null;
  static protected $runaAccountAppKey = null;
  static protected $runaAccountAppSecret = null;
  
  public function __construct()
  {
    self::$instance = $this;
    self::init();
  }
  
  static protected function init()
  {
    self::$db = Chors_Database::getInstance();
    if (!self::$db->isConnected())
      self::$db->connect();

    Chors_Session::setTime(self::$sessionTime);
    if (!Chors_Session::isActive())
      Chors_Session::start();

    Chors_Session::setNamespace('Chors_AUTH');

    if (self::$localizationActive)
      if (!isset(self::$dbAccount['localeId']) || !isset(self::$dbLocalization['id']) || !isset(self::$dbLocalization['name']))
        self::$localizationActive = false;

    $setAsAnnon = true;
    if (Chors_Session::isValue("userLogged"))
      $setAsAnnon = !self::setUserFromSession();
    
    if($setAsAnnon)
    {
      self::setSessionAnnon();
      self::getInstance()->cmd_firstVisit();
    }
  }

  /**
   * 
   */
  static public function verificate()
  {
    if (self::$userLogged)
    {
      if ((self::$userLastRenew + self::$sessionTime >= Chors_Time::nowTimestamp()
        || self::renewCheck()) && self::sessionCheck())
      {
        // all additional checks for session success checks
        return;
      }
    }

    $authed = FALSE;
    $authed |= self::runaAccount();
    $authed |= self::facebook();
    $authed |= self::autologin();
    
    if (self::$userLogged && $authed === FALSE)
    {
      // user will be logged out as no verification passed
      self::logout();
    }
  }

  /**
   * 
   * @return boolean if TRUE - user data were set, if FALSE then something went wrong
   */
  static private function setUserFromSession()
  {
    if(Chors_Session::isValue("userId") === FALSE
      || Chors_Session::isValue("userName") === FALSE
      || Chors_Session::isValue("userLevel") === FALSE
      || Chors_Session::isValue("userLogged") === FALSE
      || Chors_Session::isValue("userStart") === FALSE
      || Chors_Session::isValue("userLastRenew") === FALSE)
      return FALSE;
    self::$userId = Chors_Session::getValue("userId");
    self::$userName = Chors_Session::getValue("userName");
    self::$userLevel = Chors_Session::getValue("userLevel");
    self::$userLogged = Chors_Session::getValue("userLogged");
    self::$userStart = Chors_Session::getValue("userStart");
    self::$userLastRenew = Chors_Session::getValue("userLastRenew");
    if (self::$localizationActive)
    {
      if(Chors_Session::isValue("localeName") === FALSE
        || Chors_Session::isValue("localeId") === FALSE)
        return FALSE;
      Chors_Localization::setLanguage(Chors_Session::getValue("localeName"), Chors_Session::getValue("localeId"));
    }
    return TRUE;
  }

  static private function setSessionFromUser()
  {
    Chors_Session::setValue("userLogged", self::$userLogged);
    Chors_Session::setValue("userStart", self::$userStart);
    Chors_Session::setValue("userId", self::$userId);
    Chors_Session::setValue("userName", self::$userName);
    Chors_Session::setValue("userLevel", self::$userLevel);
    Chors_Session::setValue("userLastRenew", self::$userLastRenew);
    if (self::$localizationActive)
    {
      Chors_Session::setValue("localeName", Chors_Localization::getCurrentLanguageName());
      Chors_Session::setValue("localeId", Chors_Localization::getCurrentLanguageId());
    }
  }

  static private function setSessionAnnon()
  {
    Chors_Session::setNamespace('Chors_AUTH');
    if (!Chors_Session::isActive())
      Chors_Session::start();

    Chors_Session::setValue("userLogged", false);
    Chors_Session::setValue("userStart", Chors_Time::nowTimestamp());
    Chors_Session::setValue("userLastRenew", Chors_Time::nowTimestamp());
    self::$userLogged = false;
    self::$userStart = Chors_Time::nowTimestamp();
    if (self::$annon !== null)
    {
      Chors_Session::setValue("userId", self::$annon['id']);
      Chors_Session::setValue("userName", self::$annon['name']);
      Chors_Session::setValue("userLevel", self::$annon['level']);
      self::$userId = self::$annon['id'];
      self::$userName = self::$annon['name'];
      self::$userLevel = self::$annon['level'];
      if (self::$localizationActive)
        Chors_Localization::setLanguageFromUserPreferences(self::$annon['localeName'], self::$annon['localeId']);
    }
    else
    {
      Chors_Session::setValue("userId", null);
      Chors_Session::setValue("userName", null);
      Chors_Session::setValue("userLevel", null);
      self::$userId = null;
      self::$userName = null;
      self::$userLevel = null;
    }
  }

  //TODO
  static public function setAnnon($userId, $userName, $userLevel, $localeName = null, $localeId = null)
  {
    self::$annon = array(
        'id' => $userId,
        "name" => $userName,
        "level" => $userLevel,
        "localeName" => $localeName,
        "localeId" => $localeId);
  }

// <editor-fold defaultstate="collapsed" desc="Database Tables Setters">
  static public function setAccountTable($tableName, $idCol, $levelCol, $nameCol, $timestampCol, $localizationCol = null)
  {
    self::$dbAccount = array(
        'table' => $tableName,
        'id' => $idCol,
        'level' => $levelCol,
        'name' => $nameCol,
        'timestamp' => $timestampCol,
        'localeId' => $localizationCol);
  }

  static public function setAuthPasswordTable($tableName, $idCol, $accountCol, $loginCol, $passCol, $timestampCol)
  {
    self::$dbAuthPassword = array(
        'table' => $tableName,
        'id' => $idCol,
        'accountId' => $accountCol,
        'login' => $loginCol,
        'password' => $passCol,
        'timestamp' => $timestampCol);
  }

  static public function setAuthFacebookTable($tableName, $idCol, $accountCol, $facebookId)
  {
    self::$dbAuthFacebook = array(
        'table' => $tableName,
        'id' => $idCol,
        'accountId' => $accountCol,
        'facebookId' => $facebookId);
  }


  static public function setAuthRunaAccountTable($tableName, $idCol, $accountCol, $identityGUIDCol)
  {
    self::$dbAuthRunaAccount = array(
        'table' => $tableName,
        'id' => $idCol,
        'accountId' => $accountCol,
        'identityGUID' => $identityGUIDCol);
  }

  static public function setSessionTable($tableName, $idColumn, $accountIdColumn, $sidColumn, $startTimeColumn, $ipColumn)
  {
    self::$dbSession = array(
        'table' => $tableName,
        'id' => $idColumn,
        'accountId' => $accountIdColumn,
        'sid' => $sidColumn,
        'startTime' => $startTimeColumn,
        'ip' => $ipColumn);
  }

  static public function setAutologinTable($tableName, $idCol, $logIdCol, $alogCol, $timeCol, $cookieName)
  {
    self::$dbAutologin = array(
        'table' => $tableName,
        'id' => $idCol,
        'logId' => $logIdCol,
        'autologin' => $alogCol,
        'time' => $timeCol,
        'cookie' => $cookieName); //cookie name for PHP purposes only - not save in DB
  }

  /**
   *
   * @param String $tableName
   * @param String $logIdCol <b>can be null</b>
   * @param String $idCol
   * @param String $timeCol
   * @param String $ipCol
   * @param String $userAgentCol
   */
  static public function setLogTable($tableName, $logIdCol, $idCol, $timeCol, $ipCol, $userAgentCol)
  {
    self::$dbLog = array(
        'table' => $tableName,
        'logId' => $logIdCol,
        'id' => $idCol,
        'time' => $timeCol,
        'ip' => $ipCol,
        'userAgent' => $userAgentCol);
  }

  static public function setLocalizationTable($tableName, $idCol, $nameCol)
  {
    self::$dbLocalization = array(
        'table' => $tableName,
        'id' => $idCol,
        'name' => $nameCol);
  }
// </editor-fold>

  static public function setPasswordSalt($newSalt)
  {
    self::$passwordSalt = $newSalt;
  }

  static public function setTimes($sessionTime = 3600, $sessionRenewable = false, $sessionRenewTime = 3600)
  {
    self::$sessionTime = $sessionTime;
    self::$sessionRenewable = $sessionRenewable;
    self::$sessionRenewTime = $sessionRenewTime;
  }

  static public function setAutologinOptions($autologinLife = 86400, $autologSingleMachine = false, $autologinHashFunction = "md5")
  {
    self::$autologinLife = $autologinLife;
    self::$autologSingleMachine = $autologSingleMachine;
    self::$autologinHashFunction = $autologinHashFunction;
  }
  
  static public function activateMultipleSessionsSupport()
  {
    self::$isMultipleSessionsSupportActive = true;
  }

  static public function activateLocalizationSupport()
  {
    self::$localizationActive = true;
  }
  
  public static function activateUniqueUserNamesRequirement()
  {
    self::$isUniqueUserNameRequired = TRUE;
  }

  /**
   * Saves current SID in database and makes sure that it is only one existing at a time
   * @return void
   */
  static protected function session()
  {
    if (self::$dbSession === null)
      return;
    
    $sid = Chors_Session::getSID();
    if(self::$isMultipleSessionsSupportActive === FALSE)
    {
      $sql = sprintf("SELECT `%s`, `%s` FROM `%s%s` WHERE `%2\$s`='%s'", self::$db->safe(self::$dbSession['accountId']), self::$db->safe(self::$dbSession['sid']), self::$db->getPrefix(), self::$db->safe(self::$dbSession['table']), self::$db->safe(self::$userId));
      $res = self::$db->query($sql);
      $amount = self::$db->numRows($res);
      if ($amount > 0)
      {
        while ($raw = self::$db->fetchArray($res, 'ASSOC'))
        {
          if ($raw[self::$dbSession['sid']] == $sid)
            continue;
          Chors_Session::start(null, $raw[self::$dbSession['sid']]);
          Chors_Session::destroy();
        }
      }
    }
    Chors_Session::start(null, $sid);
    self::setSessionFromUser();
    
    if(self::$isMultipleSessionsSupportActive)
      self::sessionClear();
    else
    {      
      $sql = sprintf("DELETE FROM `%s%s` WHERE `%s`='%s' OR `%s`='%s'", self::$db->getPrefix(), self::$db->safe(self::$dbSession['table']), self::$db->safe(self::$dbSession['accountId']), self::$db->safe(self::$userId), self::$db->safe(self::$dbSession['sid']), self::$db->safe(Chors_Session::getSID()));
      self::$db->query($sql);
    }

    $sql = sprintf("INSERT INTO `%s%s` (`%s`, `%s`, `%s`, `%s`) VALUES ('%s', '%s', '%s', '%s')", self::$db->getPrefix(), self::$db->safe(self::$dbSession['table']), self::$db->safe(self::$dbSession['accountId']), self::$db->safe(self::$dbSession['sid']), self::$db->safe(self::$dbSession['startTime']), self::$db->safe(self::$dbSession['ip']), self::$db->safe(self::$userId), self::$db->safe(Chors_Session::getSID()), self::$db->safe(Chors_Time::timestampToString(self::$userStart)), self::$db->safe($_SERVER['REMOTE_ADDR']));
    self::$db->query($sql);
  }

  /**
   * Checks if this session exists in database, and if then make sure that only once
   * @return bool true - ok / false - sth wrong
   */
  static protected function sessionCheck()
  {
    if (self::$dbSession === NULL)
      return true;

    $sql = sprintf("SELECT `%s`, `%s`, `%s`, `%s` FROM `%s%s` WHERE `%2\$s`='%s'", self::$db->safe(self::$dbSession['accountId']), self::$db->safe(self::$dbSession['sid']), self::$db->safe(self::$dbSession['startTime']), self::$db->safe(self::$dbSession['ip']), self::$db->getPrefix(), self::$db->safe(self::$dbSession['table']), self::$db->safe(Chors_Session::getSID()));

    $res = self::$db->query($sql);

    $amount = self::$db->numRows($res);

    if ($amount > 1)
    {
      self::sessionClear(Chors_Session::getSID());
      return false;
    }
    elseif ($amount === 1)
    {
      $row = self::$db->fetchArray($res);
      if (Chors_Session::getValue("userId") !== $row[self::$dbSession['accountId']])
        return false;
      return true;
    }
    else
      return false;
  }

  static protected function renewCheck()
  {
    if (!self::$sessionRenewable)
      return false;

    if (self::$userLastRenew + self::$sessionRenewTime < Chors_Time::nowTimestamp())
      return false;

    self::$userLastRenew = Chors_Time::nowTimestamp();
    self::setSessionFromUser();
    return true;
  }

  /**
   * 
   * @return int or NULL if id not found
   */
  static private function getSessionId()
  {
    $sql = sprintf("SELECT `%s` FROM `%s%s` WHERE `%s` = '%s'",
      self::$db->safe(self::$dbSession['id']),
      self::$db->getPrefix(),
      self::$db->safe(self::$dbSession['table']),
      self::$db->safe(self::$dbSession['sid']),
      self::$db->safe(Chors_Session::getSID()));
      
    $res = self::$db->query($sql);

    $amount = self::$db->numRows($res);
        
    if($amount == 0)
      return NULL;
    
    $row = self::$db->fetchArray($res);
    return $row[self::$dbSession['id']];
  }
  
  /**
   * 
   * @param string $sessionSID Session SID not Session entry Id.
   * If not supplied current session SID will be used.
   * @return void
   */
  static private function sessionClear($sessionSID = NULL)
  {
    if($sessionSID === NULL)
      $sessionSID = Chors_Session::getSID();
    if($sessionSID === NULL)
      return;
    
    $sql = sprintf("DELETE FROM `%s%s` WHERE `%s`='%s'",
      self::$db->getPrefix(),
      self::$db->safe(self::$dbSession['table']),
      self::$db->safe(self::$dbSession['sid']),
      self::$db->safe($sessionSID));
    self::$db->query($sql);
  }
  
  /**
   * 
   * @param type $userId
   */
  static private function sessionClearAll($userId = NULL)
  {
    if($userId === NULL)
      $userId = self::$userId;
    
    $sql = sprintf("DELETE FROM `%s%s` WHERE `%s`='%s'",
      self::$db->getPrefix(),
      self::$db->safe(self::$dbSession['table']),
      self::$db->safe(self::$dbSession['accountId']),
      self::$db->safe($userId));
    self::$db->query($sql);
  }

  static public function logout()
  {
    if (self::$dbSession !== null)
    {
      if(self::$isMultipleSessionsSupportActive)
        self::sessionClear();
      else
        self::sessionClearAll();
    }

    self::autologinClear();

    self::$instance->cmd_logout();
    
    self::getRunaAccount();
    if(self::$runaAccount !== NULL)
      self::$runaAccount->clearAllData();
    
    self::getFacebook();
    if(self::$facebook !== NULL)
    {
      self::$facebook->logoutUser();
      self::$facebook->destroySession();
      self::$facebook = NULL;
    }
    
    Chors_Session::clear();
    Chors_Session::destroy();

    self::setSessionAnnon();
  }
  
  static public function isUserLogged()
  {
    return self::$userLogged;
  }

// <editor-fold defaultstate="collapsed" desc="Facebook Auth Support">
  static public function activateFacebookSupport()
  {
    self::$isFacebookAuthActive = true;
  }

  static public function setFacebookData($appId, $appSecret)
  {
    self::$facebookAppId = $appId;
    self::$facebookAppSecret = $appSecret;
  }

  static public function getFacebookAppId()
  {
    return self::$facebookAppId;
  }

  static public function getFacebookAppSecret()
  {
    return self::$facebookAppSecret;
  }

  static public function getFacebook()
  {
    if (self::$facebook === null && self::$isFacebookAuthActive)
    {
      require_once 'Lib/Facebook/facebook.php';
      self::$facebook = new Facebook(array(
          'appId' => self::$facebookAppId,
          'secret' => self::$facebookAppSecret,
      ));
    }
    return self::$facebook;
  }

  /**
   * Retrieves currently logged facebook userId or NULL if not logged in.
   * @return int
   */
  static public function getFacebookUserId()
  {
    self::getFacebook();

    if (self::$facebook === NULL)
      return NULL;

    $facebookId = self::$facebook->getUser();

    if ($facebookId === 0)
      return NULL;

    return $facebookId;
  }

  /**
   * Checks if user is currently logged in via facebook auth or not.
   * @return boolean
   */
  static public function isFacebookUserLogged()
  {
    self::getFacebook();

    if (self::$facebook === NULL)
      return FALSE;

    $facebookId = self::$facebook->getUser();

    if ($facebookId === 0)
      return FALSE;

    return TRUE;
  }
  
  /**
   * Performs facebook related auth.
   * @return boolean was user authed via facebook or not.
   */
  static protected function facebook()
  {
    if (!self::$isFacebookAuthActive || self::$facebookAppId === null || self::$facebookAppSecret === null || self::$dbAuthFacebook === null)
      return false;

    self::getFacebook();

    $facebookId = self::$facebook->getUser();

    if ($facebookId === 0)
      return false;

    //@todo: do sth to prevent querying db at each visit :x
    $sql = sprintf("SELECT `%s` FROM `%s%s` WHERE `%s`='%s'", self::$db->safe(self::$dbAuthFacebook['accountId']), self::$db->getPrefix(), self::$db->safe(self::$dbAuthFacebook['table']), self::$db->safe(self::$dbAuthFacebook['facebookId']), self::$db->safe($facebookId));

    $res = self::$db->query($sql);
    $row = self::$db->fetchArray($res);
    $amount = self::$db->numRows($res);
    if ($amount > 0) // is already in database
    {
      if (self::$userLogged)
      { // user is logged
        if (self::$userId == $row[self::$dbAuthFacebook['accountId']])
        { // proper user already logged in
;
        }
        else
        { // logged user is not fb user -> log as fb user
          self::$db->transactionStart();

          self::setCurrentUserByAccountId(
                  $row[self::$dbAuthFacebook['accountId']], false);

          self::$db->transactionCommit();
        }
      }
      else
      { // user not yet logged
        self::$db->transactionStart();

        self::setCurrentUserByAccountId(
                $row[self::$dbAuthFacebook['accountId']], false);

        self::$db->transactionCommit();
      }
    }
    else // not connected to account yet
    {
      try
      {
        $facebookProfile = self::$facebook->api('/me');
      }
      catch (FacebookApiException $e)
      {
        return false;
      }
      
      // if profile was not returned we need to discard that login attempt
      if($facebookProfile === NULL)
      {
        return false;
      }
      
      // if profile does not contain valid id we also discard that login attempt
      if(isset($facebookProfile['id']) === false || $facebookProfile['id'] === 0)
      {
        return false;
      }
      
      if (self::$userLogged)
      { // user logged -> bind fb account
        $sql = sprintf("INSERT INTO `%s%s` (`%s`, `%s`) VALUES ('%s', '%s')", self::$db->getPrefix(), self::$db->safe(self::$dbAuthFacebook['table']), self::$db->safe(self::$dbAuthFacebook['accountId']), self::$db->safe(self::$dbAuthFacebook['facebookId']), self::$db->safe(self::$userId), self::$db->safe($facebookProfile['id']));

        $res = self::$db->query($sql);

        self::$instance->cmd_facebookAdd($facebookProfile);
      }
      else
      { // user not yet logged -> register and bind fb account
        self::$db->transactionStart();

        $displayName = $facebookProfile['name'];

        if(self::$isUniqueUserNameRequired === TRUE)
        {
          for ($i = 1; self::isNameUsed($displayName); $i++)
            $displayName = $facebookProfile['name'] . $i;
        }

        if (self::$localizationActive)
        {
          $id = self::createUser(self::$newUserLevel, $displayName, self::$annon['localeId']);
        }
        else
          $id = self::createUser(self::$newUserLevel, $displayName);

        $sql = sprintf("INSERT INTO `%s%s` (`%s`, `%s`) VALUES ('%s', '%s')", self::$db->getPrefix(), self::$db->safe(self::$dbAuthFacebook['table']), self::$db->safe(self::$dbAuthFacebook['accountId']), self::$db->safe(self::$dbAuthFacebook['facebookId']), self::$db->safe($id), self::$db->safe($facebookProfile['id']));

        $res = self::$db->query($sql);

        self::setCurrentUserByAccountId(
                $id, false);

        self::$db->transactionCommit();
        
        self::$instance->cmd_facebookRegister($facebookProfile, $id);
      }
    }
    return true;
  }
  
  static public function isFacebookUserRegistered($facebookId)
  {
    if (self::$dbAuthFacebook === null)
      return false;

    $sql = sprintf("SELECT `%s` FROM `%s%s` WHERE `%1\$s`='%s'", self::$db->safe(self::$dbAuthFacebook['facebookId']), self::$db->getPrefix(), self::$db->safe(self::$dbAuthFacebook['table']), self::$db->safe($facebookId));

    $res = self::$db->query($sql);
    $row = self::$db->fetchArray($res);
    $num = self::$db->numRows($res);
    if ($num > 0)
      return true;
    else
      return false;
  }
// </editor-fold>
  
// <editor-fold defaultstate="collapsed" desc="RunaAccount Auth Support">
  static public function activateRunaAccountSupport()
  {
    self::$isRunaAccountAuthActive = true;
  }
  
  static public function setRunaAccountData($appKey, $appSecret)
  {
    self::$runaAccountAppKey = $appKey;
    self::$runaAccountAppSecret = $appSecret;
  }
  
  static public function getRunaAccountAppKey()
  {
    return self::$runaAccountAppKey;
  }

  static public function getRunaAccountAppSecret()
  {
    return self::$runaAccountAppSecret;
  }
  
  static public function getRunaAccount()
  {
    if (self::$runaAccount === null && self::$isRunaAccountAuthActive)
    {
      require_once 'Lib/RunaAccount/RunaAccount.php';
      self::$runaAccount = new RunaAccount(array(
          'applicationKey' => self::$runaAccountAppKey,
          'applicationSecret' => self::$runaAccountAppSecret,
      ));
    }
    return self::$runaAccount;
  }
  
  /**
   * Retrieves currently logged Runa account identity or NULL if not logged in.
   * @return RunaAccountIdentity
   */
  static public function getRunaAccountIdentity()
  {
    self::getRunaAccount();

    if (self::$runaAccount === NULL)
      return NULL;

    return self::$runaAccount->getCurrentIdentity();
  }
  
  /**
   * Checks if user is currently logged in via runa account auth or not.
   * @return boolean
   */
  static public function isRunaAccountUserLogged()
  {
    self::getRunaAccount();

    if (self::$runaAccount === NULL)
      return FALSE;

    return TRUE;
  }
  
  /**
   * Performs Runa Account related auth.
   * @return boolean was user authed via Runa Account or not.
   */
  static protected function runaAccount()
  {
    if (!self::$isRunaAccountAuthActive || self::$runaAccountAppKey === null || self::$runaAccountAppSecret === null || self::$dbAuthRunaAccount === null)
      return FALSE;

    $runaAccount = self::getRunaAccount();
    if($runaAccount === NULL)
      return FALSE;
    
    $identity = $runaAccount->getCurrentIdentity();
    if($identity === NULL)
      return FALSE;
    
    $uuid = new Chors_UUID($identity->GUID);
    $binaryUuid = pack("H*", $uuid->getString(false));
    
    //@todo: do sth to prevent querying db at each visit :x
    $sql = sprintf("SELECT `%s` FROM `%s%s` WHERE `%s`='%s'", self::$db->safe(self::$dbAuthRunaAccount['accountId']), self::$db->getPrefix(), self::$db->safe(self::$dbAuthRunaAccount['table']), self::$db->safe(self::$dbAuthRunaAccount['identityGUID']), self::$db->safe($binaryUuid));

    $res = self::$db->query($sql);
    $row = self::$db->fetchArray($res);
    $amount = self::$db->numRows($res);
    if ($amount > 0) // is already in database
    {
      if (self::$userLogged)
      { // user is logged
        if (self::$userId == $row[self::$dbAuthRunaAccount['accountId']])
        { // proper user already logged in
;
        }
        else
        { // logged user is not runa account user -> log as runa account user
          self::$db->transactionStart();

          self::setCurrentUserByAccountId(
                  $row[self::$dbAuthRunaAccount['accountId']], false);

          self::$db->transactionCommit();
        }
      }
      else
      { // user not yet logged
        self::$db->transactionStart();

        self::setCurrentUserByAccountId(
                $row[self::$dbAuthRunaAccount['accountId']], false);

        self::$db->transactionCommit();
      }
    }
    else // not connected to account yet
    {
      if (self::$userLogged)
      { // user logged -> bind fb account
        $sql = sprintf("INSERT INTO `%s%s` (`%s`, `%s`) VALUES ('%s', '%s')",
          self::$db->getPrefix(),
          self::$db->safe(self::$dbAuthRunaAccount['table']),
          self::$db->safe(self::$dbAuthRunaAccount['accountId']),
          self::$db->safe(self::$dbAuthRunaAccount['identityGUID']),
          self::$db->safe(self::$userId),
          self::$db->safe($binaryUuid));

        $res = self::$db->query($sql);

        self::$instance->cmd_runaAccountAdd($identity);
      }
      else
      { // user not yet logged -> register and bind fb account
        self::$db->transactionStart();

        $displayName = $identity->displayName;

        if(self::$isUniqueUserNameRequired === TRUE)
        {
          for ($i = 1; self::isNameUsed($displayName); $i++)
            $displayName = $identity->displayName . $i;
        }

        if (self::$localizationActive)
          $id = self::createUser(self::$newUserLevel, $displayName, self::$annon['localeId']);
        else
          $id = self::createUser(self::$newUserLevel, $displayName);

        $sql = sprintf("INSERT INTO `%s%s` (`%s`, `%s`) VALUES ('%s', '%s')",
          self::$db->getPrefix(),
          self::$db->safe(self::$dbAuthRunaAccount['table']),
          self::$db->safe(self::$dbAuthRunaAccount['accountId']),
          self::$db->safe(self::$dbAuthRunaAccount['identityGUID']),
          self::$db->safe($id),
          self::$db->safe($binaryUuid));

        $res = self::$db->query($sql);

        self::setCurrentUserByAccountId(
                $id, false);

        self::$db->transactionCommit();
        
        self::$instance->cmd_runaAccountRegister($identity, $id);
      }
    }
    return true;
  }
  
  /**
   * Checks if specified identity UUID is already registered to any account.
   * @param Chors_UUID $identityUUID
   * @return boolean
   */
  static public function isRunaAccountUserRegistered($identityUUID)
  {
    if (self::$dbAuthRunaAccount === null)
      return false;
    
    $binaryUuid = pack("H*", $identityUUID->getString(false));

    $sql = sprintf("SELECT `%s` FROM `%s%s` WHERE `%1\$s`='%s'",
      self::$db->safe(self::$dbAuthRunaAccount['identityGUID']),
      self::$db->getPrefix(),
      self::$db->safe(self::$dbAuthRunaAccount['table']),
      self::$db->safe($binaryUuid));

    $res = self::$db->query($sql);
    $row = self::$db->fetchArray($res);
    $num = self::$db->numRows($res);
    if ($num > 0)
      return true;
    else
      return false;
  }
// </editor-fold>
  
  static public function autologin()
  {
    if (self::$dbAutologin === null)
      return false;
    if (!isset($_COOKIE[self::$dbAutologin['cookie']]))
      return false;
    $loginStr = $_COOKIE[self::$dbAutologin['cookie']];

    $sql = sprintf("SELECT `%s`, `%s` FROM `%s%s` WHERE `%2\$s`='%s'", self::$db->safe(self::$dbAutologin['logId']), self::$db->safe(self::$dbAutologin['autologin']), self::$db->getPrefix(), self::$db->safe(self::$dbAutologin['table']), self::$db->safe($loginStr));

    $res = self::$db->query($sql);

    $amount = self::$db->numRows($res);
    
    if($amount === 0)
      return false;
    
    // as this login string is either going to be used or is invalid we can remove it
    $sql = sprintf("DELETE FROM `%s%s` WHERE `%s`='%s'", self::$db->getPrefix(), self::$db->safe(self::$dbAutologin['table']), self::$db->safe(self::$dbAutologin['autologin']), self::$db->safe($loginStr));
      self::$db->query($sql);
    
    if ($amount > 1)
      return false;
    
    $row = self::$db->fetchArray($res);

    self::$db->transactionStart();

    self::setCurrentUserByAccountId($row[self::$dbAutologin['logId']], true);

    self::$db->transactionCommit();

    return true;
  }

  protected function createAutologinString($iteration)
  {
    $crypt = self::$userId . ":" . self::$userName . ":" . $iteration . ":" . Chors_Session::getSID();
    return hash(self::$autologinHashFunction, $crypt, false);
  }

  static protected function autologinGenerate()
  {
    if (self::$dbAutologin === null)
      return false;

    $check = 1;
    for ($i = Chors_Time::nowTimestamp(); $check != 0; $i++)
    {
      $crypt = self::getInstance()->createAutologinString($i);
      $sql = sprintf("SELECT * FROM `%s%s` WHERE `%s`='%s'", self::$db->getPrefix(), self::$db->safe(self::$dbAutologin['table']), self::$db->safe(self::$dbAutologin['autologin']), self::$db->safe($crypt));

      $res = self::$db->query($sql);
      $check = self::$db->numRows($res);
    }

    $nowTime = Chors_Time::nowTimestamp();
    
    $sql = sprintf("INSERT INTO `%s%s` (`%s`, `%s`, `%s`) VALUES ('%s', '%s', '%s')", self::$db->getPrefix(), self::$db->safe(self::$dbAutologin['table']), self::$db->safe(self::$dbAutologin['logId']), self::$db->safe(self::$dbAutologin['autologin']), self::$db->safe(self::$dbAutologin['time']), self::$db->safe(self::$userId), self::$db->safe($crypt), self::$db->safe(Chors_Time::timestampToString($nowTime)));

    $res = self::$db->query($sql);

    setcookie(self::$dbAutologin['cookie'], $crypt, $nowTime + self::$autologinLife, Chors_Session::GetCookiePath(), null, false, true);
    return true;
  }

  static protected function autologinClearAll()
  {
    if (self::$dbAutologin === null)
      return;

    $sql = sprintf("DELETE FROM `%s%s` WHERE `%s`='%s'", self::$db->getPrefix(), self::$db->safe(self::$dbAutologin['table']), self::$db->safe(self::$dbAutologin['logId']), self::$db->safe(self::$userId));
    self::$db->query($sql);
    
    setcookie(self::$dbAutologin['cookie'], '', 1, Chors_Session::GetCookiePath(), null, false, true);
  }
  
  static protected function autologinClear()
  {
    if (self::$dbAutologin === null)
      return;
    
    $sql = sprintf("DELETE FROM `%s%s` WHERE `%s`<'%s'", self::$db->getPrefix(), self::$db->safe(self::$dbAutologin['table']), self::$db->safe(self::$dbAutologin['time']), self::$db->safe(Chors_Time::timestampToString(Chors_Time::nowTimestamp()-self::$autologinLife)));
    self::$db->query($sql);
    
    setcookie(self::$dbAutologin['cookie'], '', 1, Chors_Session::GetCookiePath(), null, false, true);
  }

  /**
   *
   * @param string $login
   * @param string $newPassword
   * @param bool $encryptPassword if <i>true</i> than method will perform passwordEncrypt call
   * @return bool true when change in effect, false when error occured
   */
  static public function changePassword($login, $newPassword, $encryptPassword = true)
  {
    if ($encryptPassword)
      $newPassword = self::$instance->passwordEncrypt($newPassword, $login);

    $sql = sprintf("UPDATE `%s%s` SET `%s` = '%s', `%s` = '%s'  WHERE `%s` = '%s'", self::$db->getPrefix(), self::$db->safe(self::$dbAuthPassword['table']), self::$db->safe(self::$dbAuthPassword['password']), self::$db->safe($newPassword), self::$db->safe(self::$dbAuthPassword['timestamp']), self::$db->safe(Chors_Time::timestampToString()), self::$db->safe(self::$dbAuthPassword['login']), self::$db->safe($login)
    );
    self::$db->query($sql);

    if (self::$db->affectedRows() == 0)
      return false;
    else
      return true;
  }

  static public function removeAuthPassword($passwordId)
  {
    $sql = sprintf("DELETE FROM `%s%s` WHERE `%s`=%d", self::$db->getPrefix(), self::$db->safe(self::$dbAuthPassword['table']), self::$db->safe(self::$dbAuthPassword['id']), $passwordId);
    self::$db->query($sql);

    return self::$db->affectedRows() == 0 ? false : true;
  }

  static public function removeAllAuthPasswordOfUser($userId)
  {
    $sql = sprintf("DELETE FROM `%s%s` WHERE `%s`=%d", self::$db->getPrefix(), self::$db->safe(self::$dbAuthPassword['table']), self::$db->safe(self::$dbAuthPassword['accountId']), $userId);
    self::$db->query($sql);

    return self::$db->affectedRows() == 0 ? false : true;
  }

  static public function addAuthPassword($login, $password, $userId, $encryptPassword = true)
  {
    if ($encryptPassword)
      $password = self::$instance->passwordEncrypt($password, $login);

    $sql = sprintf("INSERT INTO `%s%s` (`%s`, `%s`, `%s`, `%s`) VALUES (%d, '%s', '%s', '%s')", self::$db->getPrefix(), self::$db->safe(self::$dbAuthPassword['table']), self::$db->safe(self::$dbAuthPassword['accountId']), self::$db->safe(self::$dbAuthPassword['login']), self::$db->safe(self::$dbAuthPassword['password']), self::$db->safe(self::$dbAuthPassword['timestamp']), $userId, self::$db->safe($login), self::$db->safe($password), self::$db->safe(Chors_Time::timestampToString())
    );

    self::$db->query($sql);

    return self::$db->affectedRows() == 0 ? false : true;
  }

  static public function changeLoginAndPassword($passwordId, $newLogin, $newPassword, $encryptPassword = true)
  {
    if ($encryptPassword)
      $newPassword = self::$instance->passwordEncrypt($newPassword, $newLogin);

    $sql = sprintf("UPDATE `%s%s` SET `%s` = '%s', `%s` = '%s', `%s` = '%s'  WHERE `%s` = %d", self::$db->getPrefix(), self::$db->safe(self::$dbAuthPassword['table']), self::$db->safe(self::$dbAuthPassword['password']), self::$db->safe($newPassword), self::$db->safe(self::$dbAuthPassword['login']), self::$db->safe($newLogin), self::$db->safe(self::$dbAuthPassword['timestamp']), self::$db->safe(Chors_Time::timestampToString()), self::$db->safe(self::$dbAuthPassword['id']), $passwordId
    );
    self::$db->query($sql);

    if (self::$db->affectedRows() == 0)
      return false;
    else
      return true;
  }

  static public function isLoginUsed($login)
  {
    $sql = sprintf("SELECT `%s` FROM `%s%s` WHERE `%1\$s`='%s'", self::$db->safe(self::$dbAuthPassword['login']), self::$db->getPrefix(), self::$db->safe(self::$dbAuthPassword['table']), self::$db->safe($login));

    $res = self::$db->query($sql);
    $row = self::$db->fetchArray($res);
    $num = self::$db->numRows($res);
    if ($num > 0)
      return true;
    else
      return false;
  }

  static public function isNameUsed($name)
  {
    $sql = sprintf("SELECT `%s` FROM `%s%s` WHERE `%1\$s`='%s'", self::$db->safe(self::$dbAccount['name']), self::$db->getPrefix(), self::$db->safe(self::$dbAccount['table']), self::$db->safe($name));

    $res = self::$db->query($sql);
    $row = self::$db->fetchArray($res);
    $num = self::$db->numRows($res);
    if ($num > 0)
      return true;
    else
      return false;
  }

  /**
   *
   * @param string $userId id of user which data should be changed
   * @param string $level new user level
   * @param string $name new user name
   * @throws Chors_Exception AUTH_LOGIN_ALREADY_EXIST | AUTH_NAME_ALREADY_EXISTS - name already in use by other user
   */
  static public function changeUserData($userId, $name, $level, $localizationId = null)
  {
    self::$db->transactionStart();

    $sql = sprintf("SELECT `%s` FROM `%s%s` WHERE `%1\$s`='%s' AND `%s` != %d", self::$db->safe(self::$dbAccount['name']), self::$db->getPrefix(), self::$db->safe(self::$dbAccount['table']), self::$db->safe($name), self::$db->safe(self::$dbAccount['id']), $userId);

    $res = self::$db->query($sql);
    $row = self::$db->fetchArray($res);
    $num = self::$db->numRows($res);
    if ($num > 0)
    {
      self::$db->transactionCommit();
      throw new Chors_Exception("Change data error - specified user name already exist", Chors_ErrorCodes::AUTH_NAME_ALREADY_EXISTS);
    }

    if (self::$localizationActive)
    {
      $sql = sprintf("UPDATE `%s%s` SET `%s`=%d, `%s`='%s', `%s`=%d WHERE `%s` = %d", self::$db->getPrefix(), self::$db->safe(self::$dbAccount['table']), self::$db->safe(self::$dbAccount['level']), $level, self::$db->safe(self::$dbAccount['name']), self::$db->safe($name), self::$db->safe(self::$dbAccount['localeId']), $localizationId, self::$db->safe(self::$dbAccount['id']), $userId);
    }
    else
    {
      $sql = sprintf("UPDATE `%s%s` SET `%s`=%d, `%s`='%s' WHERE `%s` = %d", self::$db->getPrefix(), self::$db->safe(self::$dbAccount['table']), self::$db->safe(self::$dbAccount['level']), $level, self::$db->safe(self::$dbAccount['name']), self::$db->safe($name), self::$db->safe(self::$dbAccount['id']), $userId);
    }
    $res = self::$db->query($sql);

    self::$db->transactionCommit();
  }

  static public function getLocalizationIdFromLocalizationName($localizationName)
  {
    if (self::$localizationActive == false)
      return;

    $sql = sprintf("SELECT `%s` FROM `%s%s` WHERE `%s` = '%s'", self::$dbLocalization['id'], self::$db->getPrefix(), self::$dbLocalization['table'], self::$dbLocalization['name'], self::$db->safe($localizationName));
    $res = self::$db->query($sql);
    $row = self::$db->fetchArray($res);

    return $row == false ? null : $row[self::$dbLocalization['id']];
  }

  static public function getLocalizationNameFromLocalizationId($localizationId)
  {
    if (self::$localizationActive == false)
      return;

    $sql = sprintf("SELECT `%s` FROM `%s%s` WHERE `%s` = '%s'", self::$dbLocalization['name'], self::$db->getPrefix(), self::$dbLocalization['table'], self::$dbLocalization['id'], self::$db->safe($localizationId));
    $res = self::$db->query($sql);
    $row = self::$db->fetchArray($res);

    return $row == false ? null : $row[self::$dbLocalization['name']];
  }

  static public function changeUserLocalization($userId, $localizationId)
  {
    if (self::$localizationActive)
    {
      $sql = sprintf("UPDATE `%s%s` SET `%s`=%d WHERE `%s` = %d", self::$db->getPrefix(), self::$db->safe(self::$dbAccount['table']), self::$db->safe(self::$dbAccount['localeId']), $localizationId, self::$db->safe(self::$dbAccount['id']), $userId);
      $res = self::$db->query($sql);
    }
  }

  static public function changeCurrentUserLocalization($localizationId)
  {
    if (self::$userLogged)
      self::changeUserLocalization(self::getUserId(), $localizationId);

    Chors_Session::setValue("localeName", Chors_Localization::getCurrentLanguageName());
    Chors_Session::setValue("localeId", Chors_Localization::getCurrentLanguageId());
  }

  /**
   * Handles raw user insertion data, <u>do not check if user display name is unique or not</u>
   * @param int $level
   * @param string $name
   * @param int $localizationId 
   */
  static private function createUser($level, $name, $localizationId = null)
  {
    $dateTime = Chors_Time::timestampToString();

    if (self::$localizationActive)
    {
      $sql = sprintf("INSERT INTO `%s%s` (`%s`, `%s`, `%s`, `%s`) VALUES ('%s', %d, %d, '%s')", self::$db->getPrefix(), self::$db->safe(self::$dbAccount['table']), self::$db->safe(self::$dbAccount['name']), self::$db->safe(self::$dbAccount['level']), self::$db->safe(self::$dbAccount['localeId']), self::$db->safe(self::$dbAccount['timestamp']), self::$db->safe($name), $level, $localizationId, self::$db->safe($dateTime)
      );
    }
    else
    {
      $sql = sprintf("INSERT INTO `%s%s` (`%s`, `%s`, `%s`) VALUES ('%s', %d, '%s')", self::$db->getPrefix(), self::$db->safe(self::$dbAccount['table']), self::$db->safe(self::$dbAccount['name']), self::$db->safe(self::$dbAccount['level']), self::$db->safe(self::$dbAccount['timestamp']), self::$db->safe($name), $level, self::$db->safe($dateTime)
      );
    }

    $res = self::$db->query($sql);

    $sql = sprintf("SELECT `%s` FROM `%s%s` WHERE `%s`='%s' AND `%s`='%s' LIMIT 1", self::$db->safe(self::$dbAccount['id']), self::$db->getPrefix(), self::$db->safe(self::$dbAccount['table']), self::$db->safe(self::$dbAccount['name']), self::$db->safe($name), self::$db->safe(self::$dbAccount['timestamp']), self::$db->safe($dateTime));
    $res = self::$db->query($sql);
    $row = self::$db->fetchArray($res);

    return $row[self::$dbAccount['id']];
  }

  static public function registerNotUnique($login, $password, $level, $name, $localizationId = null)
  {
    self::$db->transactionStart();

    if (self::isLoginUsed($login))
      throw new Chors_Exception("Registration error - specified login already exist", Chors_ErrorCodes::AUTH_LOGIN_ALREADY_EXISTS);


    $id = self::createUser($level, $name, $localizationId);

    self::addAuthPassword($login, $password, $id, true);

    self::$db->transactionCommit();

    return $id;
  }

  static public function registerUnique($login, $password, $level, $name, $localizationId = null)
  {
    self::$db->transactionStart();

    $flagLogin = self::isLoginUsed($login);
    $flagName = self::isNameUsed($name);

    switch (true)
    {
      case $flagLogin && $flagName:
        throw new Chors_Exception("Registration error - specified login and name already exist", Chors_ErrorCodes::AUTH_LOGIN_AND_NAME_ALREADY_EXISTS);
        break;
      case $flagLogin:
        throw new Chors_Exception("Registration error - specified login already exist", Chors_ErrorCodes::AUTH_LOGIN_ALREADY_EXISTS);
        break;
      case $flagName:
        throw new Chors_Exception("Registration error - specified name already exist", Chors_ErrorCodes::AUTH_NAME_ALREADY_EXISTS);
        break;
    }

    $id = self::createUser($level, $name, $localizationId);

    self::addAuthPassword($login, $password, $id, true);

    self::$db->transactionCommit();

    return $id;
  }

  /**
   * Perform registration process
   * @param string $login desired login name
   * @param string $password desired password
   * @param int $level level for system access
   * @param string $name display name
   * @deprecated for old <b>register</b> behaviour use <b>registerUnique</b>
   * @return int id number of new user
   */
  static public function register($login, $password, $level, $name, $localizationId = null)
  {
    if(self::$isUniqueUserNameRequired)
      return self::registerUnique($login, $password, $level, $name, $localizationId);
    else
      return self::registerNotUnique ($login, $password, $level, $name, $localizationId);
  }

  static public function login($login, $password, $autologin = false)
  {
    self::$db->transactionStart();

    $sql = sprintf("SELECT `%s`, `%s`, `%s`, `%s` FROM `%s%s` WHERE `%s`='%s' LIMIT 1", self::$db->safe(self::$dbAuthPassword['id']), self::$db->safe(self::$dbAuthPassword['accountId']), self::$db->safe(self::$dbAuthPassword['password']), self::$db->safe(self::$dbAuthPassword['timestamp']), self::$db->getPrefix(), self::$db->safe(self::$dbAuthPassword['table']), self::$db->safe(self::$dbAuthPassword['login']), self::$db->safe($login));

    $res = self::$db->query($sql);
    $check = self::$db->numRows($res);
    if ($check === 1)
    {
      $row = self::$db->fetchArray($res);
      $password = self::$instance->passwordEncrypt($password, $login);
      if ($password == $row[self::$dbAuthPassword['password']])
      {
        self::setCurrentUserByAccountId(
                $row[self::$dbAuthPassword['accountId']], $autologin);

        self::$db->transactionCommit();
      }
      else
      {
        self::$db->transactionCommit();
        throw new Chors_Exception("Authentication error - wrong password entered", Chors_ErrorCodes::AUTH_WRONG_PASSWORD);
      }
    }
    else
    {
      self::$db->transactionCommit();
      throw new Chors_Exception("Authentication error - specified user not found", Chors_ErrorCodes::AUTH_WRONG_USERNAME);
    }
  }

  /**
   * @param int $userId System account id
   * @return bool <b>True</b> user found and logged in, <b>False</b> User not found and not logged in
   */
  static public function setCurrentUserById($userId)
  {
    //todo
    self::$db->transactionStart();
    // check if user with that id exist
    if (true)
    {
      self::setCurrentUserByAccountId($userId);
      self::$db->transactionCommit();
      return true;
    }
    else
    {
      self::$db->transactionCommit();
      return false;
    }
  }

  static private function setCurrentUserByAccountId($userId, $generateAutologin = false)
  {
    if (self::$userLogged)
      self::logout();

    self::$userId = $userId;

    if (self::$localizationActive)
    {
      $sql = sprintf("SELECT `%1\$s`.`%3\$s` AS `name`, `%1\$s`.`%4\$s` AS `level`, `%1\$s`.`%5\$s` AS `nsid`, `%2\$s`.`%6\$s` AS `ns` " .
              "FROM `%1\$s` JOIN `%2\$s` ON `%1\$s`.`%5\$s` = `%2\$s`.`%7\$s` " .
              "WHERE `%1\$s`.`%8\$s` = %9\$d LIMIT 1", self::$db->getPrefix() . self::$db->safe(self::$dbAccount['table']), // 1
              self::$db->getPrefix() . self::$db->safe(self::$dbLocalization['table']), // 2
              self::$db->safe(self::$dbAccount['name']), // 3
              self::$db->safe(self::$dbAccount['level']), // 4
              self::$db->safe(self::$dbAccount['localeId']), // 5
              self::$db->safe(self::$dbLocalization['name']), // 6
              self::$db->safe(self::$dbLocalization['id']), // 7    
              self::$db->safe(self::$dbAccount['id']), // 8
              self::$userId            // 9
      );

      $res = self::$db->query($sql);
      $row = self::$db->fetchArray($res);

      Chors_Localization::setLanguage($row["ns"], $row["nsid"]);
    }
    else
    {
      $sql = sprintf("SELECT `%s` AS `name`, `%s` AS `level` FROM `%s%s` WHERE `%s`=%d LIMIT 1", self::$db->safe(self::$dbAccount['name']), self::$db->safe(self::$dbAccount['level']), self::$db->getPrefix(), self::$db->safe(self::$dbAccount['table']), self::$db->safe(self::$dbAccount['id']), self::$userId);

      $res = self::$db->query($sql);
      $row = self::$db->fetchArray($res);
    }
    
    self::$userName = $row["name"];
    self::$userLevel = $row["level"];
    self::$userLogged = true;
    self::$userStart = Chors_Time::nowTimestamp();
    self::$userLastRenew = self::$userStart;

    if (self::$dbAutologin !== null)
    {
      if(self::$autologSingleMachine)
        self::autologinClearAll();
      
      if ($generateAutologin === true)
        self::autologinGenerate();
    }

    Chors_Session::regenerate(true);
    self::setSessionFromUser();
    self::session();
    self::log();
    self::$instance->cmd_login();
  }

  static private function log()
  {
    if (self::$dbLog === null)
      return;

    $sql = sprintf("INSERT INTO `%s%s` (`%s`, `%s`, `%s`, `%s`) VALUES ('%s', '%s', '%s', '%s')", self::$db->getPrefix(), self::$db->safe(self::$dbLog['table']), self::$db->safe(self::$dbLog['id']), self::$db->safe(self::$dbLog['time']), self::$db->safe(self::$dbLog['ip']), self::$db->safe(self::$dbLog['userAgent']), self::$db->safe(self::$userId), self::$db->safe(Chors_Time::timestampToString(self::$userStart)), self::$db->safe($_SERVER['REMOTE_ADDR']), self::$db->safe($_SERVER['HTTP_USER_AGENT']));

    $res = self::$db->query($sql);
  }

  /**
   * Overwrite this function and put inside all actions that should be performed
   * when user is visiting for a first time.
   */
  protected function cmd_firstVisit()
  {
    ;
  }

  /**
   * Overwrite this function and put inside all actions that should be performed
   * upon successful login.
   */
  protected function cmd_login()
  {
    ;
  }

  /**
   * Overwrite this function and put inside all actions that should be performed
   * upon successful logout.
   */
  protected function cmd_logout()
  {
    ;
  }

  /**
   * Overwrite this function and put inside all actions that should be performed
   * upon registering via Facebook (connected via Facebook but do not have account).
   */
  protected function cmd_facebookRegister($facebookProfile, $accountId)
  {
    ;
  }

  /**
   * Overwrite this function and put inside all actions that should be performed
   * upon connecting user with facebook account.
   */
  protected function cmd_facebookAdd($facebookProfile)
  {
    ;
  }
  
  /**
   * Overwrite this function and put inside all actions that should be performed
   * upon registering via RunaAccount (connected via RunaAccount but do not have account).
   * @param RunaAccountIdentity $identity
   */
  protected function cmd_runaAccountRegister($identity, $accountId)
  {
    ;
  }

  /**
   * Overwrite this function and put inside all actions that should be performed
   * upon connecting user with Runa account.
   * @param RunaAccountIdentity $identity
   */
  protected function cmd_runaAccountAdd($identity)
  {
    ;
  }

  public function passwordEncrypt($password, $userUnique = "")
  {
    $userUnique = md5($userUnique);
    $cryptedPassword = sha1($userUnique . $password);
    return $cryptedPassword;
  }

  static public function getUserLevel()
  {
    return self::$userLevel;
  }

  static public function getUserId()
  {
    return self::$userId;
  }

  static public function getUserName()
  {
    return self::$userName;
  }

  static public function getInstance()
  {
    return self::$instance;
  }

}
