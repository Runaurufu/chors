<?php

/**
 * Information class for Chors.
 * @package Chors
 * @version 1.0
 * @since 1.0
 * @author Runaurufu
 */
class Chors_Info
{

  /**
   * Perform all necesary operations regarding defining referrer information such
   * as it's URL address or search phrase entered in search engines.
   * @return stdClass information about referring page at format:<br/>
   * == <b>null</b> if no referrer information received<br/>
   * ->domain = referring domain<br/>
   * ->host = referring domain with subdomains<br/>
   * ->path = path at referring URL<br/>
   * ->query = query part of referring URL<br/>
   * ->url = raw referring URL<br/>
   * ->phrase = search phrase if referrer was search engine or <b>null</b> otherwise
   */
  public static function getReferrer()
  {
    if (isset($_SERVER['HTTP_REFERER']))
    {
      $info = new stdClass();
      $info->url = $_SERVER['HTTP_REFERER'];
      $int = self::interpretURL($info->url);
      $info->domain = $int->domain;
      $info->host = $int->host;
      $info->path = $int->path;
      $info->query = $int->query;

      self::checkSearchPhrase($info);

      return $info;
    }
    return null;
  }

  /**
   * This method takes class created by getReferrer or interpretURL
   * and performs checks against its database.
   * In result it adds "phrase" atribute to $info class containing search phrase.
   * @param stdClass $info Must contain atributes:<br/>
   * ->domain<br/>
   * ->query<br/>
   * After execution receives ->phrase attribute with <i>String</i> value or <b>null</b>
   * if no search string found
   */
  private static function checkSearchPhrase(&$info)
  {
    // Search engines database
    $base = array(
        'bing.com' => array('type' => 'query', 'arg' => 'q'),
        'google.pl' => array('type' => 'query', 'arg' => 'q'),
        'google.com' => array('type' => 'query', 'arg' => 'q'),
    );

    if (isset($base[$info->domain]))
    {
      switch ($base[$info->domain]['type'])
      {
        case 'query':
          $ar = Chors_String::parseQueryString($info->query);
          if (isset($ar[$base[$info->domain]['arg']]))
          {
            $info->phrase = $ar[$base[$info->domain]['arg']];
          }
          break;
        default:
      }
    }
    $info->phrase = null;
  }

  /**
   * Performs operations on provided URL (which must be absolute)
   * in order to get all it's usefull information in handy way.
   * @param string $url URL for interpretion
   * @return stdClass stdClass with arguments:<br/>
   * example address: <i>http://sub.example.com:8000/index.html?a=v#c</i>
   * ->protocol = protocol part (<i>http</i>)<br/>
   * ->domain = domain part (<i>example.com</i>)<br/>
   * ->tld = top level domain part (<i>com</i>)<br/>
   * ->port = url port (<i>8000</i>) <b>default == 80</b><br/>
   * ->host = domain with subdomains (<i>sub.example.com</i>)<br/>
   * ->url = $url (<i>http://sub.example.com:8000/index.html?a=v#c</i>)<br/>
   * ->path = path at specified domain (<i>index.html</i>)<br/>
   * ->query = path's query (<i>a=v</i>)<br/>
   * ->anchor = path's anchor (<i>c</i>)<br/>
   * ->request = ->path+query+anchor elements (<i>index.html?a=v#c</i>)<br/>
   * <b>Not provided values are replaced by empty strings <i>""</i></b>
   */
  public static function interpretURL($url)
  {
    $info = new stdClass();
    $info->url = $url;

    $temp = explode("://", $url, 2);
    $info->protocol = $temp[0];
    $info->request = "";

    $temp = explode("#", $temp[1], 2);
    if (isset($temp[1]))
    {
      $info->anchor = $temp[1];
      $info->request = "#" . $temp[1];
    }
    else
      $info->anchor = "";

    $temp = explode("?", $temp[0], 2);
    if (isset($temp[1]))
    {
      $info->query = $temp[1];
      $info->request = "?" . $temp[1] . $info->request;
    }
    else
      $info->query = "";

    $temp = explode("/", $temp[0], 2);
    if (isset($temp[1]))
    {
      $info->path = $temp[1];
      $info->request = $temp[1] . $info->request;
    }
    else
      $info->path = "";

    $temp = explode(":", $temp[0], 2);
    if (isset($temp[1]))
      $info->port = (int) $temp[1];
    else
      $info->port = 80;

    $info->host = $temp[0];

    $temp = explode(".", $temp[0]);
    $info->tld = $temp[count($temp) - 1];
    if (count($temp) == 1)
      $info->domain = $info->tld;
    else
      $info->domain = $temp[count($temp) - 2] . '.' . $temp[count($temp) - 1];

    return $info;
  }

}
