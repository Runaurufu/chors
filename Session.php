<?php
/**
 * @author Runaurufu
 */
class Chors_Session
{
  static private $id = null;
  static private $name = null;
  static private $active = false;
  static private $namespace = null;
  static private $time = null;
  static private $cookiePath = null;

  static public function start($sessionName = null, $sessionId = null, $namespace = null)
  {
    if (self::$active)
      self::close();

    if ($sessionName !== null)
      session_name($sessionName);

    if ($sessionId !== null)
      session_id($sessionId);

    if ($namespace !== null)
      self::$namespace = $namespace;

    if (self::$cookiePath == null)
      self::$cookiePath = '/' . basename(getcwd()) . '/';

    session_set_cookie_params(self::$time, self::$cookiePath);
    if (session_start())
    {
      self::$active = true;
      self::$name = session_name();
      self::$id = session_id();
      self::$time = ini_get("session.gc_maxlifetime");
      setcookie(self::$name, self::$id, 0, self::$cookiePath);
    }
    else
      throw new Chors_Exception("Session creation error", Chors_ErrorCodes::SESSION_CANNOT_CREATE);
  }

  static public function regenerate($deleteOldSession = false)
  {
    if (self::$active)
    {
      $flag = session_regenerate_id($deleteOldSession);

      if ($flag)
      {
        self::$id = session_id();
        setcookie(self::$name, self::$id, 0, self::$cookiePath);
      }
      else
      {
        //@todo solve this issue...
        //http://php.net/manual/en/function.session-regenerate-id.php
        Chors_Logger::saveToLogFile("sessionError.log", "session id: " . session_id(), "true");
        throw new Chors_Exception("Session regenerate error", Chors_ErrorCodes::SESSION_CANNOT_REGENERATE);
      }
    }
    else
      throw new Chors_Exception("Session regenerate error - No active session", Chors_ErrorCodes::SESSION_NOT_ACTIVE);
  }

  static public function close()
  {
    if (self::$active)
    {
      session_write_close();
      self::$active = false;
    }
    else
      throw new Chors_Exception("Session close error - No active session", Chors_ErrorCodes::SESSION_NOT_ACTIVE);
  }

  static public function destroy()
  {
    if (self::$active)
    {
      session_destroy();
      //if (session_destroy())
      {
        self::$active = false;
        setcookie(self::$name, "", 1, self::$cookiePath);
      }
      //else
      //  throw new Chors_Exception("Session close error - Session destroy error", Chors_ErrorCodes::SESSION_CANNOT_DESTROY);
    }
    else
      throw new Chors_Exception("Session close error - No active session", Chors_ErrorCodes::SESSION_NOT_ACTIVE);
  }

  static public function clear()
  {
    if (self::$active)
    {
      session_unset();
      $_SESSION = array();
    }
    else
      throw new Chors_Exception("Session unset error - No active session", Chors_ErrorCodes::SESSION_NOT_ACTIVE);
  }

  static public function isActive()
  {
    return self::$active;
  }

  static public function setNamespace($namespace)
  {
    self::$namespace = $namespace;
  }

  static public function getNamespace()
  {
    return self::$namespace;
  }

  static public function getSID()
  {
    return self::$id;
  }

  static public function setTime($time)
  {
    self::$time = $time;
    ini_set("session.gc_maxlifetime", self::$time);
  }

  static public function getTime()
  {
    return self::$time;
  }

  static public function setValue($key, $value, $namespace = null)
  {
    if ($namespace == null)
      $namespace = self::$namespace;
    
    if (self::$active)
      $_SESSION[$namespace][$key] = $value;
    else
      throw new Chors_Exception("Session setValue error - No active session", Chors_ErrorCodes::SESSION_NOT_ACTIVE);
  }

  static public function setNamedValue($namespace, $key, $value)
  {
    if (self::$active)
      $_SESSION[$namespace][$key] = $value;
    else
      throw new Chors_Exception("Session setValue error - No active session", Chors_ErrorCodes::SESSION_NOT_ACTIVE);
  }

  static public function getValue($key, $namespace = null)
  {
    if ($namespace == null)
      $namespace = self::$namespace;
    
    return self::getNamedValue($namespace, $key);
  }

  static public function getNamedValue($namespace, $key)
  {
    if (isset($_SESSION[$namespace][$key]))
      return $_SESSION[$namespace][$key];
    else
      throw new Chors_Exception("Session getValue error - No record found", Chors_ErrorCodes::SESSION_ARRAY_NOT_FOUND);
  }
  
  static public function unsetValue($key, $namespace = NULL)
  {
    if ($namespace == null)
      $namespace = self::$namespace;
    
    self::unsetNamedValue($namespace, $key);
  }
  
  static public function unsetNamedValue($namespace, $key)
  {
    unset($_SESSION[$namespace][$key]);
  }

  static public function isValue($key, $namespace = null)
  {
    if ($namespace == null)
      $namespace = self::$namespace;

    if (isset($_SESSION[$namespace][$key]))
      return true;
    else
      return false;
  }

  /**
   * Sets cookie path for entire session handling mechanism
   * @param string $cookiePath 
   */
  static public function setCookiePath($cookiePath)
  {
    self::$cookiePath = $cookiePath;
  }

  /**
   * Returns cookie path
   * @return string 
   */
  static public function getCookiePath()
  {
    return self::$cookiePath;
  }

  /**
   * Sets session save path for entire session handling mechanism
   * @param string $sessionPath 
   */
  static public function setSessionSavePath($sessionPath)
  {
    if(is_dir($sessionPath))
      session_save_path($sessionPath);
  }

  /**
   * Returns session save path
   * @return string 
   */
  static public function getSessionSavePath()
  {
    return session_save_path();
  }
}
