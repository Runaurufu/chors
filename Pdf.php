<?php
require_once('Lib/mpdf/mpdf.php');
/**
 * Description of Chors_Pdf
 * @package Chors
 * @version 1.0
 * @since 1.0
 * @author Runaurufu
 */
class Chors_Pdf extends mPDF
{
  public function __construct()
  {
    $this->allow_charset_conversion=true;
    $this->charset_in='UTF-8';
  }
}