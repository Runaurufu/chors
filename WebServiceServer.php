<?php
require_once('Lib/phpWsdl/class.phpwsdl.php');

/**
 * Description of Chors_WebServiceServer
 *
 * @author Runaurufu
 */
abstract class Chors_WebServiceServer
{
  /**
   * @var PhpWsdl 
   */
  private $phpWsdlServer = null;
  public $inputDataCallback = null;
  public $outputDataCallback = null;
  public $optimizeWsdl = false;


  /**
   * 
   * @param string $namespace
   * @param string $endpointUri
   * @param string $cacheFolder
   * @param string[] $wsdlFiles
   * @param string $webServiceClass
   * @ignore
   */
  public function __construct($namespace, $endpointUri, $cacheFolder, $wsdlFiles)
  {
    if($wsdlFiles === null)
      $wsdlFiles = array();
    
    $className = get_class($this);
    
    $rc = new ReflectionClass($className);
    
    while($rc->name !== __CLASS__)
    {
      $wsdlFiles[] = $rc->getFileName();
      $rc = $rc->getParentClass();
    }
    
    $this->phpWsdlServer = PhpWsdl::CreateInstance(
      $namespace,       // Chors_Boot::getBaseUrl()."/"
      $endpointUri,     // Chors_Boot::getBaseUrl()."/index.php"
      $cacheFolder,     // './cache'
      $wsdlFiles,       // All files with WSDL definitions in comments
      $className,      // name of the class that serves as webservice
      null,             // method definitions
      null,             // complex types definitions
      false,            // Don't send WSDL right now
      false             // Don't start the SOAP server right now
      );
  }
    
  /**
   * Server will run and handle awaiting request.
   * @return boolean Indicate does server started.
   * @ignore
   */
  public function runServer()
  {
    if($this->phpWsdlServer->IsWsdlRequested())
      $this->phpWsdlServer->Optimize = $this->optimizeWsdl;
    
    if($this->inputDataCallback !== null)
      $this->inputDataCallback(file_get_contents("php://input"));
    
    if($this->outputDataCallback !== null)
      ob_start();
    
    $started = $this->phpWsdlServer->RunServer(null, get_class($this), false, false);
    
    if($this->outputDataCallback !== null)
    {
      $this->outputDataCallback(ob_get_contents());
      ob_end_flush();
    }
    
    return $started;
  }

    /**
   * 
   * @param int $time Pass 0 to disable caching
   */
  public static function setCacheTime($time)
  {
    if($time === 0)
    {
      ini_set('soap.wsdl_cache_enabled',0);	// Disable caching in PHP
      PhpWsdl::$CacheTime=0;                // Disable caching in PhpWsdl
    }
    else
    {
      ini_set('soap.wsdl_cache_enabled',1);	// Enable caching in PHP
      PhpWsdl::$CacheTime=$time;            // Enable caching in PhpWsdl
    }
  }
}
