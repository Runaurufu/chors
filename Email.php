<?php

/**
 * Chors email class, use it for any operations concerning email.
 * @package Chors
 * @since 1.0
 * @version 1.0
 * @author Runaurufu
 * @todo create it
 */
class Chors_Email
{
  /**
   * @var PHPMailer 
   */
  private $phpMailer = null;
  
  /**
   * 
   * @param PHPMailer $phpMailer
   */
  public function __construct($phpMailer = NULL)
  {
    require_once('Lib/PHPMailer/PHPMailerAutoload.php');
    
    if($phpMailer === NULL)
      $phpMailer = new PHPMailer();
    $this->phpMailer = $phpMailer;
  }
  
  /**
   * 
   * @param int $level SMTP class debug output mode.
     * Debug output level.
     * Options:
     * * `0` No output
     * * `1` Commands
     * * `2` Data and commands
     * * `3` As 2 plus connection status
     * * `4` Low-level data output
     * @var integer
   * @param type $delegate provide a callable expecting two params: a message string and the debug level:
     * <code>
     * $mail->Debugoutput = function($str, $level) {echo "debug level $level; message: $str";};
     * </code>
   */
  public function SetDebug($level, $delegate)
  {
    $this->phpMailer->SMTPDebug = $level;
    $this->phpMailer->Debugoutput = $delegate; 
  }
  
  public function SetXMailer($string)
  {
    $this->phpMailer->XMailer = $string;
  }
  
  public function setSMTPConnection($host, $port, $secure, $username, $password, $options)
  {
    $this->phpMailer->isSMTP();
    $this->phpMailer->SMTPAuth = true;
    
    if($host !== NULL)
      $this->phpMailer->Host = $host;
    
    if($port !== NULL)
      $this->phpMailer->Port = $port;

    if($secure !== NULL)
      $this->phpMailer->SMTPSecure = $secure;
    
    if($username !== NULL)
      $this->phpMailer->Username = $username;
    
    if($password !== NULL)
      $this->phpMailer->Password = $password;
    
    if($options !== NULL)
      $this->phpMailer->SMTPOptions = $options;
    
    $this->phpMailer->CharSet = 'UTF-8';
  }
  
  public function setFrom($address, $name = '', $auto = true)
  {
    return $this->phpMailer->setFrom($address, $name, $auto);
  }
  
  public function addReplyToAddress($address, $name = '')
  {
    return $this->phpMailer->addReplyTo($address, $name);
  }
  
  public function clearReplyToAdresses()
  {
    $this->phpMailer->clearReplyTos();
  }
  
  public function addRecipientAddress($address, $name = '')
  {
    return $this->phpMailer->addAddress($address, $name);
  }
  
  public function clearRecipientAdresses()
  {
    $this->phpMailer->clearAddresses();
  }
  
  public function addCCAddress($address, $name = '')
  {
    return $this->phpMailer->addCC($address, $name);
  }
  
  public function clearCCAdresses()
  {
    $this->phpMailer->clearCCs();
  }
  
  public function addBCCAddress($address, $name = '')
  {
    return $this->phpMailer->addBCC($address, $name);
  }
  
  public function clearBCCAdresses()
  {
    $this->phpMailer->clearBCCs();
  }
  
  public function clearAllAdresses()
  {
    $this->phpMailer->clearAllRecipients();
  }
  
  public function addAttachment($path, $name = '', $encoding = 'base64', $type = '', $disposition = 'attachment')
  {
    return $this->phpMailer->addAttachment($path, $name, $encoding, $type, $disposition);
  }
  
  public function clearAttachments()
  {
    $this->phpMailer->clearAttachments();
  }
  
  public function setPlainTextContent($subject, $body)
  {
    $this->phpMailer->isHTML(false);
    $this->phpMailer->Subject = $subject;
    $this->phpMailer->Body = $body;
    $this->phpMailer->AltBody = '';
  }
  
  public function setHTMLContent($subject, $body, $altBody)
  {
    $this->phpMailer->isHTML(true);
    $this->phpMailer->Subject = $subject;
    $this->phpMailer->Body = $body;
    $this->phpMailer->AltBody = $altBody;
  }
  
  public function send()
  {
    try
    {
      return $this->phpMailer->send();
    }
    catch (Exception $ex)
    {
      // ??
      return false;
    }
  }

  /**
   * Checks if specified address meets RFC5322 requirements
   * @link http://tools.ietf.org/html/rfc2822#section-3.4.1
   * @link http://tools.ietf.org/html/rfc5322
   * @link http://tools.ietf.org/html/rfc3696
   * @todo check for correctness [a-zA-Z0-9+-_]@[a-zA-Z0-9_-].[a-z] or ^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$
   * @param string $email email address to check
   * @return bool <i>true</i> - email is correct<br/><i>false</i> - email is incorrect
   */
  public static function isCorrect($email)
  {
    if (strlen($email) > 254) // RFC 2821 specifies max length of an address in MAIL and RCPT commands
    {
      return false;
    }

    if (!filter_var($email, FILTER_VALIDATE_EMAIL))
    {
      return false;
    }

    return true;
  }
}
