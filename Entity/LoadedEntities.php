<?php

/**
 * This class is storing all loaded entities during this user call,
 * so it allow reusing previously created entities with minimal performance impact.
 * @author Runaurufu
 */
abstract class Chors_LoadedEntities
{

  private static $isEnabled = false;

  public static function isEnabled($value = null)
  {
    if ($value != null)
      self::$isEnabled = $value;
    return self::$isEnabled;
  }

  private static $loaded = null;

  public static function addEntity(&$entity)
  {
    if (self::$isEnabled == false)
      return;

    if (self::$loaded == null)
      self::$loaded = array();

    $className = get_class($entity);
    if (isset(self::$loaded[$className]) == false)
      self::$loaded[$className] = array();

    if (isset(self::$loaded[$className][$entity->getIdentifier()]) === false || self::$loaded[$className][$entity->getIdentifier()]->isShallow() === true)
      self::$loaded[$className][$entity->getIdentifier()] = $entity;
  }

  public static function isEntity($className, $entityIdentifier, $allowShallow = false)
  {
    if (self::$isEnabled == false)
      return false;

    if (isset(self::$loaded[$className][$entityIdentifier]) == true)
    {
      if ($allowShallow === false && self::$loaded[$className][$entityIdentifier]->isShallow() === true)
        return false;
      else
        return true;
    }
    return false;
  }

  public static function getEntity($className, $entityIdentifier, $allowShallow = false)
  {
    if (self::$isEnabled == false)
      return null;

    if (isset(self::$loaded[$className][$entityIdentifier]) == true)
    {
      if ($allowShallow === false && self::$loaded[$className][$entityIdentifier]->isShallow() === true)
        self::$loaded[$className][$entityIdentifier]->load();
      return self::$loaded[$className][$entityIdentifier];
    }
    else
      return null;
  }

  public static function removeEntity($entity)
  {
    if (self::$isEnabled == false)
      return;

    if (isset(self::$loaded[get_class($entity)][$entity->getIdentifier()]) == true)
    {
      unset(self::$loaded[get_class($entity)][$entity->getIdentifier()]);
    }
  }

}

/*
 * This class shouldn't be used by any of outside framework objects.
 * It is purely internal class for handling caching of mock entities.
 * @author Runaurufu
 */

class Chors_EntityCacheStructure
{

  private $className;
  private $identifier;

  public function __construct($classInstance, $identifier)
  {
    $this->className = get_class($classInstance);
    $this->identifier = $identifier;
  }

  /**
   *
   * @param bool $getInstance if true than class will return instance of object (reloaded data)
   * @return object 
   */
  public function createClassInstance($getInstance = false, $allowShallow = false)
  {
    Chors_Logger::internalTrace("EntityCacheStructure->createClassInstance: " . $this->className . "(" . $this->identifier . ")");
    $instance = null;
    if ($getInstance === TRUE)
      $instance = Chors_AEntity::getInstance($this->className,
                      $this->identifier, NULL, $allowShallow);
    else
    { // it gets executed every single time currently
      $instance = Chors_LoadedEntities::getEntity($this->className,
                      $this->identifier, $allowShallow);
      if ($instance == null)
      {
        $instance = new $this->className($this->identifier);
        Chors_LoadedEntities::addEntity($instance);
      }
    }
    return $instance;
  }

}

/*
 * This class shouldn't be used by any of outside framework objects.
 * It is purely internal class for handling caching of serialized entities.
 */

class Chors_SerializedEntity
{

  private $className;
  private $identifier;
  private $data;

  public function __construct($className, $identifier, $data)
  {
    $this->className = $className;
    $this->identifier = $identifier;
    $this->data = $data;
  }

  public function createClassInstance($sourceAge = 0, $databaseInstance = NULL)
  {
    $data = $this->data;
    $instance = null;

    $instance = Chors_LoadedEntities::getEntity($this->className,
                    $this->identifier, $data['__shallow']);

    if ($instance == null)
    {
      $instance = new $this->className($this->identifier, $databaseInstance);
      Chors_LoadedEntities::addEntity($instance);
    }

    $data['__age'] = $data['__age'] + $sourceAge;
    if ($instance->getAge() == null || $instance->getAge() > $data['__age'])
      $instance->setCacheSaveData($data, $sourceAge);

    return $instance;
  }

  public function getData()
  {
    return $this->data;
  }

  public function getIdentifier()
  {
    return $this->identifier;
  }

  public function getClassName()
  {
    return $this->className;
  }

}

interface Chors_IEntity
{
  static function getEntityInstance($identifier = null, $cacheTime = null, $allowShallow = false, $databaseInstance = null);
}

abstract class Chors_Coherency
{
  /** Data coherent with data in data source */
  const Coherent = 0;
  /** Data incoherent with data in data source */
  const Incoherent = 1;
  /** Instance was removed or was newly created and do not have equivalent in data source */
  const NonExistent = 2;
}

interface Chors_ICoherencyState
{
  function getCoherencyState();
  function setCoherencyState($state);
}
