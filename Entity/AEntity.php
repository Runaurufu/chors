<?php

/**
 * @author Runaurufu
 * @version 1.00
 * @since 1.0
 */
abstract class Chors_AEntity extends Chors_ARenderable implements Chors_IEntity, Chors_ICoherencyState
{
  /**
   *
   * @var boolean Determine if caching to file mechanism is active or not.
   */
  public static $IsEntityCachingEnabled = true;
  
  protected $db = null;
  protected $__shallow = false;
  protected $__age = null;
  protected $__coherency = Chors_Coherency::Incoherent;

  /** @var bool if true child entities are saved as data, if false they are saved as references to their objects */
  protected $cacheChildEntities = false;

  /** @var bool if true than this entity will be always data-cached and not reference-cached */
  protected $forceEntityCache = false;

  public function isShallow()
  {
    return $this->__shallow;
  }
  
  /**
   * Specified fields will not be serialized.
   * @return string[]
   */
  protected function getNotSerializableFields()
  {
    return array( 'identifier', 'db');
  }

  public function __construct($identifier = null, $databaseInstance = null)
  {
    parent::__construct($identifier);
    Chors_Logger::internalTrace("Entity created: " . get_class($this) . "(" . $identifier . ")");

    if ($databaseInstance === null)
      $this->db = Chors_Database::getInstance();
    else
      $this->db = $databaseInstance;
  }

  public final function getAge()
  {
    return $this->__age;
  }

  /**
   * Loads desired entity via require once
   * @param string $entityName entity which should be required
   */
  public final static function loadEntity($entityName)
  {
    if (!Chors_File::exists(Chors_Boot::$applicationDir . '/' . Chors_Boot::$entityDir . "/" . $entityName . ".php"))
    {
      throw new Chors_Exception(
        "Entity not found",
        Chors_ErrorCodes::ENTITY_NOT_FOUND,
        Chors_Boot::$applicationDir . '/' . Chors_Boot::$entityDir . "/" . $entityName . ".php");
    }
    require_once Chors_Boot::$applicationDir . '/' . Chors_Boot::$entityDir . "/" . $entityName . ".php";
  }

  private function getCacheObject($time)
  {
    return new Chors_Cache(Array("datasource" => true, "time" => (int) $time, "type" => "Entity." . get_class($this)),
            $this->identifier);
  }

  /**
   * Checks if entity corrensponding object is still existent (can its data be refreshed). If true than data are loaded and any cache file is overwritten.
   * @return bool 
   */
  public function doesExist()
  {
    $cache = self::GetCacheObject(Chors_Time::Year);

    if ($this->load(false) == false)
    {
      $cache->delete();
      $this->setCoherencyState(Chors_Coherency::NonExistent);
      return false;
    }
    else
    {
      if (self::$IsEntityCachingEnabled && $cache->check())
        $cache->saveDataSource($this->getCacheSaveData());

      $this->setCoherencyState(Chors_Coherency::Coherent);
      return true;
    }
  }

  private function getCacheSaveData()
  {
    $className = get_class($this);
    $data = get_object_vars($this);

    foreach ($this->getNotSerializableFields() as $fieldName)
    {
      unset($data[$fieldName]);
    }

    foreach ($data as $key => $value)
    {
      if ($value instanceof Chors_AEntity)
      {
        if ($this->cacheChildEntities == true || $value->forceEntityCache == true)
          $data[$key] = $value->getCacheSaveData();
        else
          $data[$key] = new Chors_EntityCacheStructure($value,
                  $value->identifier);
      }
      elseif (is_array($value))
      {
        foreach ($value as $deepKey => $deepValue)
        {
          if ($deepValue instanceof Chors_AEntity)
          {
            if ($this->cacheChildEntities == true || $deepValue->forceEntityCache == true)
              $data[$key][$deepKey] = $deepValue->getCacheSaveData();
            else
              $data[$key][$deepKey] = new Chors_EntityCacheStructure($deepValue,
                      $deepValue->identifier);
          }
        }
      }
    }
    return new Chors_SerializedEntity($className, $this->identifier, $data);
  }

  public function setCacheSaveData($data, $maxAge, $cacheAge = 0)
  {
    Chors_Logger::internalTrace("Entity->setCacheSaveData: " . get_class($this) . "(" . $this->identifier . ")");
    foreach ($data as $key => $value)
    {
      if ($value instanceOf Chors_EntityCacheStructure)
      {
        $this->$key = $value->createClassInstance();
        if ($this->$key != null && ($this->$key->__age === null || (int) $this->$key->__age + $cacheAge > $maxAge))
          $this->$key->cache($maxAge);
      }
      elseif ($value instanceOf Chors_SerializedEntity)
      {
        $this->$key = $value->createClassInstance($cacheAge, $this->db);
        if ($this->$key != null && ($this->$key->__age === null || (int) $this->$key->__age > $maxAge))
          $this->$key->cache($maxAge);
      }
      elseif (is_array($value))
      {
        $this->$key = array();
        foreach ($value as $deepKey => $deepValue)
        {
          if ($deepValue instanceOf Chors_EntityCacheStructure)
          {
            $this->{$key}[$deepKey] = $deepValue->createClassInstance();
            if ($this->{$key}[$deepKey] != null && ($this->{$key}[$deepKey]->__age === null || (int) $this->{$key}[$deepKey]->__age + $cacheAge > $maxAge))
              $this->{$key}[$deepKey]->cache($maxAge);
          }
          elseif ($deepValue instanceOf Chors_SerializedEntity)
          {
            $this->{$key}[$deepKey] = $deepValue->createClassInstance($cacheAge,
                    $this->db);
            if ($this->{$key}[$deepKey] != null && ($this->{$key}[$deepKey]->__age === null || (int) $this->{$key}[$deepKey]->__age > $maxAge))
              $this->{$key}[$deepKey]->cache($maxAge);
          }
          else
          {
            $this->{$key}[$deepKey] = $deepValue;
          }
        }
      }
      else
      {
        $this->$key = $value;
      }
    }
  }

  /**
   * It loads entity from existing cachable location.
   * @param int $time 
   * @return bool <b>true</b> if entity data were loaded or <b>false</b> if entity does not exist anymore
   */
  public function cache($time)
  {
    Chors_Logger::internalTrace("Entity->cache: " . get_class($this) . "(" . $this->identifier . ")");
    if ($this->__age !== NULL && $this->__age < $time)
      return TRUE;
    
    if(self::$IsEntityCachingEnabled === FALSE)
      return $this->load(true);
    
    $cache = self::GetCacheObject($time);
    if ($cache->check())
    {
      $cacheAge = $cache->getCacheAge();
      
      // check if we are having best available cache file already loaded
      if($this->__age !== NULL  && $this->__age <= $cacheAge)
        return TRUE;
      
      $data = $cache->fetchDataSource();
      $realData = $data->getData();

      $realAge = $realData['__age'] !== NULL ? $realData['__age'] + $cacheAge : $cacheAge;
      $this->__age = $realAge;
      $this->setCacheSaveData($realData, $time, $cacheAge);
      $this->__age = $realAge;
      return TRUE;
    }

    if ($this->load(true) === FALSE)
    {
      $cache->delete();
      return FALSE;
    }
    
    $cache->saveDataSource($this->getCacheSaveData());
    return TRUE;
  }

  /**
   * 
   * @param bool $respectCoherency if set to true then only state == Incoherent will trigger saving
   */
  public final function save($respectCoherency = false)
  {
    if($respectCoherency === false || $this->getCoherencyState() === Chors_Coherency::Incoherent)
    {
      $this->eventBeforeSaved();
      
      if ($this->identifier == null)
      {  $this->identifier = $this->insertData();  }
      else
      {  $this->updateData();  }
      
      $this->eventAfterSaved();
    }
  }
  
  /**
   * This method is fired before insertData() and updateData() called by save() method.
   */
  protected function eventBeforeSaved()
  {
    ;
  }
  
  /**
   * This method is fired after insertData() and updateData() called by save() method.
   */
  protected function eventAfterSaved()
  {
    $this->deleteCache();
    $this->__age = 0;
    $this->setCoherencyState(Chors_Coherency::Coherent);
    Chors_LoadedEntities::addEntity($this);
  }

  /**
   * @return bool true - entity exist and was loaded; false - entity does not exist 
   */
  protected abstract function shallowLoadData();

  /**
   * @return bool true - entity exist and was loaded; false - entity does not exist 
   */
  protected abstract function loadData();

  protected abstract function deleteData();

  protected abstract function insertData();

  protected abstract function updateData();

  protected abstract function parseFromRow($row);

  /**
   * Provides entity already filled with data. If there is memory cached entity than it is returned.
   * @param string $className
   * @param mixed $identifier if NULL then new instance is returned.
   * @param int $cacheTime allowed cache time. Set NULL to reload entity content.
   * @param bool $allowShallow if TRUE then it is allowed to return object loaded in shallow manner.
   * @param Chors_Database $databaseInstance
   * @return AEntity or <b>NULL</b> if not exist 
   */
  public static function getInstance($className, $identifier = NULL, $cacheTime = NULL, $allowShallow = false, $databaseInstance = NULL)
  {
    Chors_Logger::internalTrace("Entity getInstance: " . $className . "(" . $identifier . ")");
    $instance = NULL;
    if ($identifier === NULL)
      return new $className($identifier, $databaseInstance);
    else
      $instance = Chors_LoadedEntities::getEntity($className, $identifier,
                      $allowShallow);

    if ($instance === NULL)
    {
      $instance = new $className($identifier, $databaseInstance);
      if (self::$IsEntityCachingEnabled && $cacheTime !== NULL)
      {
        if ($instance->cache($cacheTime) == FALSE)
          $instance = NULL;
      }
      else if ($allowShallow === true)
      {
        if ($instance->shallowLoad() == false)
          $instance = NULL;
      }
      else if ($instance->load() == false)
        $instance = NULL;
    }
    return $instance;
  }

  public final function shallowLoad($loadOnlyIfNotLoaded = false)
  {
    if ($loadOnlyIfNotLoaded === true && $this->__age === 0)
      return true;

    Chors_Logger::internalTrace("Entity->shallowLoadData: " . get_class($this) . "(" . $this->identifier . ")");
    $value = $this->shallowLoadData();

    if ($value == true)
    {
      $this->__shallow = true;
      Chors_LoadedEntities::addEntity($this);
      $this->__age = 0;
      $this->setCoherencyState(Chors_Coherency::Coherent);
    }
    else
    {
      Chors_LoadedEntities::removeEntity($this);
      $this->__age = PHP_INT_MAX;
      $this->setCoherencyState(Chors_Coherency::NonExistent);
    }
    return $value;
  }

  /**
   * It loads entity data from datasource defined in LoadData().
   * Be aware that it does not load data from already loaded (memory cached) entities.
   * @return bool indicate whenever entity exist or not
   */
  public final function load($loadOnlyIfNotLoaded = false)
  {
    if ($loadOnlyIfNotLoaded === true && $this->__age === 0 && $this->__shallow === false)
      return true;

    Chors_Logger::internalTrace("Entity->shallowLoadData: " . get_class($this) . "(" . $this->identifier . ")");
    $value = $this->shallowLoadData($loadOnlyIfNotLoaded);

    if ($value == true)
    {
      Chors_Logger::internalTrace("Entity->loadData: " . get_class($this) . "(" . $this->identifier . ")");
      $this->loadData();
      $this->__shallow = false;
      Chors_LoadedEntities::addEntity($this);
      $this->__age = 0;
      $this->setCoherencyState(Chors_Coherency::Coherent);
    }
    else
    {
      Chors_LoadedEntities::removeEntity($this);
      $this->__age = PHP_INT_MAX;
      $this->setCoherencyState(Chors_Coherency::NonExistent);
    }
    return $value;
  }

  public final function delete()
  {
    if ($this->identifier == null)
      return;

    if ($this->loadData() == true && $this->__age == 0)
      $this->deleteData();

    $this->deleteCache();
    Chors_LoadedEntities::removeEntity($this);
    $this->__age = PHP_INT_MAX;
    $this->setCoherencyState(Chors_Coherency::NonExistent);
  }

  /**
   * Removes all caches related to current object instance.
   */
  public final function deleteCache()
  {
    $this->cacheDelete();
    $this->cacheRenderDelete();
  }
  
  protected final function cacheDelete()
  {
    $cache = $this->getCacheObject(0);
    $cache->delete();
  }

  /**
   * Get object current coherency state.
   * @return int
   */
  public function getCoherencyState()
  {
    return $this->__coherency;
  }
  
  /**
   * Sets object new coherency state. Values accepted can be found in Chors_Coherency.
   * @param int $state
   */
  public function setCoherencyState($state)
  {
    $this->__coherency = $state;
  }
}
