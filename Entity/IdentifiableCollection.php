<?php

/**
 * IdentifableCollection class allow handling array of instances
 * of objects implemention Identifiable in a manner which let you acquire
 * both collection added and removed instances.
 * @author Runaurufu
 * @version 1.00
 * @since 1.0
 */
class Chors_IdentifiableCollection
{
  /**
   * @var Chors_IIdentifiable[]
   */
  private $array = array();
  
  /**
   * @var Chors_IIdentifiable[]
   */
  private $added = array();
  
  /**
   * @var Chors_IIdentifiable[]
   */
  private $removed = array();
  
  /**
   * 
   * @param Chors_IIdentifiable $item
   */
  public function add($item)
  {
    $this->array[] = $item;
    
    $this->added[] = $item;
  }
  
  /**
   * Removes element by its reference (=== comparison).
   * @param Chors_IIdentifiable $item
   */
  public function removeByItemReference($item)
  {
    foreach ($this->array as $key => $value)
    { /* @var $value Chors_IIdentifiable  */
      if($value === $item)
      {
        $this->removed[] = $value;
        
        unset ($this->array[$key]);
        return;
      }
    }
  }
  
  /**
   * Removes element by its value (== comparison).
   * @param Chors_IIdentifiable $item
   */
  public function removeByItemValue($item)
  {
    foreach ($this->array as $key => $value)
    { /* @var $value Chors_IIdentifiable  */
      if($value == $item)
      {
        $this->removed[] = $value;
        
        unset ($this->array[$key]);
        return;
      }
    }
  }
  
  /**
   * Remove element by its type and identifier.
   * @param type $item
   * @return type
   */
  public function removeByItemId($item)
  {
    $type = get_class($item);
    $id = $item->getIdentifier();
    
    foreach ($this->array as $key => $value)
    { /* @var $value Chors_IIdentifiable  */
      if($value->getIdentifier() === $id && get_class($value) === $type)
      {
        $this->removed[] = $value;
        
        unset ($this->array[$key]);
        return;
      }
    }
  }
  
  /**
   * Removes all items from current collection.
   */
  public function clear()
  {
    foreach ($this->array as $value)
    {
      $this->removed[] = $value;
    }
    
    unset($this->array);
    $this->array = array();
  }

  public function count()
  {
    return count($this->array);
  }
  
  /**
   * Sets initial state of collection.
   * This will erase all info regarding added or removed entries.
   * @param Chors_IIdentifiable[] $data
   */
  public function setInitialState($data)
  {
    $this->array = $data;
    
    $this->reset();
  }
  
  private function reset()
  {
    unset($this->added);
    $this->added = array();
    unset($this->removed);
    $this->removed = array();
  }
  
  /**
   * Sets current collection state as new initial state.
   * This will erase all info regarding added or removed entries.
   */
  public function commitChanges()
  {
    $this->reset();
  }
  
  /**
   * Revert current collection state to current initial state.
   * This will erase all info regarding added or removed entries.
   */
  public function revertChanges()
  {
    $newArray = array();
    
    // keep items from current array which were not recently added to it.
    foreach ($this->array as $key => $value)
    { /* @var $value Chors_IIdentifiable  */
      $wasAdded = FALSE;
      
      foreach ($this->added as $addedItem)
      {
        if($value === $addedItem)
        {
          $wasAdded = TRUE;
          break;
        }
      }
      
      if($wasAdded === FALSE)
      {
        $newArray[] = $value;
      }
    }
    
    // add all removed items.
    foreach ($this->removed as $value)
    {
      $newArray[] = $value;
    }
    
    unset($this->array);
    $this->array = $newArray;
    
    $this->reset();
  }
  
  /**
   * @return Chors_IIdentifiable[] Array of added elements
   */
  public function getAddedElements()
  {
    return $this->added;
  }
  
  /**
   * @return Chors_IIdentifiable[] Array of removed elements.
   */
  public function getRemovedElements()
  {
    return $this->removed;
  }
  
  /**
   * @return Chors_IIdentifiable[] Array of currently hold elements.
   */
  public function getCurrentElements()
  {
    return $this->array;
  }
}
