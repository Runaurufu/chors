<?php

interface Chors_IIdentifiable
{
  /**
   * Retrieves object identifier
   * @return mixed 
   */
  function getIdentifier();
}

/**
 * Represents renderable and render-cacheable class object
 * @author Runaurufu
 */
abstract class Chors_ARenderable implements Chors_IIdentifiable
{

  protected $identifier = null;
  protected $managedDecorators = null;

  public function __construct($identifier = null)
  {
    Chors_Logger::internalTrace("Renderable created: " . get_class($this) . "(" . $identifier . ")");

    $this->identifier = $identifier;
  }

  /**
   * Set unique per class name identifier (required for caching)
   * @param type $identifier 
   */
  public final function setIdentifier($identifier)
  {
    $this->identifier = $identifier;
    if($this instanceof Chors_ICoherencyState)
      $this->setCoherencyState(Chors_Coherency::Incoherent);
  }

  /**
   * Retrieves object identifier
   * @return type 
   */
  public final function getIdentifier()
  {
    return $this->identifier;
  }

  /**
   * Render object with using specified decorator
   * @param String $decorator decorator file name (without .deco extension) and any sub path from application presentation dir
   * @param type $renderData 
   */
  public final function render($decorator, $renderData = null)
  {
    include Chors_Boot::$applicationDir . '/' . Chors_Boot::$presentationDir . '/' . $decorator . '.deco';
  }

  /**
   * Render object with using specified decorator, retrieve render from file if its age is lower than time and saves created render to file
   * @param String $decorator decorator file name (without .deco extension) and any sub path from application presentation dir
   * @param type $time
   * @param type $renderData
   * @return type 
   */
  public final function cacheRender($decorator, $time, $renderData = null)
  {
    $cache = $this->getCacheRenderObject($time, $decorator);
    if ($cache->check())
      return $cache->fetchRender();

    $cache->startOutputCaching();
    include Chors_Boot::$applicationDir . '/' . Chors_Boot::$presentationDir . '/' . $decorator . '.deco';
    return $cache->endOutputCachingClean();
  }

  /**
   * Render object into string
   * @param String $decorator decorator file name (without .deco extension) and any sub path from application presentation dir
   * @param type $renderData
   * @return String rendered data 
   */
  public final function renderAsString($decorator, $renderData = null)
  {
    ob_start();

    include Chors_Boot::$applicationDir . '/' . Chors_Boot::$presentationDir . '/' . $decorator . '.deco';
    $string = ob_get_contents();

    ob_end_clean();
    return $string;
  }

  /**
   * Remove any cached renders for specified decorator of current object
   * @param type $decorator 
   */
  protected final function cacheRenderDelete($decorator = null)
  {
    if ($decorator === NULL)
    {
      if ($this->managedDecorators !== NULL && is_array($this->managedDecorators))
        foreach ($this->managedDecorators as $decoratorName)
          $this->cacheRenderDelete($decoratorName);
    }
    else
    {
      $cache = $this->getCacheRenderObject(0, $decorator);
      $cache->delete();
    }
  }

  /**
   *
   * @param type $time
   * @return Chors_Cache 
   */
  protected final function getCacheRenderObject($time, $decorator)
  {
    return new Chors_Cache(Array("datasource" => false, "time" => (int) $time, "type" => "Presentation." . get_class($this) . "." . str_replace(array('/', '\\'),
                "_", $decorator)), $this->identifier);
  }

}
