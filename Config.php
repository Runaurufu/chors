<?php

/**
 * Description of Config
 * @author Runaurufu
 */
class Chors_Config
{

  /**
   * Acquire configuration settings from specified file. File should be in format:
   * [section]<br/>
   * <b>key:value</b> (or <b>key=value</b>)<br/>
   * be aware that '=' is interpreted before ':',
   * so row 'key:12=34' would be translated into: 'key:12' = '34'
   * also no escape characters are provided
   * @param string $filename file which should parsed
   * @param array $settings already existing config array, to which data would be added
   * @return array returns created (or modified) array
   */
  static function acquire($filename, &$settings = NULL)
  {
    if ($settings === NULL)
      $settings = array();
    if (Chors_File::exists($filename))
    {
      $buffer = Chors_File::readAsArray($filename, null, true);

      $tag = '';
      $tag_is_set = false;
      foreach ($buffer as $key => $value)
      {
        if (($pos = strpos($value, '[')) === 0 && ($pose = strpos($value, ']')) > 1)
        { // if tag exists
          $tag = trim(substr($value, 1, $pose - 1));
          $tag_is_set = true;
        }
        elseif ($pos === 0 && $pose === 1)
        {
          $tag = '';
          $tag_is_set = false;
        }
        elseif (($pos = strpos($value, '=')) > 0)
        {
          if ($tag_is_set)
            $settings[$tag][trim(substr($value, 0, $pos))] = trim(substr($value,
                            $pos + 1));
          else
            $settings[trim(substr($value, 0, $pos))] = trim(substr($value,
                            $pos + 1));
        }
        elseif (($pos = strpos($value, ':')) > 0)
        {
          if ($tag_is_set)
            $settings[$tag][trim(substr($value, 0, $pos))] = trim(substr($value,
                            $pos + 1));
          else
            $settings[trim(substr($value, 0, $pos))] = trim(substr($value,
                            $pos + 1));
        }
      }
    }
    else
    {
      throw new Chors_Exception("File not found", Chors_ErrorCodes::FILE_NOT_FOUND, $filename);
    }
    return $settings;
  }

  /**
   * Acquire configuration settings for specific release
   * @param array $settings array obtained from configuration file
   * @param string $preferedRelease relase which should be used. By default <i>:Local</i> and <i>:Remote</i> are supported
   */
  static function acquireSpecificRelease(&$settings, $preferedRelease = NULL)
  {
    if ($preferedRelease == NULL)
    {
      if ($_SERVER['REMOTE_ADDR'] == "127.0.0.1" || ($_SERVER['HTTP_HOST'] == "localhost" && $_SERVER['SERVER_ADDR'] == "127.0.0.1"))
        $preferedRelease = ":Local";
      else
        $preferedRelease = ":Remote";
    }

    foreach ($settings as $key => $value)
    {
      if (Chors_String::contains($key, $preferedRelease) !== false)
      {
        $newKey = str_replace($preferedRelease, "", $key);
        $settings[$newKey] = $settings[$key];
      }
    }
  }

}
